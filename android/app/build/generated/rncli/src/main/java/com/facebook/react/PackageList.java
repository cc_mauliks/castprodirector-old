
package com.facebook.react;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainPackageConfig;
import com.facebook.react.shell.MainReactPackage;
import java.util.Arrays;
import java.util.ArrayList;

import com.director.castpro.BuildConfig;
import com.director.castpro.R;

// @react-native-community/async-storage
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
// @react-native-community/netinfo
import com.reactnativecommunity.netinfo.NetInfoPackage;
// react-native-audio
import com.rnim.rn.audio.ReactNativeAudioPackage;
// react-native-audio-recorder-player
import com.dooboolab.RNAudioRecorderPlayerPackage;
// react-native-device-info
import com.learnium.RNDeviceInfo.RNDeviceInfo;
// react-native-document-picker
import io.github.elyx0.reactnativedocumentpicker.DocumentPickerPackage;
// react-native-fbsdk
import com.facebook.reactnative.androidsdk.FBSDKPackage;
// react-native-file-viewer
import com.vinzscam.reactnativefileviewer.RNFileViewerPackage;
// react-native-firebase
import io.invertase.firebase.RNFirebasePackage;
// react-native-fs
import com.rnfs.RNFSPackage;
// react-native-geocoder
import com.devfd.RNGeocoder.RNGeocoderPackage;
// react-native-gesture-handler
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
// react-native-get-location
import com.github.douglasjunior.reactNativeGetLocation.ReactNativeGetLocationPackage;
// react-native-image-crop-picker
import com.reactnative.ivpusic.imagepicker.PickerPackage;
// react-native-image-picker
import com.imagepicker.ImagePickerPackage;
// react-native-keep-awake
import com.corbt.keepawake.KCKeepAwakePackage;
// react-native-linear-gradient
import com.BV.LinearGradient.LinearGradientPackage;
// react-native-localization
import com.babisoft.ReactNativeLocalization.ReactNativeLocalizationPackage;
// react-native-location
import com.github.reactnativecommunity.location.RNLocationPackage;
// react-native-location-switch
import org.pweitz.reactnative.locationswitch.LocationSwitchPackage;
// react-native-orientation
import com.github.yamill.orientation.OrientationPackage;
// react-native-push-notification
import com.dieam.reactnativepushnotification.ReactNativePushNotificationPackage;
// react-native-reanimated
import com.swmansion.reanimated.ReanimatedPackage;
// react-native-sound
import com.zmxv.RNSound.RNSoundPackage;
// react-native-sound-recorder
import com.kevinresol.react_native_sound_recorder.RNSoundRecorderPackage;
// react-native-splash-screen
import org.devio.rn.splashscreen.SplashScreenReactPackage;
// react-native-thumbnail
import me.hauvo.thumbnail.RNThumbnailPackage;
// react-native-tooltips
import px.tooltips.RNTooltipsPackage;
// react-native-vector-icons
import com.oblador.vectoricons.VectorIconsPackage;
// react-native-version-info
import cx.evermeet.versioninfo.RNVersionInfoPackage;
// react-native-video
import com.brentvatne.react.ReactVideoPackage;
// rn-fetch-blob
import com.RNFetchBlob.RNFetchBlobPackage;

public class PackageList {
  private Application application;
  private ReactNativeHost reactNativeHost;
  private MainPackageConfig mConfig;

  public PackageList(ReactNativeHost reactNativeHost) {
    this(reactNativeHost, null);
  }

  public PackageList(Application application) {
    this(application, null);
  }

  public PackageList(ReactNativeHost reactNativeHost, MainPackageConfig config) {
    this.reactNativeHost = reactNativeHost;
    mConfig = config;
  }

  public PackageList(Application application, MainPackageConfig config) {
    this.reactNativeHost = null;
    this.application = application;
    mConfig = config;
  }

  private ReactNativeHost getReactNativeHost() {
    return this.reactNativeHost;
  }

  private Resources getResources() {
    return this.getApplication().getResources();
  }

  private Application getApplication() {
    if (this.reactNativeHost == null) return this.application;
    return this.reactNativeHost.getApplication();
  }

  private Context getApplicationContext() {
    return this.getApplication().getApplicationContext();
  }

  public ArrayList<ReactPackage> getPackages() {
    return new ArrayList<>(Arrays.<ReactPackage>asList(
      new MainReactPackage(mConfig),
      new AsyncStoragePackage(),
      new NetInfoPackage(),
      new ReactNativeAudioPackage(),
      new RNAudioRecorderPlayerPackage(),
      new RNDeviceInfo(),
      new DocumentPickerPackage(),
      new FBSDKPackage(),
      new RNFileViewerPackage(),
      new RNFirebasePackage(),
      new RNFSPackage(),
      new RNGeocoderPackage(),
      new RNGestureHandlerPackage(),
      new ReactNativeGetLocationPackage(),
      new PickerPackage(),
      new ImagePickerPackage(),
      new KCKeepAwakePackage(),
      new LinearGradientPackage(),
      new ReactNativeLocalizationPackage(),
      new RNLocationPackage(),
      new LocationSwitchPackage(),
      new OrientationPackage(),
      new ReactNativePushNotificationPackage(),
      new ReanimatedPackage(),
      new RNSoundPackage(),
      new RNSoundRecorderPackage(),
      new SplashScreenReactPackage(),
      new RNThumbnailPackage(),
      new RNTooltipsPackage(),
      new VectorIconsPackage(),
      new RNVersionInfoPackage(),
      new ReactVideoPackage(),
      new RNFetchBlobPackage()
    ));
  }
}
