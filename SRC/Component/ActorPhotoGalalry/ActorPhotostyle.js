import * as AppConstants from '../Helper/AppConstants';

export default {
    container: {
        flex: 1,
    },
   
    fullImageStyle: {
        height: AppConstants.getDeviceHeight(85),
        width: AppConstants.getDeviceWidth(100),
        marginTop: AppConstants.getDeviceHeight(2),
    },
    modelStyle: {
        flex: 1,
        backgroundColor: AppConstants.COLORS.BLACK,
    },

    videoimage: {
        width: AppConstants.getDeviceHeight(12),
        height: AppConstants.getDeviceHeight(12),
        marginLeft: AppConstants.getDeviceWidth(3),
        justifyContent: "center",
        flexDirection: 'row',
        marginTop: AppConstants.getDeviceHeight(2),
    },
    profiletext2: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        marginLeft: AppConstants.getDeviceWidth(4),
        marginBottom: AppConstants.getDeviceHeight(0.5),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        fontWeight: '500',
    },
    profiletext: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        marginLeft: AppConstants.getDeviceWidth(4),
        marginBottom: AppConstants.getDeviceHeight(0.5),
        marginTop:AppConstants.getDeviceHeight(2),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        fontWeight: '500',
    },
    videoimage1: {
        width: AppConstants.getDeviceHeight(12),
        height: AppConstants.getDeviceHeight(12),
        backgroundColor: AppConstants.COLORS.PROFILETEXINPUT,
        justifyContent: "center",
        alignSelf: 'center',
    },
    addbutton: {
        width: AppConstants.getDeviceHeight(9),
        height: AppConstants.getDeviceHeight(9),
        justifyContent: "center",
        flexDirection: 'row'
    },
    viewerorr: {
        flex: 1,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        justifyContent: 'center',
        marginBottom: AppConstants.getDeviceHeight(8)
    },
    error: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        marginTop: 0,
        textAlign: 'center',
        fontWeight: '500',
    },
};
