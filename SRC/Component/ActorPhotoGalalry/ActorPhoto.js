import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    Modal,
    Alert,
    ActivityIndicator,
    StatusBar,
    Image,
} from 'react-native';
import styles from './ActorPhotostyle'
import AsyncStorage from '@react-native-community/async-storage';
import * as AppConstants from '../Helper/AppConstants';
import OfflineNotice from '../Helper/OfflineNotice';
import { PostData, internet } from '../WebServices/webAPIRequest';
import API from '../WebServices/API';
import Iconss from "react-native-vector-icons/MaterialIcons";
import ImageViewer from 'react-native-image-zoom-viewer';

export default class ActorPhoto extends Component {
    constructor(props) {
        super(props);
        this.state = {
            imageuri: '',
            ModalVisibleStatus: false,
            dataSource: [],
            isLoading: false,
            isLoading1: false,
            imaagearray: [],
            index: '',
            imageone: '',
            imagetwo: '',
            imagethree: '',
            imagefour: '',
            imagename: '',
            name: '',
        };
        this.lists = [
            this.props.navigation.addListener('didFocus', () => { this.componentDidFocus() })
        ];
    }

    ShowModalFunction(visible, imageURL, imagemame) {
        this.setState({
            ModalVisibleStatus: visible,
            imageuri: imageURL,
            imagename: imagemame
        });
    }
    componentDidFocus() {
        this.ProfielGet();
    }
    ProfielGet() {
        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({
                actor_id: this.props.navigation.state.params.artistid,
            });
            PostData(API.ViewartistProfile, APIData)
                .then((responseJson) => {
                    console.log(responseJson)
                    this.setState({ isLoading: false, dataSource: [], imageone: '', imagetwo: '', imagethree: '', imagefour: '' });
                    if (responseJson.statusCode == 1) {
                        var name = responseJson.name + "'s Photos";
                        this.setState({ name: name });
                        responseJson.actor_images.map((y) => {
                            if (y.image_type == 'Close Up') {
                                this.setState({
                                    imageone: y.actor_image,
                                });
                            }
                            if (y.image_type == 'Medium') {
                                this.setState({
                                    imagetwo: y.actor_image,
                                });
                            }

                            if (y.image_type == 'Full') {
                                this.setState({
                                    imagethree: y.actor_image,
                                });
                            }

                            if (y.image_type == 'Other') {
                                this.setState({
                                    imagefour: y.actor_image,
                                });
                            }

                        });

                    }
                    else if (responseJson.statusCode == 5) {
                        Alert.alert('CastPro', responseJson.message, [
                            {
                                text: 'OK', onPress: () => {
                                    AsyncStorage.clear();
                                    this.props.navigation.navigate(AppConstants.SCREENS.LOGIN)
                                }
                            },
                        ],
                            { cancelable: false });
                    }
                    else {
                        Alert.alert(responseJson.message);
                    }

                })
                .catch((error) => {
                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }

    }

    render() {
        if (this.state.isLoading) {
            return AppConstants.ShowActivityIndicator();
        }
        if (this.state.ModalVisibleStatus) {
            return (
                <Modal
                    transparent={false}
                    animationType={'fade'}
                    closeOnTouchOutside={false}
                    visible={this.state.ModalVisibleStatus}
                    onRequestClose={() => {
                        this.ShowModalFunction(!this.state.ModalVisibleStatus, '');
                    }}>
                    <View style={styles.modelStyle}>
                        <Iconss name="cancel" onPress={() => {

                            this.ShowModalFunction(!this.state.ModalVisibleStatus, '')
                        }} color="white" style={{ marginTop: AppConstants.getDeviceHeight(5), marginLeft: AppConstants.getDeviceWidth(4) }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS25)} />

                        <Text style={{ justifyContent: 'center', marginTop: AppConstants.getDeviceHeight(5), marginLeft: AppConstants.getDeviceHeight(23), position: 'absolute', color: AppConstants.COLORS.WHITE }}>{this.state.imagename}</Text>
                        <ImageViewer style={styles.fullImageStyle} imageUrls={[{
                            url: this.state.imageuri,
                            props: {
                            }
                        }]} />
                    </View>
                </Modal>
            );
        } else {
            return (
                <View style={styles.container}>
                    <StatusBar barStyle="light-content" backgroundCopr={AppConstants.SHADOWCOLORS.INTRO} animated translucent></StatusBar>
                    <OfflineNotice />
                    <View style={styles.container}>
                        <View style={{ width: AppConstants.getDeviceWidth(100), height: AppConstants.getDeviceHeight(8), flexDirection: 'row' }}>
                            <Text style={{
                                marginLeft: AppConstants.getDeviceWidth(5),
                                fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS22),
                                fontWeight: '600',
                                fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
                                alignSelf: 'center',
                                color: AppConstants.COLORS.PROFILEINACTIVE,
                            }}>{this.state.name}</Text>
                        </View>

                        {this.state.imageone == '' ? null : <View>
                            <Text style={styles.profiletext}>Close Up</Text>
                            <View style={styles.videoimage}>
                                <TouchableOpacity onPress={() => { this.ShowModalFunction(true, this.state.imageone, 'Close Up') }} style={styles.addbutton}>
                                    <Image style={styles.videoimage1} source={{ uri: this.state.imageone }} />
                                </TouchableOpacity>
                            </View>
                        </View>}
                        {this.state.imagetwo == '' ? null : <View>
                            <Text style={styles.profiletext2}>Medium</Text>
                            <View style={styles.videoimage}>
                                <TouchableOpacity onPress={() => { this.ShowModalFunction(true, this.state.imagetwo, 'Medium') }} style={styles.addbutton}>
                                    <Image style={styles.videoimage1} source={{ uri: this.state.imagetwo }} />
                                </TouchableOpacity>
                            </View>
                        </View>}

                        {this.state.imagethree == '' ? null : <View>
                            <Text style={styles.profiletext2}>Full</Text>
                            <View style={styles.videoimage}>
                                <TouchableOpacity onPress={() => { this.ShowModalFunction(true, this.state.imagethree, 'Full') }} style={styles.addbutton}>
                                    <Image style={styles.videoimage1} source={{ uri: this.state.imagethree }} />
                                </TouchableOpacity>
                            </View>
                        </View>}
                        {this.state.imagefour == '' ? null : <View>
                            <Text style={styles.profiletext2}>Other</Text>
                            <View style={styles.videoimage}>
                                <TouchableOpacity onPress={() => { this.ShowModalFunction(true, this.state.imagefour, 'Other') }} style={styles.addbutton} >
                                    <Image style={styles.videoimage1} source={{ uri: this.state.imagefour }} />
                                </TouchableOpacity>
                            </View>
                        </View>}

                    </View>
                    {this.state.imagefour == '' || this.state.imagethree == '' || this.state.imagetwo == '' || this.state.imageone == '' ?
                        <View style={styles.viewerorr}>
                            <Text style={styles.error}>No Photos Added!</Text>
                        </View> : null}
                    {this.state.isLoading1 ? <View style={AppConstants.CommonStyles.spinner}>
                        <ActivityIndicator size='large' color={AppConstants.COLORS.SIGNUP}></ActivityIndicator>
                    </View> : null}
                </View>
            );
        }
    }
}

