import React, { Component } from 'react';

import {
    TextInput,
    Image,
    ScrollView,
    View,
    Text,
    StatusBar,
    ImageBackground,
    TouchableOpacity,
    Platform,
    Alert
} from 'react-native';
import styles from './MoreStyle'
import * as AppConstants from '../Helper/AppConstants';
import OfflineNotice from '../Helper/OfflineNotice';
import Toast, { DURATION } from 'react-native-easy-toast'
import Icons from "react-native-vector-icons/FontAwesome";
import Iconsss from "react-native-vector-icons/MaterialIcons";
import Iconss from "react-native-vector-icons/MaterialCommunityIcons";
import AsyncStorage from '@react-native-community/async-storage';
import { LoginManager } from 'react-native-fbsdk';
import VersionInfo from 'react-native-version-info';

const Images = {
    TabprofielIcon: 'tabprofile',
    Profilerequest: 'profilerequest',
    Shortlist: 'shortlist',
    Auditionrequest: 'audition'
}
export default class More extends Component {
    constructor(props) {
        super(props);


    }
    componentDidMount() {
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <StatusBar barStyle="light-content" backgroundCopr={AppConstants.SHADOWCOLORS.INTRO} animated translucent></StatusBar>
                <OfflineNotice />
                <View style={styles.mainContainer}>
                    <Toast
                        ref="toast"
                        style={[AppConstants.CommonStyles.ToastStyle1, { justifyContent: 'flex-end', height: AppConstants.getDeviceHeight(11), }]}
                        position="top"
                        positionValue={AppConstants.getDeviceHeight(0)}
                        fadeInDuration={750}
                        fadeOutDuration={1000}
                        textStyle={AppConstants.CommonStyles.ToastTextStyle} />


                    <TouchableOpacity style={styles.titleview} onPress={() => { this.props.navigation.navigate(AppConstants.SCREENS.PROFILEREQIESTLIST) }} >
                        <View style={{ width: AppConstants.getDeviceHeight(10), height: AppConstants.getDeviceHeight(8), justifyContent: 'center', }}>
                            <Image source={{ uri: Images.Profilerequest }} style={{ width: AppConstants.getDeviceHeight(4), height: AppConstants.getDeviceHeight(4), alignSelf: 'center' }}>
                            </Image>
                        </View>
                        <Text style={[styles.text1]}>Sent Profile Requests</Text>
                        <Icons name="angle-right" color={AppConstants.COLORS.PROFILEINACTIVE} style={{ position: 'absolute', right: 0, marginRight: AppConstants.getDeviceWidth(5), alignSelf: 'center' }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS28)} />
                    </TouchableOpacity>
                    <View style={styles.line}></View>

                    <TouchableOpacity style={styles.titleview} onPress={() => { this.props.navigation.navigate(AppConstants.SCREENS.AUDITIONREQUESTLIST) }}>
                        <View style={{ width: AppConstants.getDeviceHeight(10), height: AppConstants.getDeviceHeight(8), justifyContent: 'center', }}>
                            <Image source={{ uri: Images.Auditionrequest }} style={{ width: AppConstants.getDeviceHeight(4), height: AppConstants.getDeviceHeight(4), alignSelf: 'center' }}>
                            </Image>
                        </View>
                        <Text style={[styles.text1]}>Sent Audition Requests</Text>
                        <Icons name="angle-right" color={AppConstants.COLORS.PROFILEINACTIVE} style={{ position: 'absolute', right: 0, marginRight: AppConstants.getDeviceWidth(5), alignSelf: 'center' }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS28)} />
                    </TouchableOpacity>

                    <View style={styles.line}></View>
                    <TouchableOpacity style={styles.titleview} onPress={() => { this.props.navigation.navigate(AppConstants.SCREENS.SHORTLIST) }}>
                        <View style={{ width: AppConstants.getDeviceHeight(10), height: AppConstants.getDeviceHeight(8), justifyContent: 'center', }}>
                            <Image source={{ uri: Images.Shortlist }} style={{ width: AppConstants.getDeviceHeight(4), height: AppConstants.getDeviceHeight(4), alignSelf: 'center' }}>
                            </Image>
                        </View>
                        <Text style={[styles.text1]}>Shortlisted Auditions</Text>
                        <Icons name="angle-right" color={AppConstants.COLORS.PROFILEINACTIVE} style={{ position: 'absolute', right: 0, marginRight: AppConstants.getDeviceWidth(5), alignSelf: 'center' }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS28)} />
                    </TouchableOpacity>
                    <View style={styles.line}></View>
                    <TouchableOpacity style={styles.titleview} onPress={() => { this.props.navigation.navigate(AppConstants.SCREENS.CHANGEPASSWORD) }}>
                        <View style={{ width: AppConstants.getDeviceHeight(10), height: AppConstants.getDeviceHeight(8), justifyContent: 'center', }}>
                            <Iconss name="lock-outline" color={AppConstants.COLORS.BLACK} style={{ alignSelf: 'center' }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS28)} />
                        </View>
                        <Text style={[styles.text1]}>Change Password</Text>
                        <Icons name="angle-right" color={AppConstants.COLORS.PROFILEINACTIVE} style={{ position: 'absolute', right: 0, marginRight: AppConstants.getDeviceWidth(5), alignSelf: 'center' }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS28)} />
                    </TouchableOpacity>
                    <View style={styles.line}></View>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate(AppConstants.SCREENS.ABOUTUS) }} style={styles.titleview}>
                        <View style={{ width: AppConstants.getDeviceHeight(10), height: AppConstants.getDeviceHeight(8), justifyContent: 'center', }}>
                            <Image source={{ uri: Images.TabprofielIcon }} style={{ width: AppConstants.getDeviceHeight(4), height: AppConstants.getDeviceHeight(4), alignSelf: 'center' }}>
                            </Image>
                        </View>
                        <Text style={[styles.text1]}>About Us</Text>
                        <Icons name="angle-right" color={AppConstants.COLORS.PROFILEINACTIVE} style={{ position: 'absolute', right: 0, marginRight: AppConstants.getDeviceWidth(5), alignSelf: 'center' }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS28)} />
                    </TouchableOpacity>
                    <View style={styles.line}></View>


                    <TouchableOpacity style={styles.titleview} onPress={() => {
                        Alert.alert(
                            'Logout',
                            'Are you sure you want to log out?',
                            [
                                {
                                    text: 'Cancel',
                                    onPress: () => { },
                                    style: 'cancel',
                                },
                                {
                                    text: 'OK', onPress: () => {
                                        AsyncStorage.clear();
                                        LoginManager.logOut();
                                        global.token = '';
                                        this.props.navigation.replace(AppConstants.SCREENS.LOGIN);
                                    }
                                },
                            ],
                            { cancelable: false },
                        );
                    }}>
                        <View style={{ width: AppConstants.getDeviceHeight(10), height: AppConstants.getDeviceHeight(8), justifyContent: 'center', }}>
                            <Iconss name="logout" color={AppConstants.COLORS.BLACK} style={{ alignSelf: 'center' }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS26)} />
                        </View>
                        <Text style={[styles.text1]}>Log Out</Text>
                        <Icons name="angle-right" color={AppConstants.COLORS.PROFILEINACTIVE} style={{ position: 'absolute', right: 0, marginRight: AppConstants.getDeviceWidth(5), alignSelf: 'center' }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS28)} />
                    </TouchableOpacity>
                    <View style={styles.line}></View>
                </View>
                <Text style={styles.version}>Version {VersionInfo.appVersion} </Text>
            </View>
        );
    }
}