import * as AppConstants from '../Helper/AppConstants';
export default {
    mainContainer: {
        flex: 1,
    },
    line: {
        height: AppConstants.getDeviceHeight(0.2),
        backgroundColor: AppConstants.COLORS.HEDARBACKGROUND,
        width: AppConstants.getDeviceWidth(100),
        marginTop: AppConstants.getDeviceHeight(1)
    },
    text: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS16),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        fontWeight: 'bold',
        marginLeft: AppConstants.getDeviceWidth(4),
        alignSelf: 'center'
    },
    text1: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS16),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        fontWeight: 'bold',
        marginLeft: AppConstants.getDeviceWidth(3),
        alignSelf: 'center'
    },
    titleview: {
        width: AppConstants.getDeviceWidth(100),
        height: AppConstants.getDeviceHeight(7),
        flexDirection: 'row'
    },
    version:{
        position: 'absolute', bottom: 0, 
        marginBottom: AppConstants.getDeviceHeight(10), 
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.datecolor,

        marginLeft: AppConstants.getDeviceWidth(3),
        alignSelf: 'center'
    }

}