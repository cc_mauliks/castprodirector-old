import React, { Component } from 'react';
import {
    TextInput,
    Image,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    Alert,
    Keyboard,
    NativeModules,
    AsyncStorage,
    ActivityIndicator
} from 'react-native';
import styles from './DirectorprofileStyle'
import * as AppConstants from '../Helper/AppConstants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import OfflineNotice from '../Helper/OfflineNotice';
import Toast, { DURATION } from 'react-native-easy-toast'
import { PostData } from '../WebServices/webAPIRequest';
import ImagePicker from "react-native-image-picker";
import API from "../WebServices/API";
import TagProject from '../Taginput'
import RNFetchBlob from 'rn-fetch-blob'
var ImagePicker1 = NativeModules.ImageCropPicker;
import DeviceInfo, { getUniqueId } from "react-native-device-info";
import { Dropdown } from 'react-native-material-dropdown';
import Iconss from "react-native-vector-icons/AntDesign";
import Permissions from "react-native-permissions";

const { PlatformConstants } = NativeModules;
const Images = {
    Profileimage: 'profileimage',
}
const options = {
    quality: 1.0,
    // mediaType: 'photo',
    cameraType: 'front',
    maxWidth: 1024,
    maxHeight: 1024,
    storageOptions: {
        skipBackup: true
    }
};
const inputProps = {
    keyboardType: "default",
    autoFocus: true,
    style: {
        fontSize: 14,
        marginVertical: Platform.OS == "ios" ? 10 : -2
    }
};
export default class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ImageSource: Images.Profileimage,
            ImageSource1: false,
            name: global.firstname,
            Lastname: global.lastname,
            name1: false,
            Lastname1: false,
            work_link: '',
            tags: [],
            tags1: false,
            text: '',
            mobileno: '',
            mobileno1: false,
            companyname: '',
            companyname1: false,
            emailid: '',
            emailid1: false,
            profile: '',
            profile1: false,
            Description: '',
            Description1: false,
            Base: '',
            devicetype: '',
            isLoading: false,
            isLoading1: false,
            countrycode: [],
            selectcountrycode: '',
            uniqueId: '',
            Statearray: [],
            city: [],
            statename: '',
            statename1: false,
            cityname: '',
            cityname1: false,
        }
    }
    componentWillMount() {
        this.GetState();
        this.setState({ deviceType: Platform.OS == 'ios' ? PlatformConstants.interfaceIdiom : DeviceInfo.getType() });
    }
    labelExtractor = (tag) => tag;
    onChangeText = (text) => {
        this.setState({ text });
        const lastTyped = text.charAt(text.length - 1);
        const parseWhen = [',', ' ', ';', '\n'];
        if (parseWhen.indexOf(lastTyped) > -1) {
            this.setState({
                tags: [...this.state.tags, this.state.text],
                text: "",
            });
        }
    }
    tag = () => {
        if (this.state.work_link == '') {
            this.refs.toast.show(AppConstants.Messages.LINK, DURATION.LENGTH_LONG);
        }
        else if (this.state.Description == '') {
            this.refs.toast.show(AppConstants.Messages.DESCRIPTION, DURATION.LENGTH_LONG);
        }
        else {
            var joined = this.state.tags.concat({ work_link: this.state.work_link, work_description: this.state.Description });
            this.setState({
                tags: joined,
                Userinterests: '',
                work_link: '',
            });
        }
    }
    onChangeTags = (tags) => {
        this.setState({ tags });
    }
    componentWillUnmount() {
        clearTimeout(this.timeoutHandle);
    }
    validator() {
        if (this.state.ImageSource == Images.Profileimage) {
            this.setState({ ImageSource1: true })
            //this.refs.toast.show(AppConstants.Messages.NOPROFILEPHOTO, DURATION.LENGTH_LONG);
        }
        else if (this.state.name == '') {
            this.setState({ name1: true })
            // this.refs.toast.show(AppConstants.Messages.FIRSTNAME, DURATION.LENGTH_LONG);
        }
        else if (this.state.Lastname == '') {
            this.setState({ Lastname1: true })
            // this.refs.toast.show(AppConstants.Messages.LASTNAME, DURATION.LENGTH_LONG);
        }
        else if (this.state.companyname == '') {
            this.setState({ companyname1: true })
            // this.refs.toast.show(AppConstants.Messages.COMPANYNAME, DURATION.LENGTH_LONG);
        }
        else if (this.state.mobileno == '') {
            this.setState({ mobileno1: true })
            // this.refs.toast.show(AppConstants.Messages.MOBILENONOTANUMBER, DURATION.LENGTH_LONG);
        }
        else if (this.state.emailid == '') {
            this.setState({ emailid1: true })
            // this.refs.toast.show(AppConstants.Messages.NOEMAIL, DURATION.LENGTH_LONG);
        }
        else if (this.state.statename == '') {
            this.setState({ statename1: true })
            // this.refs.toast.show(AppConstants.Messages.STATE, DURATION.LENGTH_LONG);
        }
        else if (this.state.cityname == '') {
            this.setState({ cityname1: true })
            // this.refs.toast.show(AppConstants.Messages.CITY, DURATION.LENGTH_LONG);
        }
        else if (this.state.profile == '') {
            this.setState({ profile1: true })
            // this.refs.toast.show(AppConstants.Messages.PROFILE, DURATION.LENGTH_LONG);
        }
        // else if (this.state.tags == '') {
        //     this.setState({ tags1: true })
        //     // this.refs.toast.show(AppConstants.Messages.WORKLINK, DURATION.LENGTH_LONG);
        // }
        else {
            this.ProfileAdd();
        }
    }
    GetCountry() {
        APIData = JSON.stringify({
        });
        PostData(API.Getcountrycode, APIData)
            .then((responseJson) => {
                this.setState({ isLoading: false });
                var arr = [];
                responseJson.data.map((data) => {
                    arr.push({
                        value: data.country_code,
                        coutryname: data.country_name
                    })
                });

                if (AppConstants.countryname != '' && this.state.selectcountrycode == '' || this.state.selectcountrycode == null) {
                    this.setState({ selectcountrycode: arr.filter(item => item.coutryname == AppConstants.countryname)[0].value })
                }
                this.setState({ countrycode: arr });
            }).catch(() => {
                this.setState({ isLoading: false });
                Alert.alert(AppConstants.Messages.APIERROR);
            });
    }
    ProfielGet() {
        this.setState({ isLoading: true });
        APIData = JSON.stringify({
        });
        PostData(API.Directorgetprofile, APIData)
            .then((responseJson) => {
                if (responseJson.statusCode == 1) {
                    this.setState({
                        name: responseJson.userData.first_name,
                        Lastname: responseJson.userData.surname,
                        companyname: responseJson.userData.company_name,
                        mobileno: responseJson.userData.mobile_no,
                        emailid: responseJson.userData.email_address,
                        profile: responseJson.userData.profile,
                        tags: responseJson.userData.key_projects,
                        selectcountrycode: responseJson.userData.country_code,
                        statename: responseJson.userData.state,
                        cityname: responseJson.userData.city,
                    });
                    if (responseJson.userData.profile_image == '') {
                        this.setState({ ImageSource: Images.Profileimage })
                    }
                    else {
                        this.setState({ ImageSource: responseJson.userData.profile_image });
                    }
                    RNFetchBlob.config({ fileCache: true }).fetch("GET", responseJson.userData.profile_image)
                        .then(resp => {
                            imagePath = resp.path();
                            return resp.readFile("base64");
                        }).then(base64Data => {
                            this.setState({ Base: base64Data });
                            return fs.unlink(imagePath);
                        });
                    this.GetCountry();
                }
                else {
                    Alert.alert(responseJson.message);
                }
            }).catch(() => {
                this.setState({ isLoading: false });
                Alert.alert(AppConstants.Messages.APIERROR);
            });
    }
    ProfileAdd() {
        Keyboard.dismiss();
        if (global.isConnected) {
            this.setState({ isLoading1: true });
            APIData = JSON.stringify({
                email_address: this.state.emailid,
                mobile_no: this.state.mobileno,
                first_name: this.state.name,
                surname: this.state.Lastname,
                company_name: this.state.companyname,
                profile: this.state.profile,
                device_id: AppConstants.DeviceID,
                plaform_type: (Platform.OS === 'ios' ? 'Android' : 'iOS'),
                device_type: 'phone',
                key_projects: this.state.tags,
                profile_image: this.state.Base,
                state: this.state.statename,
                city: this.state.cityname,
                country_code: this.state.selectcountrycode
            });
            PostData(API.Directorupdateprofile, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading1: false });
                    if (responseJson.statusCode == 1) {
                        this.refs.toast1.show(responseJson.message, DURATION.LENGTH_SHORT, true);
                        this.timeoutHandle = setTimeout(() => {
                            this.props.navigation.navigate(AppConstants.SCREENS.DIRECTORPROFILE)
                        }, 2000);
                    }
                    else {
                        Alert.alert(responseJson.message);
                    }
                }).catch(() => {
                    this.setState({ isLoading1: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }
    }
    takePhoto() {
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
            } else if (response.error) {
                Permissions.check("camera").then(response => {
                    if (response == "denied") {
                        Permissions.request("camera").then(response => {
                            if (response == "denied") {
                                Alert.alert(
                                    "Please allow Permissions to access Camera from Settings -> Apps -> Castpro -> Camera"
                                );
                            }
                        });
                    }
                });
            } else if (response.customButton) {
            } else {
                if (response.data != null) {
                    ImagePicker1.openCropper({
                        path: response.uri,
                        width: 300,
                        mediaType: 'photo',
                        height: 400,
                        // compressImageQuality: 0.3,
                        includeBase64: true,
                    }).then(image => {
                        var base64Icon = 'data:image/png;base64,' + image.data;
                        this.setState({
                            Base: image.data,
                            ImageSource: base64Icon,
                            ImageSource1: false
                        });
                        ImagePicker1.clean().then(() => {
                        }).catch(e => {
                            alert(e);
                        });
                    });
                }
            }
        });
    }
    GetState() {
        Keyboard.dismiss();
        if (global.isConnected) {
            APIData = JSON.stringify({
                action: 'state',
                state_id: '0'
            });
            PostData(API.GetStateAndCity, APIData)
                .then((responseJson) => {
                    if (responseJson.statusCode == 1) {
                        var arr = [];
                        responseJson.data.map((data) => {
                            arr.push({
                                value: data.title,
                                id: data.id
                            })
                        });
                        this.setState({ Statearray: arr });
                        this.ProfielGet();
                    }
                    else {
                        Alert.alert(responseJson.message);
                    }
                }).catch(() => {
                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }
    }
    Getcity(cityid) {
        if (global.isConnected) {
            this.setState({ isLoading1: true });
            APIData = JSON.stringify({
                action: 'city',
                state_id: cityid
            });
            PostData(API.GetStateAndCity, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading1: false })
                    if (responseJson.statusCode == 1) {
                        var arr = [];
                        responseJson.data.map((data) => {
                            arr.push({
                                value: data.title,
                                id: data.id
                            });
                        });
                        this.setState({ city: arr });
                    }
                    else {
                        Alert.alert(responseJson.message);
                    }
                }).catch(() => {
                    this.setState({ isLoading1: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }
    }
    render() {
        if (this.state.isLoading) {
            return AppConstants.ShowActivityIndicator();
        }
        return (
            <KeyboardAwareView doNotForceDismissKeyboardWhenLayoutChanges={true}>
                <StatusBar barStyle="light-content" backgroundCopr={AppConstants.SHADOWCOLORS.INTRO} animated translucent></StatusBar>
                <OfflineNotice />
                <View style={styles.mainContainer}>
                    <Toast
                        ref="toast"
                        style={[AppConstants.CommonStyles.ToastStyle, { justifyContent: 'flex-end', height: AppConstants.getDeviceHeight(8), }]}
                        position="top"
                        positionValue={AppConstants.getDeviceHeight(0)}
                        fadeInDuration={750}
                        fadeOutDuration={1000}
                        textStyle={AppConstants.CommonStyles.ToastTextStyle} />
                    <Toast
                        ref="toast1"
                        style={[AppConstants.CommonStyles.ToastStyle2, { height: AppConstants.getDeviceHeight(6.5), }]}
                        position='bottom'
                        positionValue={200}
                        fadeInDuration={1000}
                        fadeOutDuration={1000}
                        textStyle={AppConstants.CommonStyles.ToastTextStyle1} />
                    <ScrollView contentContainerStyle={{ flexGrow: 1, }} style={{ marginBottom: AppConstants.getDeviceHeight(1) }}>
                        <View style={styles.profileview}>
                            <TouchableOpacity style={styles.profileview} onPress={() => {
                                Permissions.request("photo").then(response => {
                                    if (response == "denied") {
                                        Alert.alert(
                                            "Please allow Permissions to access Camera from Settings -> Apps -> Castpro -> Camera"
                                        );
                                    }
                                    else {
                                        this.takePhoto()
                                    }
                                });

                            }}>
                                <Image style={styles.profile} source={{ uri: this.state.ImageSource }} ></Image>
                                <Text style={styles.profiletext}>{this.state.name + ' ' + this.state.Lastname}</Text>
                            </TouchableOpacity>
                        </View>
                        {this.state.ImageSource1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(7) }]}>{AppConstants.Messages.NOPROFILEPHOTO}</Text> : null}

                        <Text style={styles.profiletext2}>First Name</Text>
                        <TextInput style={styles.textboxText}
                            placeholderTextColor={AppConstants.COLORS.PLACEHOLDAR}
                            underlineColorAndroid="transparent"
                            blurOnSubmit={false}
                            autoCapitalize="none"
                            maxLength={20}
                            value={this.state.name}
                            onChangeText={(name) => this.setState({ name, name1: false })} />
                        {this.state.name == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(7) }]}>{AppConstants.Messages.FIRSTNAME}</Text> : null}

                        <Text style={styles.profiletext2}>Surname</Text>
                        <TextInput
                            style={styles.textboxText}
                            placeholderTextColor={AppConstants.COLORS.PLACEHOLDAR}
                            underlineColorAndroid="transparent"
                            value={this.state.Lastname}
                            blurOnSubmit={false}
                            autoCapitalize="none"
                            maxLength={20}
                            onChangeText={(Lastname) => this.setState({ Lastname, Lastname1: false })} />
                        {this.state.Lastname1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(7) }]}>{AppConstants.Messages.LASTNAME}</Text> : null}

                        <Text style={styles.profiletext2}>Company / Production House Name</Text>
                        <TextInput style={styles.textboxText}
                            placeholderTextColor={AppConstants.COLORS.PLACEHOLDAR}
                            underlineColorAndroid="transparent"
                            value={this.state.companyname}
                            blurOnSubmit={false}
                            autoCapitalize="none"
                            onChangeText={(companyname) => this.setState({ companyname, companyname1: false })} />

                        {this.state.companyname1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(7) }]}>{AppConstants.Messages.COMPANYNAME}</Text> : null}

                        <Text style={styles.profiletext2}>Mobile No</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <View>
                                <Dropdown
                                    containerStyle={styles.Countrycode}
                                    customStyles={{ placeholderText: { fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.PLANVIEWTEXT), }, }}
                                    value={this.state.selectcountrycode}
                                    itemTextStyle={{ textAlign: 'center' }}
                                    labelHeight={0}
                                    disabled={!this.state.edit}
                                    style={styles.dropdown1}
                                    inputContainerStyle={{ borderBottomColor: "transparent", height: AppConstants.getDeviceHeight(5.5), justifyContent: 'center', }}
                                    labelPadding={0}
                                    renderAccessory={() => <Iconss name="down" style={styles.dropdownicon} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS14)} />}
                                    data={this.state.countrycode}
                                    onChangeText={selectcountrycode => {
                                        this.setState({ selectcountrycode, selectcountrycode1: false });
                                    }} />
                                <View style={{
                                    position: 'absolute', backgroundColor: AppConstants.COLORS.PROFILETEXINPUT, width: AppConstants.getDeviceWidth(23),
                                    height: AppConstants.getDeviceHeight(0.5), marginLeft: AppConstants.getDeviceWidth(7), bottom: 0
                                }}></View>
                                {this.state.selectcountrycode1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(7) }]}>{AppConstants.Messages.NOPROFILEPHOTO}</Text> : null}

                            </View>
                            <View>
                                <TextInput
                                    style={styles.textboxTextphonenumber}
                                    placeholderTextColor={AppConstants.COLORS.PLACEHOLDAR}
                                    underlineColorAndroid="transparent"
                                    editable={false}
                                    blurOnSubmit={false}
                                    keyboardType={"phone-pad"}
                                    autoCapitalize="none"
                                    maxLength={20}
                                    value={this.state.mobileno}
                                    onChangeText={(mobileno) => this.setState({ mobileno, mobileno: false })} />
                                {this.state.mobileno == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(7) }]}>{AppConstants.Messages.MOBILENONOTANUMBER}</Text> : null}

                            </View>
                        </View>
                        <Text style={styles.profiletext2}>Email Id</Text>
                        <TextInput style={styles.textboxText}
                            placeholderTextColor={AppConstants.COLORS.PLACEHOLDAR}
                            underlineColorAndroid="transparent"
                            editable={false}
                            value={this.state.emailid}
                            blurOnSubmit={false}
                            autoCapitalize="none"

                            onChangeText={(emailid) => this.setState({ emailid, emailid1: false })} />
                        {this.state.emailid1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(7) }]}>{AppConstants.Messages.NOEMAIL}</Text> : null}

                        <Text style={styles.profiletext2}>State</Text>
                        <Dropdown
                            containerStyle={styles.dropdowncontainer}
                            customStyles={{ placeholderText: { fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.PLANVIEWTEXT), }, }}
                            value={this.state.statename}
                            labelHeight={0}
                            itemTextStyle={{ textAlign: 'center' }}
                            style={styles.dropdonw}
                            inputContainerStyle={{ borderBottomColor: "transparent", justifyContent: 'center', }}
                            labelPadding={0}
                            renderAccessory={() => <Iconss name="down" style={styles.dropdownicon} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS14)} />}
                            data={this.state.Statearray}
                            onChangeText={(statename, index) => {
                                let no = index + 1;
                                this.Getcity(no);
                                this.setState({ statename: statename, cityname: '', statename1: false });
                            }} />
                        {this.state.statename1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(7) }]}>{AppConstants.Messages.STATE}</Text> : null}

                        <Text style={styles.profiletext2}>City</Text>
                        <Dropdown
                            containerStyle={styles.dropdowncontainer}
                            customStyles={{ placeholderText: { fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.PLANVIEWTEXT), }, }}
                            value={this.state.cityname}
                            labelHeight={0}
                            style={styles.dropdonw}
                            inputContainerStyle={{ borderBottomColor: "transparent" }}
                            labelPadding={0}
                            renderAccessory={() => <Iconss name="down" style={styles.dropdownicon} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS14)} />}
                            data={this.state.city}
                            onChangeText={cityname => {
                                this.setState({ cityname, cityname1: false });
                            }} />
                        {this.state.cityname1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(7) }]}>{AppConstants.Messages.CITY}</Text> : null}

                        <Text style={styles.profiletext2}>Profile</Text>
                        <TextInput style={styles.textboxText1}
                            placeholderTextColor={AppConstants.COLORS.PLACEHOLDAR}
                            underlineColorAndroid="transparent"
                            value={this.state.profile}
                            blurOnSubmit={false}
                            multiline={true}
                            autoCapitalize="none"
                            autoCapitalize="none"
                            onChangeText={(profile) => this.setState({ profile, profile1: false })} />
                        {this.state.profile1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(7) }]}>{AppConstants.Messages.PROFILE}</Text> : null}
                        <View style={styles.taglink}>
                            <TouchableOpacity style={styles.touchdone} onPress={() => { this.validator() }}>
                                <Text style={styles.Signup}>
                                    DONE
                                    </Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
                {this.state.isLoading1 ? <View style={AppConstants.CommonStyles.spinner}>
                    <ActivityIndicator size='large' ></ActivityIndicator>
                </View> : null}
            </KeyboardAwareView>
        );
    }
}