import * as AppConstants from '../Helper/AppConstants';
export default {
    mainContainer: {
        flex: 1,
    },
    profileview: {
        flexDirection: 'row',
        height: AppConstants.getDeviceHeight(15),
    },
    profile: {
        width: AppConstants.getDeviceHeight(13),
        height: AppConstants.getDeviceHeight(13),
        borderRadius: AppConstants.getDeviceHeight(13) / 2,
        marginLeft: AppConstants.getDeviceWidth(5),
        alignSelf: 'center',
    },
    profiletext: {
        alignSelf: 'center',
        marginLeft: AppConstants.getDeviceWidth(3),
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontWeight: '600',
    },
    profiletext2: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        marginLeft: AppConstants.getDeviceWidth(7),
        marginBottom: AppConstants.getDeviceHeight(0.5),
        marginTop: AppConstants.getDeviceHeight(2)
    },
    textboxText: {
        width: AppConstants.getDeviceWidth(86),
        height: AppConstants.getDeviceHeight(5.5),
        color: AppConstants.COLORS.BLACK,
        backgroundColor: AppConstants.COLORS.PROFILETEXINPUT,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS13),
        marginLeft: AppConstants.getDeviceWidth(7),
        marginRight: AppConstants.getDeviceWidth(7),
        paddingLeft: AppConstants.getDeviceWidth(2),
        padding: 0,
        margin: 0,
    },
    textboxTextphonenumber: {
        width: AppConstants.getDeviceWidth(62),
        height: AppConstants.getDeviceHeight(5.5),
        color: AppConstants.COLORS.BLACK,
        backgroundColor: AppConstants.COLORS.PROFILETEXINPUT,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS13),
        marginLeft: AppConstants.getDeviceWidth(1),
        marginRight: AppConstants.getDeviceWidth(7),
        paddingLeft: AppConstants.getDeviceWidth(2),
        justifyContent: 'center',
        padding: 0,
        margin: 0,
    },
    Countrycode: {
        width: AppConstants.getDeviceWidth(23),
        height: AppConstants.getDeviceHeight(5.5),
        marginLeft: AppConstants.getDeviceWidth(7),
        backgroundColor: AppConstants.COLORS.PROFILETEXINPUT,
    },
    textboxText1: {
        width: AppConstants.getDeviceWidth(86),
        height: AppConstants.getDeviceHeight(10),
        color: AppConstants.COLORS.BLACK,
        backgroundColor: AppConstants.COLORS.PROFILETEXINPUT,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS13),
        marginLeft: AppConstants.getDeviceWidth(7),
        marginRight: AppConstants.getDeviceWidth(7),
        paddingLeft: AppConstants.getDeviceWidth(2)
    },
    Signup: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontWeight: '600',
        alignSelf: 'center',
    },
    ProfilebackGround: {
        marginTop: AppConstants.getDeviceHeight(0.4),
        width: AppConstants.getDeviceWidth(100),
        height: AppConstants.getDeviceHeight(40),
        backgroundColor: AppConstants.COLORS.PROFILEBACKGROUND,
        justifyContent: "center",
    },
    imagebackground: {
        width: AppConstants.getDeviceHeight(33),
        height: AppConstants.getDeviceHeight(33),
        alignSelf: 'center',
        justifyContent: "center",
    },
    username: {
        alignSelf: 'center',
        marginTop: AppConstants.getDeviceHeight(1),
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS19),
        fontWeight: 'bold',
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.WHITE,
        textAlign: 'center'
    },
    profiledetail: {
        width: AppConstants.getDeviceWidth(100),
        height: AppConstants.getDeviceHeight(5),
        borderBottomWidth: 0.3,
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    persionaldetai: {
        height: AppConstants.getDeviceHeight(8),
        justifyContent: 'center',
        width: AppConstants.getDeviceWidth(50)
    },
    persionaldetai1: {
        height: AppConstants.getDeviceHeight(8),
        justifyContent: 'center',
        width: AppConstants.getDeviceWidth(50)
    },
    username1: {
        marginTop: 0,
        textAlign: 'center',
    },
    introtext: {
        paddingTop: AppConstants.getDeviceHeight(1),
        marginTop: 0,
        textAlign: 'center',
        marginLeft: AppConstants.getDeviceWidth(3)
    },
    profileselect: {
        width: AppConstants.getDeviceWidth(100),
        height: AppConstants.getDeviceHeight(35),
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    taglink: {
        width: AppConstants.getDeviceWidth(100),
        marginTop: AppConstants.getDeviceHeight(7),
        height: AppConstants.getDeviceHeight(3),
        justifyContent: "center",
    },
    dropdowncontainer: {
        width: AppConstants.getDeviceWidth(86),
        height: AppConstants.getDeviceHeight(5.5),
        marginLeft: AppConstants.getDeviceWidth(7),
        backgroundColor: AppConstants.COLORS.PROFILETEXINPUT,
    },
    touchdone: {
        backgroundColor: AppConstants.COLORS.PROFILEINACTIVE,
        height: AppConstants.getDeviceHeight(5),
        marginBottom: AppConstants.getDeviceHeight(2),
        alignItems: 'center',
        justifyContent: 'center',
        width: AppConstants.getDeviceWidth(30),
        alignSelf: 'center'
    },
    dropdonw: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        paddingLeft: AppConstants.getDeviceWidth(1),
        marginTop: AppConstants.getDeviceHeight(0.5)
    },
    dropdown1: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        paddingLeft: AppConstants.getDeviceWidth(1),
        marginTop: AppConstants.getDeviceHeight(0.5),
        textAlign: 'center',
        paddingRight: AppConstants.getDeviceWidth(3)
    },
    dropdownicon: {
        position: 'absolute',
        right: 0,
        marginRight: AppConstants.getDeviceWidth(2),
        paddingTop: AppConstants.getDeviceHeight(5)
    }
}