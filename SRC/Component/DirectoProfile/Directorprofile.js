import React, { Component } from 'react';
import {
    View,
    Text,
    StatusBar,
    ImageBackground,
    TouchableOpacity,
    Alert
} from 'react-native';
import styles from './DirectorprofileStyle'
import * as AppConstants from '../Helper/AppConstants';
import OfflineNotice from '../Helper/OfflineNotice';
import { PostData, internet } from '../WebServices/webAPIRequest';
import API from "../WebServices/API";
import { Avatar } from 'react-native-elements';
import Toast, { DURATION } from 'react-native-easy-toast'

const Images = {
    Profileicon: 'profileicon',
    Bio: 'bio',
    Photos: 'photo',
    IntroVideo: 'introvideo',
    Worklink: 'worklink',
}

export default class Directorprofile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            picture: Images.Photos,
            name: 'Casting Director',
            agencyname: 'Casting Agency',
            city: 'Mumbai, Maharastra',
            isLoading: false,
            name: ''
        };
        this.lists = [
            this.props.navigation.addListener('didFocus', () => { this.componentDidFocus() })
        ];
    }
    componentDidFocus() {
        this.ProfielGet();
    }
    ProfielGet() {
        this.setState({ isLoading: true });
        if (global.isConnected) {
            APIData = JSON.stringify({
            });
            PostData(API.Directorgetprofile, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false });
                    if (responseJson.statusCode == 1) {
                        var name = responseJson.userData.first_name + ' ' + responseJson.userData.surname
                        var city = '';
                        if (responseJson.userData.city != '') {
                            city = responseJson.userData.city + ', ' + responseJson.userData.state;
                        }
                        this.setState({
                            name: name,
                            picture: responseJson.userData.textboxTextphonenumber_image == '' ? Images.Photos : responseJson.userData.profile_image,
                            agencyname: responseJson.userData.company_name,
                            city: city
                        });
                    } else {
                        Alert.alert(responseJson.message);
                    }
                })
                .catch(() => {
                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }
    }
    render() {
        if (this.state.isLoading) {
            return AppConstants.ShowActivityIndicator();
        }
        return (
            <View style={styles.mainContainer}>
                <StatusBar barStyle="light-content" backgroundCopr={AppConstants.SHADOWCOLORS.INTRO} animated translucent></StatusBar>
                <OfflineNotice />
                <View style={styles.mainContainer}>
                    <Toast
                        ref="toast"
                        style={[AppConstants.CommonStyles.ToastStyle, { justifyContent: 'flex-end', height: AppConstants.getDeviceHeight(8), }]}
                        position="top"
                        positionValue={AppConstants.getDeviceHeight(0)}
                        fadeInDuration={750}
                        fadeOutDuration={1000}
                        textStyle={AppConstants.CommonStyles.ToastTextStyle} />
                    <View style={styles.ProfilebackGround}>
                        <ImageBackground source={{ uri: Images.Profileicon }} style={styles.imagebackground}>
                            <Avatar
                                size={AppConstants.getDeviceHeight(30.5)}
                                overlayContainerStyle={{ backgroundColor: null }}
                                containerStyle={{ alignSelf: 'center', backgroundColor: null }}
                                rounded
                                source={{ uri: this.state.picture }}
                            />
                        </ImageBackground>
                        <Text style={styles.username}>{this.state.name}</Text>
                    </View>
                    <View style={styles.profiledetail}>
                        <View style={styles.persionaldetai}>
                            <Text style={[styles.username1, AppConstants.CommonStyles.Fontfamilandcolor14]}>{this.state.agencyname}</Text>
                        </View>
                        <View style={styles.persionaldetai1}>
                            <Text style={[styles.username1, AppConstants.CommonStyles.Fontfamilandcolor14]}>{this.state.city}</Text>
                        </View>
                    </View>
                    <View style={styles.profileselect}>
                        <TouchableOpacity style={{ width: AppConstants.getDeviceWidth(50) }} onPress={() => { this.props.navigation.navigate(AppConstants.SCREENS.PROFILE) }}>
                            <Avatar source={{ uri: Images.Bio }} overlayContainerStyle={{ backgroundColor: 'white' }} rounded size={AppConstants.moderateScale(85)}
                                containerStyle={{ alignSelf: 'center', marginLeft: AppConstants.getDeviceWidth(3) }}></Avatar>
                            <Text style={[styles.introtext, AppConstants.CommonStyles.Fontfamilandcolor14]}>Profile</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: AppConstants.getDeviceWidth(50) }} onPress={() => { this.props.navigation.navigate(AppConstants.SCREENS.DIRECTORWORKLINK) }}>
                            <Avatar source={{ uri: Images.Worklink }} overlayContainerStyle={{ backgroundColor: 'white' }} rounded size={AppConstants.moderateScale(85)}
                                containerStyle={{ alignSelf: 'center', marginLeft: AppConstants.getDeviceWidth(3) }}></Avatar>
                            <Text style={[styles.introtext, AppConstants.CommonStyles.Fontfamilandcolor14]}>WorkLink</Text>
                        </TouchableOpacity>

                    </View>

                </View>
            </View>
        );
    }
}