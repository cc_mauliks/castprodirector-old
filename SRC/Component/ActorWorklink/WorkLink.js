import React, { Component } from 'react';

import {
    TextInput,
    ScrollView,
    View,
    Text,
    StatusBar,
    Keyboard,
    TouchableOpacity,
    Platform,
    ActivityIndicator,
    BackHandler,
    Alert
} from 'react-native';
import styles from './DirectorWorklinkStyle'
import * as AppConstants from '../Helper/AppConstants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import OfflineNotice from '../Helper/OfflineNotice';
import Toast, { DURATION } from 'react-native-easy-toast'
import { PostData, } from '../WebServices/webAPIRequest';

import API from "../WebServices/API";
import TagInput from "../Taginput";
const inputProps = {
    keyboardType: "default",
    autoFocus: true,
    style: {
        fontSize: 14,
        marginVertical: Platform.OS == "ios" ? 10 : -2
    }
};
export default class WorkLink extends Component {
    constructor(props) {
        super(props);
        this.state = {
            myArr: [],
            isLoading: false,
            isLoading1: false,
            tags: [],
            ActivityIndicator,
            Description: '',
            userlink: '',
            Description1: false,
            userlink1: false
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        clearTimeout(this.timeoutHandle);
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    handleBackButtonClick() {

        this.props.navigation.navigate(AppConstants.SCREENS.ACTORWORKLINK);

        return true;
    }
    componentWillMount() {
        this.ProfielGet();
    }
    ProfielGet() {
        Keyboard.dismiss();
        if (global.isConnected) {
            APIData = JSON.stringify({
            });
            PostData(API.Directorgetprofile, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false });
                    if (responseJson.statusCode == 1) {
                        this.setState({ tags: responseJson.userData.key_projects })
                    }
                    else {
                        Alert.alert(responseJson.message);
                    }
                })
                .catch(() => {
                    this.setState({ isLoading: false });
                });
        }
    }
    UpdateWorklink() {
        Keyboard.dismiss();
        if (global.isConnected) {
            APIData = JSON.stringify({
                key_projects: this.state.tags
            });
            PostData(API.UpdateWorklink, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false });
                    if (responseJson.statusCode == 1) {
                        this.refs.toast1.show(responseJson.message);
                    }
                    else {
                        Alert.alert(responseJson.message);
                    }
                })
                .catch(() => {
                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }

    }
    validator = () => {
        if (this.state.tags == '') {
            this.setState({tags1:true})
        }
        else {
            this.UpdateWorklink();
        }
    }

    onChangeTags = (tags) => {
        this.setState({ tags });
    }
    onChangeText = (text) => {
        this.setState({ text });
        const lastTyped = text.charAt(text.length - 1);
        const parseWhen = [',', ' ', ';', '\n'];
        if (parseWhen.indexOf(lastTyped) > -1) {
            this.setState({
                tags: [...this.state.tags, this.state.text],
                text: "",
                tags1:false
            });
        }
    }
    tag = () => {
        if (this.state.userlink == '') {
            this.setState({userlink1:true})
        }
        else if (this.state.Description == '') {
            this.setState({Description1:true})
        }
        else {
            var joined = this.state.tags.concat({ work_link: this.state.userlink, work_description: this.state.Description,userlink1:false ,Description1:false});
            this.setState({
                tags: joined,
                Userinterests: '',
                userlink: '',
                Description: '',
            });
        }
    }
    labelExtractor = (tag) => tag;
    render() {
        if (this.state.isLoading) {
            return AppConstants.ShowActivityIndicator();
        }

        return (
            <View style={styles.mainContainer}>
                <KeyboardAwareView doNotForceDismissKeyboardWhenLayoutChanges={true}>
                    <StatusBar barStyle="light-content" backgroundCopr={AppConstants.SHADOWCOLORS.INTRO} animated translucent></StatusBar>
                    <OfflineNotice />
                    <View style={styles.mainContainer}>
                        <Toast
                            ref="toast"
                            style={[AppConstants.CommonStyles.ToastStyle, { justifyContent: 'flex-end', height: AppConstants.getDeviceHeight(8), }]}
                            position="top"
                            positionValue={AppConstants.getDeviceHeight(0)}
                            fadeInDuration={750}
                            fadeOutDuration={1000}
                            textStyle={AppConstants.CommonStyles.ToastTextStyle}
                        />
                        <Toast
                            ref="toast1"
                            style={[AppConstants.CommonStyles.ToastStyle2, { height: AppConstants.getDeviceHeight(5), }]}
                            position='bottom'
                            positionValue={200}
                            fadeInDuration={1000}
                            fadeOutDuration={1000}
                            textStyle={AppConstants.CommonStyles.ToastTextStyle1}
                        />
                        <ScrollView contentContainerStyle={{ flexGrow: 1, }}>

                            <Text style={styles.profiletext2}>Work Links</Text>
                            <View style={styles.tagview}>
                                <TextInput style={styles.taginputtext}
                                    placeholder="Please Enter Work Link"
                                    placeholderTextColor={AppConstants.COLORS.PLACEHOLDAR}
                                    underlineColorAndroid="transparent"
                                    blurOnSubmit={false}
                                    autoCapitalize="none"
                                    value={this.state.userlink}
                                    ref={mobileno => { this.textInput1 = mobileno }}
                                    onChangeText={(userlink) => this.setState({ userlink, userlink1: false })} />
                                <TouchableOpacity style={styles.add} onPress={() => { this.tag() }}>
                                    <Text style={styles.Signup}>Add</Text>
                                </TouchableOpacity>

                            </View>
                            {this.state.userlink1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor,{marginLeft:AppConstants.getDeviceWidth(7),marginTop:AppConstants.getDeviceHeight(2)}]}>{AppConstants.Messages.LINK}</Text> : null}

                            <View style={styles.Descroption}>
                                <TextInput style={styles.textmultiline}
                                    placeholderTextColor={AppConstants.COLORS.PLACEHOLDAR}
                                    underlineColorAndroid="transparent"
                                    value={this.state.Description}
                                    blurOnSubmit={false}
                                    placeholder={'Enter Short Description'}
                                    multiline={true}
                                    autoCapitalize="none"
                                    onChangeText={(Description) => this.setState({ Description, Description1: false })} />
                                {this.state.Description1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor,{marginLeft:AppConstants.getDeviceWidth(7),}]}>{AppConstants.Messages.DESCRIPTION}</Text> : null}

                            </View>
                            <View
                                style={{ alignItems: "center", marginTop: AppConstants.getDeviceHeight(2) }}>
                                <Text style={{ marginLeft: AppConstants.getDeviceWidth(13) }} />
                                <TagInput
                                    autoFocus={true}
                                    value={this.state.tags}
                                    onChange={this.onChangeTags}
                                    labelExtractor={this.labelExtractor}
                                    text={this.state.text}
                                    onChangeText={() => this.onChangeText}
                                    tagColor="transparent"
                                    scrollEnabled={true}
                                    tagTextColor="black"
                                    inputProps={inputProps} />
                            </View>
                            {this.state.tags1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor,{marginLeft:AppConstants.getDeviceWidth(7),}]}>{AppConstants.Messages.DESCRIPTION}</Text> : null}

                            <View style={styles.button}>
                                <TouchableOpacity style={styles.save} onPress={() => { this.validator() }}>
                                    <Text style={styles.Signup}>
                                        SAVE
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>
                </KeyboardAwareView>
                {this.state.isLoading1 ? <View style={AppConstants.CommonStyles.spinner}>
                    <ActivityIndicator size='large' ></ActivityIndicator>
                </View> : null}
            </View>
        );
    }
}