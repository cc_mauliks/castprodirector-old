import * as AppConstants from '../Helper/AppConstants';
export default {
    mainContainer: {
        flex: 1,
    },

    error: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        marginTop: 0,
        textAlign: 'center',
        fontWeight: '500',
    },
    username: {
        marginLeft: AppConstants.getDeviceWidth(5),
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS22),
        fontWeight: 'bold',
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEINACTIVE,
        alignSelf: 'center',
        marginRight: AppConstants.getDeviceWidth(20),
    },
    socialmedia: {
        marginLeft: AppConstants.getDeviceWidth(5),
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS16),
        fontWeight: '300',
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEINACTIVE,
    },
    descrption: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        textAlign: 'center'
    },
    profiletext2: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        marginLeft: AppConstants.getDeviceWidth(7),
        marginBottom: AppConstants.getDeviceHeight(0.5),
        marginTop: AppConstants.getDeviceHeight(3),
        color: AppConstants.COLORS.PROFILEBACKGROUND,
    },
    tagview: {
        width: AppConstants.getDeviceWidth(86),
        height: AppConstants.getDeviceHeight(4),
        flexDirection: 'row',
    },
    taginputtext: {
        width: AppConstants.getDeviceWidth(65),
        height: AppConstants.getDeviceHeight(5.5),
        color: AppConstants.COLORS.BLACK,
        backgroundColor: AppConstants.COLORS.PROFILETEXINPUT,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS13),
        marginLeft: AppConstants.getDeviceWidth(7),
        marginRight: AppConstants.getDeviceWidth(2),
        paddingLeft: AppConstants.getDeviceWidth(2),
        padding: 0,
        margin: 0,
    },
    Descroption: {
        width: AppConstants.getDeviceWidth(100),
        marginTop: AppConstants.getDeviceHeight(6),
        justifyContent: "center",
        height: AppConstants.getDeviceHeight(8),
    },
    textmultiline: {
        width: AppConstants.getDeviceWidth(65),
        height: AppConstants.getDeviceHeight(11),
        color: AppConstants.COLORS.BLACK,
        backgroundColor: AppConstants.COLORS.PROFILETEXINPUT,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS13),
        marginLeft: AppConstants.getDeviceWidth(7),
        marginRight: AppConstants.getDeviceWidth(2),
        paddingLeft: AppConstants.getDeviceWidth(2),
    },

    Signup: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontWeight: 'bold',
        alignSelf: 'center',
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
    },
    iconedit: {
        width: AppConstants.getDeviceHeight(4),
        alignSelf: 'center',
        right: 0,
        position: 'absolute',
        height: AppConstants.getDeviceHeight(3),
        marginRight: AppConstants.getDeviceWidth(6)
    },
    linkclick: {
        marginLeft: AppConstants.getDeviceWidth(1),
        marginRight: AppConstants.getDeviceWidth(1),
        flexDirection: 'column',
        justifyContent: 'space-between',
        width: AppConstants.getDeviceWidth(20)
    },
    save: {
        backgroundColor: AppConstants.COLORS.PROFILEINACTIVE,
        height: AppConstants.getDeviceHeight(5),
        marginBottom: AppConstants.getDeviceHeight(2),
        alignItems: 'center',
        justifyContent: 'center',
        width: AppConstants.getDeviceWidth(30),
        alignSelf: 'center'
    },
    add: {
        backgroundColor: AppConstants.COLORS.PROFILEINACTIVE,
        height: AppConstants.getDeviceHeight(5.5),
        justifyContent: "center",
        width: AppConstants.getDeviceWidth(19),
        marginRight: AppConstants.getDeviceWidth(7)
    }
}