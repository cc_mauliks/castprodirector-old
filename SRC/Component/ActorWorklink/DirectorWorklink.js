import React, { Component } from 'react';
import {
    Image,
    ScrollView,
    View,
    Text,
    StatusBar,
    BackHandler,
    TouchableOpacity,
    Linking,
    FlatList,
    ActivityIndicator,
    Alert
} from 'react-native';
import styles from './DirectorWorklinkStyle'
import * as AppConstants from '../Helper/AppConstants';
import OfflineNotice from '../Helper/OfflineNotice';
import { PostData, internet } from '../WebServices/webAPIRequest';
import API from '../WebServices/API';
import Iconss from "react-native-vector-icons/MaterialIcons";
const Images = {
    Worklink: 'worklink',

}
export default class DirectorWorklink extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            name: '',
            socialarray: [],
            workarray: [],
            nullcheck: ''
        };
        this.lists = [
            this.props.navigation.addListener('didFocus', () => { this.componentDidFocus() })
        ];
    }


    componentDidFocus() {
        this.setState({ socialarray: [], workarray: [] });
        this.ProfielGet();
    }
    ProfielGet() {
        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({

            });
            PostData(API.Directorgetprofile, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false, workarray: [] });

                    if (responseJson.statusCode == 1) {
                        this.setState({ workarray: responseJson.userData.key_projects })
                    }
                    else {
                        Alert.alert(responseJson.message);
                    }
                })
                .catch(() => {
                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }
    }

    render() {
        if (this.state.isLoading) {
            return AppConstants.ShowActivityIndicator();
        }
        return (
            <View style={styles.mainContainer}>
                <StatusBar barStyle="light-content" backgroundCopr={AppConstants.SHADOWCOLORS.INTRO} animated translucent></StatusBar>
                <OfflineNotice />
                <View style={styles.mainContainer}>
                    <View style={{ width: AppConstants.getDeviceWidth(100), height: AppConstants.getDeviceHeight(8), flexDirection: 'row' }}>
                        <Text style={styles.username}>{this.state.name}</Text>
                        <TouchableOpacity onPress={() => {
                            BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
                            this.props.navigation.navigate(AppConstants.SCREENS.WORKLINK)
                        }} style={styles.iconedit}>
                            <Iconss name="edit" color={AppConstants.COLORS.BLACK} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS22)} />
                        </TouchableOpacity>
                    </View>
                    <ScrollView contentContainerStyle={{ flexGrow: 1, }} style={{ marginBottom: AppConstants.getDeviceHeight(8) }}>

                        {this.state.workarray == '' ? <View style={{ flex: 1, justifyContent: 'center' }}>
                            <Text style={styles.error}>There are no work links added yet.</Text>
                        </View> : <View style={{ marginTop: AppConstants.getDeviceHeight(2), }}>
                                <Text style={styles.socialmedia}>Work Links</Text>
                                <FlatList
                                    data={this.state.workarray}
                                    renderItem={({ item }) =>
                                        (
                                            <View style={styles.linkclick}>
                                                <TouchableOpacity
                                                    key={item.id}
                                                    style={{ justifyContent: 'center' }}
                                                    onPress={() => {
                                                        Linking.openURL(item.work_link).then((s) => {
                                                        }).catch((e) => {
                                                            Alert.alert('The link you used may be invalid');
                                                        });
                                                    }}>
                                                    <Image
                                                        style={styles.image}
                                                        source={{ uri: Images.Worklink }} />
                                                </TouchableOpacity>
                                                <Text numberOfLines={3} style={styles.descrption}>{item.work_description}</Text>
                                            </View>
                                        )}
                                    numColumns={3}
                                    keyExtractor={(item, index) => index.toString()} />
                            </View>}

                    </ScrollView>
                </View>
                {this.state.isLoading ? <View style={AppConstants.CommonStyles.spinner}>
                    <ActivityIndicator size='large' color={AppConstants.COLORS.SIGNUP}></ActivityIndicator>
                </View> : null}
            </View>
        );
    }
}