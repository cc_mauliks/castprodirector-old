import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Image, Alert, Text,Platform } from 'react-native';
import * as AppConstants from '../Helper/AppConstants';
import { Avatar, Badge, withBadge } from 'react-native-elements';
import Icon from "react-native-vector-icons/FontAwesome";
import Icons from "react-native-vector-icons/Ionicons";
import Iconss from "react-native-vector-icons/Entypo";
import Iconsss from "react-native-vector-icons/MaterialIcons";
import Iconssss from "react-native-vector-icons/MaterialCommunityIcons";
import API from "../WebServices/API";
import { PostData, internet } from '../WebServices/webAPIRequest';


const Images = {
    TabprofielIcon: 'tabprofile',
    Tabprofileactive: 'tabprofileactive'
}
export default class Tab extends Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 0,
            dataUpdatedAt: null,
            dashboard: '',
            unreadcount: ''
        };
        this.lists = [
            this.props.navigation.addListener('didFocus', () => {
                this.componentdidfoucuse()
            })
        ];
    }
    componentdidfoucuse() {
        this.Unreadcount();
    }
    async Unreadcount() {
        if (global.isConnected) {
            APIData = JSON.stringify({
            });
            PostData(API.Unreadcoiunt, APIData)
                .then((responseJson) => {
                    if (responseJson.statusCode == 1) {
                        this.setState({
                            unreadcount: responseJson.count
                        })
                    }
                    else {
                        Alert.alert(responseJson.message);
                    }
                }).catch(() => {
                    this.setState({ isLoading: false });

                });
        }
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.tabNavContainer}>
                <TouchableOpacity style={styles.tabIcon} activeOpacity={0.9} onPress={() => { 
                     this.Unreadcount();
                    navigate(AppConstants.SCREENS.DASHBOARD, this.props); }}>
                    <Icon style={{ alignSelf: 'center' }} color={this.props.navigation.state.index == 0 ? this.props.activeTintColor : this.props.inactiveTintColor}
                        name="home" size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS30)} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.tabIcon} activeOpacity={0.9} onPress={() => {
                     this.Unreadcount();
                      navigate(AppConstants.SCREENS.DIRECTORSEARCH) }} >
                    <Icons style={{ alignSelf: 'center' }} color={this.props.navigation.state.index == 1 ? this.props.activeTintColor : this.props.inactiveTintColor} name="ios-search" size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS30)} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.tabIcon} activeOpacity={0.9} onPress={() =>{
                      this.Unreadcount();
                     navigate(AppConstants.SCREENS.DIRECTORPROFILE)} 
                     } >
                    <Image source={{ uri: this.props.navigation.state.index == 2 ? Images.Tabprofileactive : this.props.navigation.state.index == 5 ? Images.Tabprofileactive : this.props.navigation.state.index == 6 ? Images.Tabprofileactive : this.props.navigation.state.index == 7 ? Images.Tabprofileactive : this.props.navigation.state.index == 8 ? Images.Tabprofileactive : Images.TabprofielIcon }}
                        style={{ width: AppConstants.getDeviceHeight(4), height: AppConstants.getDeviceHeight(4), alignSelf: 'center' }}></Image>
                    {/* <Iconss style={{alignSelf:'center'}} name="user" size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS30)} /> */}
                </TouchableOpacity>
                <TouchableOpacity style={styles.tabIcon} activeOpacity={0.9} onPress={() => {
                     this.Unreadcount();
                     navigate(AppConstants.SCREENS.DIRECTONOTIFICATION)}} >
                    <Iconsss style={{ alignSelf: 'center' }} color={this.props.navigation.state.index == 3 ? this.props.activeTintColor : this.props.inactiveTintColor} name="notifications" size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS30)} />
                    {this.state.unreadcount==''?null:<View style={{ justifyContent: 'center', backgroundColor: 'red', width: 25, height: 25, borderRadius: 25 / 2, position: 'absolute', right: 0, marginRight:Platform.isPad==true? AppConstants.getDeviceWidth(7): AppConstants.getDeviceWidth(5), top: 0, marginTop:Platform.isPad==true? AppConstants.getDeviceHeight(5):AppConstants.getDeviceHeight(2) }}>
                        <Text style={{ alignSelf: 'center',color:AppConstants.COLORS.WHITE }}>{this.state.unreadcount}</Text>
                    </View>}

                </TouchableOpacity>
                <TouchableOpacity style={styles.tabIcon} activeOpacity={0.9} onPress={() => {
                     this.Unreadcount();
                    navigate(AppConstants.SCREENS.MORE)}} >
                    <Iconss style={{ alignSelf: 'center' }} color={this.props.navigation.state.index == 4 ? this.props.activeTintColor : this.props.inactiveTintColor} name="dots-three-horizontal" size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS30)} />
                </TouchableOpacity>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    // tabNavContainer
    tabNavContainer: {
        flexDirection: 'row',
        marginTop: AppConstants.getDeviceHeight(1),
        height: AppConstants.getDeviceHeight(8),
        width: AppConstants.getDeviceWidth(100),
        backgroundColor: AppConstants.COLORS.VIEWBACKGROUND,
        alignItems: 'center',
        justifyContent: 'space-between',
        position: 'absolute',
        bottom: 0,
        borderTopWidth: 0.5,
    },
    tabIcon: {
        height: AppConstants.getDeviceWidth(20),
        aspectRatio: 1,
        shadowColor: AppConstants.SHADOWCOLORS.TEXT_SHADOW,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 1,
        shadowRadius: AppConstants.getDeviceWidth(2),
        justifyContent: 'center',

    },
});