import * as AppConstants from '../Helper/AppConstants';
export default {
    mainContainer: {
        flex: 1,
        backgroundColor: AppConstants.COLORS.VIEWBACKGROUND
    },
  
    FlatlistContainer: {
        flexDirection: 'row',
        width: AppConstants.getDeviceWidth(100),
        marginTop: AppConstants.getDeviceHeight(0.5),
        shadowOffset: { width: 0, height: 2 },
        shadowColor: AppConstants.SHADOWCOLORS.TEXT_SHADOW,
    },
    FlatlistContainer1: {
        flexDirection: 'row',
        width: AppConstants.getDeviceWidth(96),
        backgroundColor: AppConstants.COLORS.WHITE,
        shadowOffset: { width: 0, height: 2 },
        shadowColor: AppConstants.SHADOWCOLORS.TEXT_SHADOW,
        shadowOpacity: 0.8,
        shadowRadius: 3,
        elevation: 3,
        borderRadius: 3,
        marginLeft: AppConstants.getDeviceWidth(2),
        marginRight: AppConstants.getDeviceWidth(2),
        marginBottom: AppConstants.getDeviceWidth(2),
        marginTop: AppConstants.getDeviceWidth(2),
    },
    projecttitle: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND, fontWeight: 'bold',
        marginLeft: AppConstants.getDeviceWidth(5)
    },
    time: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
    },
    time1: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        fontWeight: 'bold',
    },
    projectrole: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,

        marginLeft: AppConstants.getDeviceWidth(5)
    },

    dateview: {
        width: AppConstants.getDeviceWidth(28),
        height: AppConstants.getDeviceHeight(10),
        justifyContent: 'center',
        position: 'absolute',
        right: 0,
        alignSelf: 'center',
        marginRight: AppConstants.getDeviceWidth(1),
    },
    dateview2: {
        width: AppConstants.getDeviceWidth(28),
        height: AppConstants.getDeviceHeight(6),
        flexDirection: 'row',
        marginTop: AppConstants.getDeviceHeight(3)
    },
    viewerorr: {
        flex: 1,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        justifyContent: 'center',
        marginBottom: AppConstants.getDeviceHeight(8)
    },
    error: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        marginTop: 0,
        textAlign: 'center',
        fontWeight: '500',
    },
    listview: {
        width: AppConstants.getDeviceWidth(100),
        marginBottom: AppConstants.getDeviceHeight(1),
        marginTop: AppConstants.getDeviceHeight(1),
        flexDirection: 'row'
    }
}