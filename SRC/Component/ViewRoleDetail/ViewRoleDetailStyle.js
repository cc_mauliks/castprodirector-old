import * as AppConstants from '../Helper/AppConstants';
import { Platform,} from 'react-native';
export default {
    mainContainer: {
        flex: 1,

    },
    projectrole: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        fontWeight: 'bold',
        marginLeft: AppConstants.getDeviceWidth(5)
    },
    projectdesc1: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,

    },
    projectdesc: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        marginLeft: AppConstants.getDeviceWidth(5),
        marginTop: AppConstants.getDeviceHeight(1),
        marginRight: AppConstants.getDeviceWidth(5),
    },
    projectdate: {
        fontSize: AppConstants.moderateScale(12),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.datecolor,
        fontWeight: 'bold',
        marginLeft: AppConstants.getDeviceWidth(5),
        marginRight: AppConstants.getDeviceWidth(0.5),
        alignSelf: 'center',
        alignItems: 'center',
        textAlign: 'center',
    },
    line: {
        width: AppConstants.getDeviceWidth(0.5),
        height: AppConstants.getDeviceHeight(5),
        backgroundColor: AppConstants.COLORS.datecolor,
        marginLeft: AppConstants.getDeviceWidth(1),
        marginRight: AppConstants.getDeviceWidth(10),
        right: 0,
        position: 'absolute',
    },
    button1: {
        width: AppConstants.getDeviceWidth(100),
        marginTop: AppConstants.getDeviceHeight(8),
        height: AppConstants.getDeviceHeight(3),
        justifyContent: "center",
    },
    Signup: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        fontWeight: 'bold',
        alignSelf: 'center',
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        alignItems: 'center'

    },
    savetouchable: {
        backgroundColor: AppConstants.COLORS.PROFILEINACTIVE,
        height: AppConstants.getDeviceHeight(5),
        marginBottom: AppConstants.getDeviceHeight(2),
        alignItems: 'center',
        justifyContent: 'center',
        width: AppConstants.getDeviceWidth(35),
        alignSelf: 'center'
    },
    datepicker: {
        width: AppConstants.getDeviceWidth(42),
        height: AppConstants.getDeviceHeight(5),
        backgroundColor: AppConstants.COLORS.BACKHOME,
        marginLeft: AppConstants.getDeviceWidth(5),
        justifyContent: 'center',
        marginTop: AppConstants.getDeviceHeight(1)
    },
    datepickerdate: {
        alignSelf: 'center',
        alignItems: 'center',
        textAlign: 'center',
        paddingBottom: AppConstants.getDeviceWidth(2),
        color: '#000',
        marginTop: AppConstants.getDeviceHeight(1)
    },
    line: {
        width: AppConstants.getDeviceWidth(0.5),
        height: AppConstants.getDeviceHeight(5),
        backgroundColor: AppConstants.COLORS.datecolor,
        marginBottom: AppConstants.getDeviceHeight(1)
    },
    close: {
        backgroundCopr: 'red',
        marginLeft: AppConstants.getDeviceWidth(5),
        position: 'absolute',
        width: AppConstants.getDeviceWidth(45),
        height: AppConstants.getDeviceHeight(5),
    },
    dateicon: {
        marginLeft: AppConstants.getDeviceWidth(1),
        alignItems: 'center',
        marginTop: AppConstants.moderateScale(1)
    },
    datepicker: {
        width: AppConstants.getDeviceWidth(10),
        height: AppConstants.getDeviceHeight(5),
        backgroundColor: AppConstants.COLORS.BACKHOME,
        marginTop: Platform.isPad == true ? AppConstants.getDeviceHeight(2) : 0,
        justifyContent: 'center',
    }
}