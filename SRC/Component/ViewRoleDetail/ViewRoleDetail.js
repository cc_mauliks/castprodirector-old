import React, { Component } from 'react';
import {
    View,
    Text,
    StatusBar,
    Keyboard,
    TouchableOpacity,
    Platform,
    ActivityIndicator,
    Alert
} from 'react-native';
import styles from './ViewRoleDetailStyle'
import * as AppConstants from '../Helper/AppConstants';
import OfflineNotice from '../Helper/OfflineNotice';
import Toast, { DURATION } from 'react-native-easy-toast'
import Icon from "react-native-vector-icons/MaterialIcons";
import DatePicker from 'react-native-datepicker';
import API from "../WebServices/API";
import { PostData, internet } from '../WebServices/webAPIRequest';
export default class ViewRoleDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Search: '',
            thumbnail: '',
            Rolename: '',
            startdate: '',
            enddate: '',
            Description: '',
            projectid: '',
            projectbrief: '',
            Projectname1: '',
            VisibaleUpdatedate: 0,
            Auditioncount: '',
            openclose: '',
            projectstartdate: '', Projectendate: '',
            datevalue: ''
        };
        this.lists = [
            this.props.navigation.addListener('didFocus', () => { this.componentDidFocus() })
        ];
    }
    componentDidFocus() {
        this.Viewroledetail();
    }
    Viewroledetail() {
        Keyboard.dismiss();
        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({
                role_id: this.props.navigation.state.params.Roleid,

            });
            PostData(API.RoleDetail, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false });
                    if (responseJson.statusCode == 1) {
                        this.setState({
                            Rolename: responseJson.data.role_name,
                            startdate: responseJson.data.start_date,
                            enddate: responseJson.data.end_date,
                            Description: responseJson.data.role_detail,
                            projectid: responseJson.data.project_id,
                            projectbrief: responseJson.data.project_brief,
                            Projectname1: responseJson.data.project_name,
                            Auditioncount: responseJson.count,
                            openclose: responseJson.data.open_closed_status,
                            projectstartdate: responseJson.data.start_date,
                            Projectendate: responseJson.data.project_end_date,
                        })
                        var d = responseJson.data.project_end_date.split('-')
                        var startDate1 = new Date(d[0] + '/' + d[1] + '/' + d[2]);
                        this.setState({ datevalue: startDate1.getTime() > new Date().getTime() })
                    }
                    else {
                        Alert.alert(responseJson.message);
                    }
                }).catch(() => {
                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }
    }
    Updatedate() {
        Keyboard.dismiss();
        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({
                start_date: this.state.startdate,
                end_date: this.state.enddate,
                role_id: this.props.navigation.state.params.Roleid
            });
            PostData(API.RoleUpdate, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false });
                    if (responseJson.statusCode == 1) {
                        this.refs.toast1.show(responseJson.message)
                    }
                    else {
                        Alert.alert(responseJson.message);
                    }
                }).catch(() => {
                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }
    }
    RoleDelete() {
        Keyboard.dismiss();
        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({
                role_id: this.props.navigation.state.params.Roleid,
            });
            PostData(API.Roledelete, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false });
                    if (responseJson.statusCode == 1) {
                        this.props.navigation.navigate(AppConstants.SCREENS.PROJECTDETAIL, { projectid: this.state.projectid });
                    }
                    else {
                        Alert.alert(responseJson.message);
                    }
                }).catch(() => {
                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }
    }
    render() {
        return (
            <View style={styles.mainContainer}>
                <StatusBar barStyle="light-content" backgroundCopr={AppConstants.SHADOWCOLORS.INTRO} animated translucent></StatusBar>
                <OfflineNotice />
                <View style={styles.mainContainer}>
                    <Toast
                        ref="toast"
                        style={[AppConstants.CommonStyles.ToastStyle1, { justifyContent: 'flex-end', height: AppConstants.getDeviceHeight(11), }]}
                        position="top"
                        positionValue={AppConstants.getDeviceHeight(0)}
                        fadeInDuration={750}
                        fadeOutDuration={1000}
                        textStyle={AppConstants.CommonStyles.ToastTextStyle}
                    />
                    <Toast
                        ref="toast1"
                        style={[AppConstants.CommonStyles.ToastStyle2, { height: AppConstants.getDeviceHeight(6.5), }]}
                        position='bottom'
                        positionValue={200}
                        fadeInDuration={1000}
                        fadeOutDuration={1000}
                        textStyle={AppConstants.CommonStyles.ToastTextStyle1} />
                    <View style={{ width: AppConstants.getDeviceWidth(100), flexDirection: 'row', marginTop: AppConstants.getDeviceHeight(2) }}>
                        <Text style={[styles.projectrole]}>{this.state.Projectname1} </Text>
                        <TouchableOpacity onPress={() => {
                            Alert.alert(
                                'Delete',
                                'Are you sure you want to delete?',
                                [
                                    {
                                        text: 'Cancel',
                                        onPress: () => { },
                                        style: 'cancel',
                                    },
                                    {
                                        text: 'OK', onPress: () => {

                                            this.RoleDelete();
                                        }
                                    },
                                ],
                                { cancelable: false },
                            );
                        }} style={{ position: 'absolute', right: 0, flexDirection: 'row', justifyContent: 'center' }}>
                            <Text style={[styles.projectdesc1, { fontWeight: 'bold', color: AppConstants.COLORS.PROFILEINACTIVE, marginRight: AppConstants.getDeviceWidth(1), alignSelf: 'center' }]}>Delete Role</Text>
                            <Icon name="delete" color={AppConstants.COLORS.PROFILEINACTIVE} style={{ alignSelf: 'center' }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS22)} />

                        </TouchableOpacity>
                    </View>
                    <Text style={styles.projectdesc}>{this.state.projectbrief}</Text>
                    <View style={{ width: AppConstants.getDeviceWidth(100), flexDirection: 'row', marginTop: AppConstants.getDeviceHeight(2) }}>
                        <Text style={[styles.projectrole]}>Role: </Text>
                        <Text style={styles.projectdesc1}>{this.state.Rolename}</Text>
                    </View>

                    <Text style={[styles.projectrole, { marginTop: AppConstants.getDeviceHeight(3) }]}>Description </Text>
                    <Text style={styles.projectdesc}>{this.state.Description}</Text>
                    <Text style={styles.projectdesc}>Start Date</Text>
                    <View style={{ flexDirection: 'row', marginTop: AppConstants.getDeviceHeight(1) }}>
                        <DatePicker
                            style={styles.datepicker}
                            date={this.state.startdate}
                            mode="date"
                            placeholder="DD/MM/YYYY"
                            format="DD-MM-YYYY"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            showIcon={true}
                            iconComponent={<View style={{ flexDirection: 'row', justifyContent: 'center', height: AppConstants.getDeviceHeight(5), }}>
                                <View style={[styles.line, { marginTop: Platform.isPad == true ? AppConstants.getDeviceHeight(2) : 0 }]}></View>
                                <View  style={styles.datepicker}>
                                    <Icon name="date-range" color={AppConstants.COLORS.datecolor} style={styles.dateicon} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS28)} />
                                </View>
                            </View>
                            }
                            customStyles={{
                                dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    marginLeft: 0
                                },
                                dateInput: {
                                    borderLeftWidth: 0,
                                    borderRightWidth: 0,
                                    borderTopWidth: 0,
                                    borderBottomWidth: 0,
                                },
                                borderBottomColor: AppConstants.COLORS.WHITE,
                                dateIcon: null,
                                dateText: styles.datepickerdate,
                                placeholderText: styles.projectdate
                            }}
                            onDateChange={(date) => {
                                this.setState({ startdate: date, VisibaleUpdatedate: 1 })
                            }} />
                        {this.state.openclose == "Close" || this.state.datevalue == false ? <TouchableOpacity style={{ marginLeft: AppConstants.getDeviceWidth(5), position: 'absolute', width: AppConstants.getDeviceWidth(45), height: AppConstants.getDeviceHeight(5), }}></TouchableOpacity> : null}
                    </View>
                    <Text style={styles.projectdesc}>End Date</Text>
                    <View style={{ flexDirection: 'row', marginTop: AppConstants.getDeviceHeight(1) }}>
                        <DatePicker
                            style={styles.datepicker}
                            date={this.state.enddate}
                            mode="date"
                            placeholder="DD/MM/YYYY"
                            format="DD-MM-YYYY"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            showIcon={true}
                            iconComponent={<View style={{ flexDirection: 'row', justifyContent: 'center', height: AppConstants.getDeviceHeight(5), }}>
                                <View style={[styles.line, { marginTop: Platform.isPad == true ? AppConstants.getDeviceHeight(2) : 0 }]}></View>
                                <View style={styles.datepicker}>
                                    <Icon name="date-range" color={AppConstants.COLORS.datecolor} style={styles.dateicon} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS28)} />
                                </View>
                            </View>
                            }
                            customStyles={{
                                dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    marginLeft: 0
                                },
                                dateInput: {
                                    borderLeftWidth: 0,
                                    borderRightWidth: 0,
                                    borderTopWidth: 0,
                                    borderBottomWidth: 0,
                                },
                                borderBottomColor: AppConstants.COLORS.WHITE,
                                dateIcon: null,
                                dateText: styles.datepickerdate,
                                placeholderText: styles.projectdate
                            }}
                            onDateChange={(date) => {
                                this.setState({ enddate: date, VisibaleUpdatedate: 1 })
                            }} />
                        {this.state.openclose == "Close" || this.state.datevalue == false ? <TouchableOpacity style={styles.close}></TouchableOpacity> : null}
                    </View>
                    <View style={styles.button1}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                            {this.state.Auditioncount == 0 ? null : <TouchableOpacity style={[styles.savetouchable, { marginLeft: AppConstants.getDeviceWidth(5) }]} onPress={() => { this.props.navigation.navigate(AppConstants.SCREENS.VIEWAUDITIONLIST, { rolid: this.props.navigation.state.params.Roleid }) }}>
                                <Text style={styles.Signup}>
                                    VIEW AUDITION
                                    </Text>
                            </TouchableOpacity>}
                            {this.state.VisibaleUpdatedate == 1 ? <TouchableOpacity style={[styles.savetouchable, { marginRight: AppConstants.getDeviceWidth(5), marginLeft: this.state.Auditioncount == 0 ? AppConstants.getDeviceWidth(5) : null }]} onPress={() => { this.Updatedate() }}>
                                <Text style={styles.Signup}>
                                    UPDATE
                                    </Text>
                            </TouchableOpacity> : null}
                        </View>
                    </View>
                </View>
                {this.state.isLoading ? <View style={AppConstants.CommonStyles.spinner}>
                    <ActivityIndicator size='large' ></ActivityIndicator>
                </View> : null}
            </View>
        )
    }
}