import * as AppConstants from '../Helper/AppConstants';
export default {
    mainContainer: {
        flex: 1,
    },
    projecttitle: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEINACTIVE,
        fontWeight: 'bold',
        marginLeft: AppConstants.getDeviceWidth(1)
    },
    time: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        fontWeight: 'bold',
        right: 0,
        position: 'absolute',
        marginRight: AppConstants.getDeviceWidth(5)
    },
    projectrole: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        fontWeight: 'bold',
        marginLeft: AppConstants.getDeviceWidth(1)
    },
    projectdesc: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
    },
    comment: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        fontWeight: 'bold',
        textAlign: 'center',
        position: 'absolute',
        alignItems: 'center',
        right: 0,
        marginRight: AppConstants.getDeviceWidth(4)
    },
    comment2: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        fontWeight: 'bold',
        textAlign: 'center',
        alignItems: 'center',
        marginLeft: AppConstants.getDeviceWidth(2)
    },
    videotouch: {
        height: AppConstants.getDeviceHeight(25),
        width: AppConstants.getDeviceWidth(96),
        backgroundColor: 'black',
        marginTop: AppConstants.getDeviceHeight(1)
    },
    viewerorr: {
        flex: 1,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        justifyContent: 'center',
        marginBottom: AppConstants.getDeviceHeight(8)
    },
    error: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        marginTop: 0,
        textAlign: 'center',
        fontWeight: '500',
    },
    FlatlistContainer1: {
        flexDirection: 'row',
        flex: 1,
        width: AppConstants.getDeviceWidth(96),
        marginTop: AppConstants.getDeviceHeight(0.2),
        backgroundColor: AppConstants.COLORS.WHITE,
        shadowOffset: { width: 0, height: 2 },
        shadowColor: AppConstants.SHADOWCOLORS.TEXT_SHADOW,
        shadowOpacity: 0.8,
        shadowRadius: 3,
        elevation: 3,
        borderRadius: 3,
        marginLeft: AppConstants.getDeviceWidth(2),
        marginRight: AppConstants.getDeviceWidth(2),
        marginBottom: AppConstants.getDeviceWidth(2),
        marginTop: AppConstants.getDeviceWidth(2),
    },
    navigationview: {
        position: 'absolute', right: 0,
        width: AppConstants.getDeviceHeight(4),
        height: AppConstants.getDeviceHeight(4),
        marginRight: AppConstants.getDeviceWidth(5)
    },
    commenticon: {
        alignSelf: 'center',
        position: 'absolute',
        right: 0,
        marginRight: AppConstants.getDeviceWidth(21)
    },
    playicon: {
        height: AppConstants.getDeviceHeight(25),
        width: AppConstants.getDeviceWidth(96),
        justifyContent: 'center'
    },
    titlename: {
        width: AppConstants.getDeviceWidth(96),
        flexDirection: 'row',
        marginTop: AppConstants.getDeviceHeight(1)
    },
    donwloadaudio: {
        marginLeft: AppConstants.getDeviceWidth(7),
        paddingTop: AppConstants.getDeviceHeight(1),
        paddingBottom: AppConstants.getDeviceHeight(1)
    },
    viewdonwload: {
        height: AppConstants.moderateScale(1),
        width: AppConstants.getDeviceWidth(100),
        backgroundColor: AppConstants.COLORS.PROFILEBACKGROUND
    },
    donwloadscript: {
        marginLeft: AppConstants.getDeviceWidth(7),
        paddingTop: AppConstants.getDeviceHeight(1),
        paddingBottom: AppConstants.getDeviceHeight(1)
    }
}