import React, { Component } from 'react';
import {
    Platform,
    View,
    Text,
    StatusBar,
    ActivityIndicator,
    TouchableOpacity,
    Keyboard,
    FlatList,
    ImageBackground,
    Alert,
    RefreshControl
} from 'react-native';
import styles from './AuditionlistStyale'
import * as AppConstants from '../Helper/AppConstants';
import OfflineNotice from '../Helper/OfflineNotice';
import Toast, { DURATION } from 'react-native-easy-toast'
import Icon from "react-native-vector-icons/MaterialIcons";
import Commneticon from "react-native-vector-icons/Fontisto";
import API from "../WebServices/API";
import { PostData, internet } from '../WebServices/webAPIRequest';
import Modal from "react-native-simple-modal";
import RNFetchBlob from 'rn-fetch-blob'
import Permissions from "react-native-permissions";
var RNFS = require('react-native-fs');
import FileViewer from 'react-native-file-viewer';
const localFile = `${RNFS.DocumentDirectoryPath}/temporaryfile.pdf`;

export default class Auditionlist extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Search: '',
            thumbnail: '',
            dataSource: [],
            isRefreshing: false,
            page: 1,
            nexturl: '',
            open: false,
            AudioURL: '', Audioname: '', ScriptURL: '', Scriptname: ''
        }
        this._renderRow = this._renderRow.bind(this);
        this.lists = [
            this.props.navigation.addListener('didFocus', () => { this.componentDidFocus() })
        ];
    }
    componentDidFocus() {
        this.setState({ page: 1, dataSource: [] })
        this.ViewroleAuditioin();
    }
    renderFooter = () => {
        if (this.state.isRefreshing == true) {
            return (
                <ActivityIndicator
                    style={{ color: '#000' }}
                />
            );
        }
        return null;

    };
    handleLoadMore() {
        if (this.state.nexturl == '') {
            this.setState({ isRefreshing: false });
        }
        else {
            this.setState({ page: this.state.page + 1, isRefreshing: true, isLoading: false });
            this.WsAllProject();
        }
    };
    onRefresh() {
    }
    ViewroleAuditioin() {
        Keyboard.dismiss();
        if (global.isConnected) {
            if (this.state.isRefreshing == true) {
            }
            else {
                this.setState({ isLoading: true, });
            }
            APIData = JSON.stringify({
                role_id: this.props.navigation.state.params.rolid,
            });
            PostData(API.RoleAuditionlist + '?page=' + this.state.page, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false });
                    if (responseJson.statusCode == 1) {
                        this.setState({
                            dataSource: [
                                ...this.state.dataSource,
                                ...responseJson.data
                            ],
                            nexturl: responseJson.next_page_url,
                        })
                    }
                    else {
                        // Alert.alert(responseJson.message);
                    }
                }).catch(() => {
                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }
    }
    donwloadmethid() {
        var name = this.state.Audioname;
        RNFS.readDir(Platform.OS == 'ios' ? RNFetchBlob.fs.dirs.DocumentDir : RNFetchBlob.fs.dirs.SDCardApplicationDir) // On Android, use "RNFS.DocumentDirectoryPath" (MainBundlePath is not defined)
            .then((result) => {
                let filteredData = result.filter(function (item) {
                    return item.name.includes(name);
                });
                this.setState({ video: '', thumbnail: '' })
                if (filteredData == '') {
                    this.setState({ isLoading: true });
                    if (Platform.OS == 'ios') {
                        var filePath = `${RNFetchBlob.fs.dirs.DocumentDir}/${decodeURI(this.state.Audioname).split('uploads%2')[1]}`
                        RNFetchBlob
                            .config({
                                path: filePath,
                            })
                            .fetch('GET', this.state.AudioURL, {
                            })
                            .then((res) => {
                                this.setState({ isLoading: false });
                                this.setState({ isLoading: false })
                                this.props.navigation.navigate(AppConstants.SCREENS.AUDIOPLYER, { audio: res.path(), thumbnail: '' })
                            })
                    } else {
                        Permissions.check("storage")
                            .then(status => {
                                if (status == "undetermined") {
                                    Permissions.request('storage')
                                        .then((data) => {
                                            this.Downloadfile();
                                        })
                                        .catch(() => {
                                        });
                                } else {
                                    this.Downloadfile();
                                }
                            })
                            .then(status => {
                                this.setState({ status });
                            });
                    }
                }
                else {
                    this.props.navigation.navigate(AppConstants.SCREENS.AUDIOPLYER, { audio: filteredData[0].path, thumbnail: '' })
                }
            })
            .catch(() => {
            });

    }
    Downloadfile() {
        let dirs = RNFetchBlob.fs.dirs
        var filePath = `${dirs.SDCardApplicationDir}/${decodeURI(this.state.videofilename).split('uploads%2')[1]}`
        // const Path = RNFetchBlob.fs.dirs.DocumentDir;
        RNFetchBlob
            .config({
                addAndroidDownloads: {
                    title: 'Topup Report',
                    useDownloadManager: true,
                    mediaScannable: true,
                    notification: true,
                    fileCache: true,
                    description: 'File downloaded by download manager.',
                    path: filePath
                },
            })
            .fetch('GET', this.state.AudioURL)
            .then((res) => {
                this.setState({ isLoading: false })
                this.props.navigation.navigate(AppConstants.SCREENS.AUDIOPLYER, { audio: res.path(), thumbnail: '' })

            }).catch((error) => {
            });
    }

    _renderRow(item, index, ) {
        return (
            <View style={styles.FlatlistContainer1}>
                <View style={{
                    marginTop: AppConstants.getDeviceHeight(0.5),
                    shadowOffset: { width: 0, height: 2 },
                }}>
                    <View style={{ width: AppConstants.getDeviceWidth(96), justifyContent: 'center' }}>
                        <View style={styles.titlename}>
                            <Text style={styles.projecttitle}>{item.item.project_name}</Text>
                            <Text style={styles.time}>{item.item.submission_date}</Text>
                        </View>
                        <View style={{ width: AppConstants.getDeviceWidth(96), flexDirection: 'row', }}>
                            <Text style={[styles.projectrole]}>Role: </Text>
                            <Text style={styles.projectdesc}>{item.item.role_name}</Text>
                            {item.item.audition_audio == '' && item.item.audition_document == '' ? null : <TouchableOpacity onPress={() => {
                                this.setState({
                                    AudioURL: item.item.audition_audio,
                                    Audioname: item.item.audition_audio_file_name,
                                    ScriptURL: item.item.audition_document,
                                    Scriptname: item.item.audition_document_file_name,
                                    open: true
                                })

                            }} style={styles.navigationview}>
                                <Icon name="cloud-download" color='#7B8F99' size={AppConstants.moderateScale(20)} style={{ alignSelf: 'center' }} />

                            </TouchableOpacity>}

                        </View>
                        <TouchableOpacity onPress={() => { this.props.navigation.navigate('Videonew', { video: item.item.audition_video, thumbnail: item.item.audition_video_thumbnail }) }} style={styles.videotouch}>
                            <ImageBackground source={{ uri: item.item.audition_video_thumbnail }} style={styles.playicon}>
                                <Icon name="play-circle-outline" color='#7B8F99' size={AppConstants.moderateScale(60)} style={{ alignSelf: 'center' }} />
                            </ImageBackground>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: 'row', marginBottom: AppConstants.getDeviceHeight(1),marginTop: AppConstants.getDeviceHeight(1) }}>
                        <Text style={styles.comment2}>{item.item.artist_name}</Text>
                        <Commneticon name="comment" color={AppConstants.COLORS.PROFILEBACKGROUND}
                            size={AppConstants.moderateScale(15)} style={styles.commenticon} />
                        <Text style={styles.comment}>Comments</Text>
                    </View>
                </View>
            </View>
        );
    }
    render() {
        return (
            <View style={styles.mainContainer}>
                <StatusBar barStyle="light-content" backgroundCopr={AppConstants.SHADOWCOLORS.INTRO} animated translucent></StatusBar>
                <OfflineNotice />
                <View style={styles.mainContainer}>
                    <Toast
                        ref="toast"
                        style={[AppConstants.CommonStyles.ToastStyle1, { justifyContent: 'flex-end', height: AppConstants.getDeviceHeight(11), }]}
                        position="top"
                        positionValue={AppConstants.getDeviceHeight(0)}
                        fadeInDuration={750}
                        fadeOutDuration={1000}
                        textStyle={AppConstants.CommonStyles.ToastTextStyle}
                    />
                    <FlatList
                        scrollEnabled={true}
                        data={this.state.dataSource}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={this._renderRow.bind(this)}
                        refreshControl={
                            <RefreshControl refreshing={this.state.isRefreshing}
                            />
                        }
                        ListFooterComponent={this.renderFooter.bind(this)}
                        onEndReachedThreshold={0.4}
                        onEndReached={this.handleLoadMore.bind(this)}
                    />
                </View>
                <Modal
                    closeOnTouchOutside={true}
                    backdrop={true}
                    open={this.state.open}
                    closeOnTouchOutside={false}
                    modalDidOpen={this.modalDidOpen}
                    modalDidClose={this.modalDidClose}
                    modalStyle={{
                        margin: 0,
                        justifyContent: "center",
                        alignItems: "center",
                        padding: 0,
                        borderRadius: Platform.OS === 'ios' ? 5 : 5,

                        width: AppConstants.getDeviceWidth(89.47),
                        marginLeft: AppConstants.getDeviceWidth(6),
                    }}>
                    <View style={{ width: AppConstants.getDeviceWidth(100), }}>
                        {this.state.AudioURL == '' ? null : <TouchableOpacity onPress={() => {
                            this.setState({ open: false })
                            this.donwloadmethid();
                        }}>
                            <Text style={[styles.projectrole, styles.donwloadaudio]}>Audio Download</Text>
                        </TouchableOpacity>}
                        {this.state.ScriptFile == '' || this.state.AudioURL == '' ? null : <View style={styles.viewdonwload}></View>}
                        {this.state.ScriptFile == '' ? null : <TouchableOpacity onPress={() => {
                            this.setState({ isLoading: true })
                            const options = {
                                fromUrl: this.state.ScriptURL,
                                toFile: localFile
                            };
                            RNFS.downloadFile(options).promise
                                .then((responce) => {
                                    this.setState({ isLoading: false, open: false })
                                    FileViewer.open(localFile)
                                }).then(() => {
                                })
                                .catch(() => {
                                });
                        }}>
                            <Text style={[styles.projectrole,styles.donwloadscript]}>Script Download</Text>
                        </TouchableOpacity>}
                    </View>
                </Modal>
                {this.state.dataSource == '' ?
                    <View style={styles.viewerorr}>
                        <Text style={styles.error}>No Audition Found!</Text>
                    </View> : null}
                {this.state.isLoading ? <View style={AppConstants.CommonStyles.spinner}>
                    <ActivityIndicator size='large' ></ActivityIndicator>
                </View> : null}
            </View>
        )
    }
}