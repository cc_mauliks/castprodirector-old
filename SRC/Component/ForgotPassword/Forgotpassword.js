import React, { Component } from 'react';
import {
    TextInput,
    Image,
    View,
    Text,
    StatusBar,
    ImageBackground,
    TouchableOpacity,
    Platform,
    Keyboard,
    Alert,
    ActivityIndicator
} from 'react-native';
import styles from './ForgotpasswordStyle'
import * as AppConstants from '../Helper/AppConstants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import OfflineNotice from '../Helper/OfflineNotice';
import Toast, { DURATION } from 'react-native-easy-toast'
import { PostData, internet } from '../WebServices/webAPIRequest';
import Iconss from "react-native-vector-icons/MaterialCommunityIcons";
import API from "../WebServices/API";

const Images = {
    backgroundImage: 'background',
    Loginlogo: 'loginlogo',

}
export default class Forgotpassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            verifyotp: '',
            matchotp: global.otp,
            isLoading: false,
            Emailid: '',
            Emailid1: false,
            Emailid11: false,
        };
    }
    validateEmail = Email => {
        var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return re.test(Email);
    };
    validator() {
        if (this.state.Emailid == '') {
            this.setState({ Emailid1: true })
            // this.refs.toast.show(AppConstants.Messages.NOEMAIL, DURATION.LENGTH_LONG);
        }
        else if (!this.validateEmail(this.state.Emailid)) {
            this.setState({ Emailid11: true })
            // this.refs.toast.show(AppConstants.Messages.NOEMAILINVALIDEMAILADDRESS, DURATION.LENGTH_LONG);
        }
        else {
            this.WSResendOTP();
        }
    }

    WSResendOTP() {
        Keyboard.dismiss();
        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({
                email_address: this.state.Emailid
            });
            PostData(API.Chnagepassword, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false });
                    if (responseJson.statusCode == 1) {
                        Alert.alert(
                            'Forgot Password',
                            responseJson.message,
                            [
                                {
                                    text: 'Cancel',
                                    onPress: () => { },
                                    style: 'cancel',
                                },

                                { text: 'OK', onPress: () => this.props.navigation.navigate(AppConstants.SCREENS.LOGIN) },
                            ],
                            { cancelable: false },
                        );
                    }
                    else {
                        Alert.alert(responseJson.message);
                    }
                })
                .catch(() => {
                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }
    }
    render() {
        return (
            <KeyboardAwareView doNotForceDismissKeyboardWhenLayoutChanges={true}>
                <StatusBar barStyle="light-content" backgroundCopr={AppConstants.SHADOWCOLORS.INTRO} animated translucent></StatusBar>
                <OfflineNotice />
                <View style={styles.mainContainer}>
                    {/* <ImageBackground resizeMode="stretch" source={{ uri: Images.backgroundImage }} style={styles.backgroundImage}> */}
                    <Toast
                        ref="toast"
                        style={[AppConstants.CommonStyles.ToastStyle, { justifyContent: 'flex-end', height: AppConstants.getDeviceHeight(8), }]}
                        position="top"
                        positionValue={AppConstants.getDeviceHeight(0)}
                        fadeInDuration={750}
                        fadeOutDuration={1000}
                        textStyle={AppConstants.CommonStyles.ToastTextStyle}
                    />
                    <View >
                        <Iconss
                            onPress={() => this.props.navigation.goBack()}
                            name="arrow-left"
                            style={{ marginLeft: AppConstants.getDeviceWidth(4), marginTop: AppConstants.getDeviceHeight(this.props.navigation.getParam('header') != null && !this.props.navigation.getParam('header') ? 6.8 : Platform.OS === 'ios' ? 0 : 3) }}
                            size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS25)}
                            color={AppConstants.COLORS.WHITE}
                        />
                    </View>
                    <View style={styles.logoContainer}>
                        <Image style={styles.lginlogo} source={{ uri: Images.Loginlogo }} />
                    </View>
                    <View style={styles.TextContainer}>
                        <Text style={styles.Text}>
                            Enter your email address to reset your password
                                </Text>
                    </View>

                    <View style={styles.textboxContainer}>
                        <TextInput style={styles.textboxText}
                            placeholder="Enter Email Id"
                            placeholderTextColor={AppConstants.COLORS.PLACEHOLDARLOGIN}
                            underlineColorAndroid="transparent"
                            returnKeyType={"next"}
                            blurOnSubmit={false}
                            autoCapitalize="none"
                            onChangeText={(Emailid) => this.setState({ Emailid, Emailid1: false, Emailid11: false })} />
                        <View style={{
                            width: AppConstants.getDeviceWidth(87),
                            height: AppConstants.getDeviceHeight(0.1),
                            backgroundColor: AppConstants.COLORS.LANGAUGE,
                        }}></View>
                    </View>
                    {this.state.Emailid1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(6), marginTop: AppConstants.getDeviceHeight(2) }]}>{AppConstants.Messages.NOEMAIL}</Text> : null}
                    {this.state.Emailid11 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(6), marginTop: AppConstants.getDeviceHeight(2) }]}>{AppConstants.Messages.NOEMAILINVALIDEMAILADDRESS}</Text> : null}

                    <View style={{ height: AppConstants.getDeviceHeight(15), }}>
                        <TouchableOpacity style={{
                            height: AppConstants.getDeviceHeight(15), marginTop: AppConstants.getDeviceHeight(5), marginLeft: AppConstants.getDeviceWidth(5),
                            marginRight: AppConstants.getDeviceWidth(5),
                        }} onPress={() => { this.validator() }}>
                            {/* <LinearGradient start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }} colors={['#fff59a', '#d6b469', '#d6b469',]} style={{ height: AppConstants.getDeviceHeight(5), bottom: 0, alignItems: 'center', justifyContent: 'center' }}> */}
                            <Text style={styles.logintext}>
                                Send
                                    </Text>
                            {/* </LinearGradient> */}
                        </TouchableOpacity>
                    </View>
                    {/* </ImageBackground> */}
                </View>
                {this.state.isLoading ? <View style={AppConstants.CommonStyles.spinner}>
                    <ActivityIndicator size='large' color={AppConstants.COLORS.SIGNUP}></ActivityIndicator>
                </View> : null}
            </KeyboardAwareView>
        )
    }
}