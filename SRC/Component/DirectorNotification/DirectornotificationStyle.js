import * as AppConstants from '../Helper/AppConstants';
export default {
    mainContainer: {
        flex: 1,
        backgroundColor: AppConstants.COLORS.VIEWBACKGROUND
    },
    textboxText: {
        width: AppConstants.getDeviceWidth(87),
        height: AppConstants.getDeviceHeight(6.6),
        color: AppConstants.COLORS.BLACK,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS13),
        marginLeft: AppConstants.getDeviceWidth(3.53),
        paddingLeft: AppConstants.getDeviceWidth(5)
    },
    FlatlistContainer: {
        flexDirection: 'row',
        // height: AppConstants.getDeviceHeight(10),
        width: AppConstants.getDeviceWidth(100),
      
       
        
        // shadowOpacity: 0.8,
        // shadowRadius: 2,
        // elevation: 1,

    },
    FlatlistContainer1: {
        flexDirection: 'row',
        // height: AppConstants.getDeviceHeight(38),
        width: AppConstants.getDeviceWidth(96),
       
        backgroundColor: AppConstants.COLORS.WHITE,
        shadowOffset: { width: 0, height: 2 },
        shadowColor: AppConstants.SHADOWCOLORS.TEXT_SHADOW,
        shadowOpacity: 0.8,
        shadowRadius: 3,
        elevation: 3,
        borderRadius: 3,
        marginLeft: AppConstants.getDeviceWidth(2),
        marginRight: AppConstants.getDeviceWidth(2),
        marginBottom: AppConstants.getDeviceWidth(2),
        marginTop: AppConstants.getDeviceWidth(2),
    },
    projecttitle: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND, fontWeight: 'bold',
        marginLeft: AppConstants.getDeviceWidth(5)
    },
    time: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.datecolor,
        fontWeight: 'bold',
     
        alignSelf: 'center',
        marginRight:AppConstants.getDeviceWidth(5)
    },
    projectrole: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND, fontWeight: 'bold',
        marginLeft: AppConstants.getDeviceWidth(5)
    },
    projectdesc: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        marginLeft: AppConstants.getDeviceWidth(2),
      
        justifyContent: 'center', 
        alignSelf: 'center' ,
        padding:10,
        fontWeight: 'bold',
    },
    projectdesc1: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.datecolor,
        marginLeft: AppConstants.getDeviceWidth(2),
       
        justifyContent: 'center', 
        alignSelf: 'center' ,
        padding:10,
        
    },
    projectdate: {
        fontSize: AppConstants.moderateScale(9.5),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.datecolor, fontWeight: 'bold',
        marginLeft: AppConstants.getDeviceWidth(5),
        marginRight: AppConstants.getDeviceWidth(0.5)
    },
    viewerorr: {
        flex: 1,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        justifyContent: 'center',
        // marginTop: AppConstants.getDeviceHeight(6.6),
        marginBottom: AppConstants.getDeviceHeight(8)
    },
    error: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        marginTop: 0,
        textAlign: 'center',
        fontWeight: '500',
    },
}