import React, { Component } from 'react';

import {
    ActivityIndicator,
    TouchableOpacity,
    View,
    Text,
    StatusBar,
    FlatList,
    Keyboard,
    Alert
} from 'react-native';
import styles from './DirectornotificationStyle'
import * as AppConstants from '../Helper/AppConstants';
import OfflineNotice from '../Helper/OfflineNotice';
import Toast, { DURATION } from 'react-native-easy-toast'
import Icons from "react-native-vector-icons/AntDesign";
import API from "../WebServices/API";
import { PostData, internet } from '../WebServices/webAPIRequest';
import Tab from '../TabComponent/Tab'
export default class Directonotification extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Search: '',
            dataSource: [],
            errormessage: '',
            isLoading: false,
        }
        this._renderRow = this._renderRow.bind(this);
        this.lists = [
            this.props.navigation.addListener('didFocus', () => { this.componentDidFocus() })
        ];
    }
    componentDidFocus() {
        this.GetNotification();
    }
    GetNotification() {
        Keyboard.dismiss();
        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({
            });
            PostData(API.GetNotification, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false });
                    if (responseJson.statusCode == 1) {
                        this.setState({ dataSource: responseJson.data })
                    }
                    else {
                        this.setState({ errormessage: responseJson.message, dataSource: [] });
                        if (responseJson.message !== 'No notifications found!') {
                            Alert.alert(responseJson.message);
                        }


                    }
                }).catch(() => {
                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }
    }
    ReadNotificaiton(notificationid) {
        Keyboard.dismiss();
        if (global.isConnected) {
            // this.setState({ isLoading: true });
            APIData = JSON.stringify({
                notification_id: notificationid
            });
            PostData(API.ReadNotificaion, APIData)
                .then((responseJson) => {
                    if (responseJson.statusCode == 1) {
                    }
                    else {
                        this.setState({ errormessage: responseJson.message });
                        Alert.alert(responseJson.message);
                    }
                }).catch(() => {
                    this.setState({ isLoading: false });
                    this.refs.toast.show(AppConstants.Messages.APIERROR, DURATION.LENGTH_LONG);
                });
        }
    }

    _renderRow(item, index) {




        return (
            <View style={styles.FlatlistContainer1}>
                <TouchableOpacity
                    onPress={() => {
                        if (item.item.screen_open_type == 'Actor') {
                            this.props.navigation.navigate(AppConstants.SCREENS.VIEWARTISTPROFILE, { artistid: item.item.open_id })
                        }
                        else if (item.item.screen_open_type == 'Audition') {
                            this.props.navigation.navigate(AppConstants.SCREENS.VIEWAUDITION, { auditionid: item.item.open_id })
                        }
                        this.ReadNotificaiton(item.item.notification_id)
                    }}>
                    <View style={styles.FlatlistContainer}>
                        <View style={{ width: AppConstants.getDeviceWidth(96), flexDirection: 'row', }}>
                            <View style={{ width: AppConstants.getDeviceWidth(70), }}>
                                <Text style={item.item.message_read_status == 'Y' ? styles.projectdesc1 : styles.projectdesc}>{item.item.message}</Text>
                            </View>
                            <View style={{ width: AppConstants.getDeviceWidth(25), justifyContent: 'center' }}>
                                <Text style={styles.time}>{item.item.date}</Text>
                                <Text style={styles.time}>{item.item.time}</Text>
                            </View>




                        </View>

                    </View>
                </TouchableOpacity>
            </View>
        );
    }
    render() {
        return (
            <View style={styles.mainContainer}>
                <StatusBar barStyle="light-content" backgroundCopr={AppConstants.SHADOWCOLORS.INTRO} animated translucent></StatusBar>
                <OfflineNotice />
                <View style={styles.mainContainer}>
                    <Toast
                        ref="toast"
                        style={[AppConstants.CommonStyles.ToastStyle1, { justifyContent: 'flex-end', height: AppConstants.getDeviceHeight(11), }]}
                        position="top"
                        positionValue={AppConstants.getDeviceHeight(0)}
                        fadeInDuration={750}
                        fadeOutDuration={1000}
                        textStyle={AppConstants.CommonStyles.ToastTextStyle}
                    />

                    <FlatList
                        scrollEnabled={true}
                        data={this.state.dataSource}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={this._renderRow.bind(this)} />
                </View>

                {this.state.dataSource == '' && this.state.isLoading == false ?
                    <View style={styles.viewerorr}>
                        <Text style={styles.error}>{this.state.errormessage}</Text>
                    </View> : null}
                {this.state.isLoading ? <View style={AppConstants.CommonStyles.spinner}>
                    <ActivityIndicator size='large' ></ActivityIndicator>
                </View> : null}

            </View>
        );
    }
}