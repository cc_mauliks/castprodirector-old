import * as AppConstants from '../Helper/AppConstants';
export default {
    mainContainer: {
        flex: 1,
        marginBottom: AppConstants.getDeviceHeight(4),
        backgroundColor: AppConstants.COLORS.VIEWBACKGROUND
    },
    textboxText: {
        width: AppConstants.getDeviceWidth(96),
        height: AppConstants.getDeviceHeight(5),
        color: AppConstants.COLORS.BLACK,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS13),
        marginLeft: AppConstants.getDeviceWidth(2),
        paddingLeft: AppConstants.getDeviceWidth(10),
        backgroundColor: AppConstants.COLORS.WHITE,
        padding: 0,
        margin: 0,
        borderWidth: 0,
        borderRadius: 5, position: 'absolute', alignSelf: 'center',
    },
    textinputview: {
        flexDirection: 'row',
        backgroundColor: AppConstants.COLORS.PROFILEBACKGROUND,
        height: AppConstants.getDeviceHeight(7),
        width: AppConstants.getDeviceWidth(100),
        marginTop: AppConstants.getDeviceHeight(1),
    },
    FlatlistContainer: {
        flexDirection: 'row',
        // height: AppConstants.getDeviceHeight(11),
        width: AppConstants.getDeviceWidth(100),
        marginTop: AppConstants.getDeviceHeight(0.5),
        shadowOffset: { width: 0, height: 2 },
        shadowColor: AppConstants.SHADOWCOLORS.TEXT_SHADOW,


    },
    FlatlistContainer1: {
        flexDirection: 'row',
        // height: AppConstants.getDeviceHeight(11),
        flex: 1,
        width: AppConstants.getDeviceWidth(96),
        marginTop: AppConstants.getDeviceHeight(0.2),
        backgroundColor: AppConstants.COLORS.WHITE,
        shadowOffset: { width: 0, height: 2 },
        shadowColor: AppConstants.SHADOWCOLORS.TEXT_SHADOW,
        shadowOpacity: 0.8,
        shadowRadius: 3,
        elevation: 3,
        borderRadius: 3,
        marginLeft: AppConstants.getDeviceWidth(2),
        marginRight: AppConstants.getDeviceWidth(2),
        marginBottom: AppConstants.getDeviceWidth(2),
        marginTop: AppConstants.getDeviceWidth(2),
    },
    projecttitle: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        fontWeight: 'bold',
        marginLeft: AppConstants.getDeviceWidth(5),
        // marginTop: AppConstants.getDeviceHeight(1),
        // alignItems: 'center'
    },
    projectrole: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND, fontWeight: 'bold',
        marginLeft: AppConstants.getDeviceWidth(5)
    },
    projectdesc: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        marginLeft: AppConstants.getDeviceWidth(5),

    },
    projectdate: {
        fontSize: AppConstants.moderateScale(9.5),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.datecolor, fontWeight: 'bold',
        marginLeft: AppConstants.getDeviceWidth(5),
        marginRight: AppConstants.getDeviceWidth(0.5)
    },
    error: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        marginTop: 0,
        textAlign: 'center',
        fontWeight: '500',
    },
    viewerorr: {
        flex: 1,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        justifyContent: 'center',
        marginTop: AppConstants.getDeviceHeight(6.6),
        marginBottom: AppConstants.getDeviceHeight(8)
    },
    dateview: {
        width: AppConstants.getDeviceWidth(28),
        height: AppConstants.getDeviceHeight(10),
        justifyContent: 'center',
        position: 'absolute',
        right: 0,
        alignSelf: 'center',
        marginRight: AppConstants.getDeviceWidth(2),

    },
    dateview1: {
        width: AppConstants.getDeviceWidth(28),
        height: AppConstants.getDeviceHeight(6),

        // flexDirection:'row'
    },
    dateview2: {
        width: AppConstants.getDeviceWidth(28),
        height: AppConstants.getDeviceHeight(6),


        flexDirection: 'row',
        marginTop: AppConstants.getDeviceHeight(3)
    },
    shortlist: {
        width: AppConstants.getDeviceWidth(25),
        height: AppConstants.getDeviceHeight(3.5),
        backgroundColor: AppConstants.COLORS.PROFILEINACTIVE,

        borderRadius: AppConstants.moderateScale(7),
        marginRight: AppConstants.getDeviceWidth(5),
        justifyContent: 'center',

    },
    shorting: {
        alignSelf: 'center',
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        justifyContent: 'center',
        fontWeight: 'bold',
    },
    modalRenderView: {
        flex: 1,
        position: "absolute",
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        justifyContent: "center"
    },
    time: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,


        // marginRight: AppConstants.getDeviceWidth(5)
    },
    time1: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        fontWeight: 'bold',
        // marginRight: AppConstants.getDeviceWidth(17)
    },
    filter: {
        height: 50,

        width: 50,
        borderRadius: 50 / 2,
        backgroundColor: AppConstants.COLORS.PROFILEINACTIVE,
        shadowOffset: { width: 0, height: 2 },
        shadowColor: AppConstants.SHADOWCOLORS.TEXT_SHADOW,
        shadowOpacity: 0.8,
        shadowRadius: 3,
        elevation: 3,
        marginBottom: AppConstants.getDeviceHeight(10),
        position: 'absolute',
        right: 0,
        bottom: 0,
        marginRight: AppConstants.getDeviceWidth(5),
        justifyContent: 'center'
    },
    filtericon: {

        alignSelf: 'center',

    },
    projecttitle2: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEINACTIVE,
        position: 'absolute',
        right: 0,
        marginRight: AppConstants.getDeviceWidth(2),
        marginLeft: AppConstants.getDeviceWidth(5),
        fontWeight: 'bold',
        alignSelf: 'center'
    },
    // projecttitle: {
    //     fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
    //     fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
    //     color: AppConstants.COLORS.PROFILEINACTIVE,

    //     fontWeight: 'bold',
    //     alignSelf: 'center'
    // },
}