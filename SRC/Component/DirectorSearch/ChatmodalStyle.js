import * as AppConstants from '../Helper/AppConstants';
import { Platform } from 'react-native';


export default {
    mainContainer: {
        flex: 1,
      
        backgroundColor: AppConstants.COLORS.TABVIEW,
       
    },
    FlatlistContainer: {
        flexDirection: 'row',
     
        width: AppConstants.getDeviceWidth(99.50),
        shadowOffset: { width: 0, height: 2 },
        shadowColor: AppConstants.SHADOWCOLORS.TEXT_SHADOW,
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
    },
    sentat: {
        backgroundColor: 'white'
        // marginRight: AppConstants.getDeviceHeight(2),
        // marginLeft: AppConstants.getDeviceHeight(0.5),
    },
   
    header: {
        height: AppConstants.getDeviceWidth(10),

        borderRadius: Platform.OS === 'ios' ? 30 : 5,
        width: AppConstants.getDeviceWidth(89.47),
    },
    text: {
        justifyContent: "center",
        alignItems: "center",
        justifyContent: 'space-between',
        alignSelf: 'center',
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        fontWeight: AppConstants.WEIGHT.FONTWEIGHT
    },
    header1: {
        height: AppConstants.getDeviceWidth(13),
        borderTopRightRadius: Platform.OS === 'ios' ? 30 : 5,
        borderTopLeftRadius: Platform.OS === 'ios' ? 30 : 5,
        width: AppConstants.getDeviceWidth(89.47),
    },
    swipeline: {
        width: AppConstants.getDeviceWidth(0.5),
        //width: AppConstants.getDeviceWidth(0.05),
        //backgroundColor: AppConstants.COLORS.GRADIENT_1,
    },
    textname: {
        color: AppConstants.COLORS.BLACK,
        fontFamily: AppConstants.FONTFAMILY.fontFamily_1,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS13),
    },
    model: {
        margin: 0,
        justifyContent: "center",
        alignItems: "center",
        padding: 0,
        borderRadius: Platform.OS === 'ios' ? 5 : 5,
        height: AppConstants.getDeviceHeight(78),
        width: AppConstants.getDeviceWidth(89.47),
        marginLeft: AppConstants.getDeviceWidth(6),
       marginBottom:AppConstants.getDeviceHeight(5)
    },
}
