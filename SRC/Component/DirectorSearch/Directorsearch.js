import React, { Component } from 'react';
import {
    TextInput,
    Keyboard,
    View,
    Text,
    StatusBar,
    ActivityIndicator,
    TouchableOpacity,
    FlatList,
    Alert
} from 'react-native';
import styles from './DirectorsearchStyle'
import * as AppConstants from '../Helper/AppConstants';
import OfflineNotice from '../Helper/OfflineNotice';
import Toast, { DURATION } from 'react-native-easy-toast'
import Icons from "react-native-vector-icons/AntDesign";
import Icon from "react-native-vector-icons/FontAwesome";
import API from "../WebServices/API";
import { PostData, internet } from '../WebServices/webAPIRequest';
import { Avatar } from 'react-native-elements';
import Modal from "react-native-modal";
import CustomMultiPicker from "./multipleSelect";
const Images = {
    Bio: 'bio',
}
var array = [];
export default class Directorsearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            isRefreshing: false,
            Search: '',
            Project: [],
            Project1: [],
            projecttext: '',
            modal1: '0',
            skill: '',
            skilllarray: [],
            dataSource: [],
            nexturl: '',
            page: 1
        }
        this.page = 1;
        this.onEndReachedCalledDuringMomentum = true;

        this.lists = [
            this.props.navigation.addListener('didFocus', () => { this.componentDidFocus() })
        ];
        this._renderRow = this._renderRow.bind(this);
    }
    componentDidFocus() {

    }
    componentDidMount() {
        array = [];
        this.setState({ page: 1, Project: [], dataSource: [''] })
        this.WsAllProject('', '', 1);
    }

    WsAllProject(skill, filter, page) {
        if (global.isConnected) {
            if (this.state.isRefreshing == true) {
            }
            else {
                this.setState({ isLoading: true, });
            }
            APIData = JSON.stringify({
                filter_value: filter,
                search_keyword: skill,
            });
            PostData(API.Directorsearch + '?page=' + page, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false, isRefreshing: false });
                    if (responseJson.statusCode == 1) {
                        array.push(...responseJson.data)
                        this.setState({
                            Project: array,
                            nexturl: responseJson.next_page_url,
                        });
                    }
                    else {
                        this.onEndReachedCalledDuringMomentum = true;
                        if (responseJson.message == "No result found!") {
                            this.setState({ Project: [], });
                        }
                        else {
                            this.setState({ Project: [], });
                        }
                    }
                }).catch(() => {
                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }
    }
    SendProfileRequest(id) {
        Keyboard.dismiss();
        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({
                actor_id: id,
                status: 'Pending'
            });
            PostData(API.RequestForProfile, APIData)
                .then((responseJson) => {
                    if (responseJson.statusCode == 1) {
                        array = [];
                        this.setState({ Project: [] });
                        this.refs.toast1.show(responseJson.message)
                        this.WsAllProject(this.state.projecttext, this.state.dataSource[0], 1);
                    }
                    else {

                        Alert.alert(responseJson.message);
                    }
                }).catch(() => {
                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }
    }
    Getskill() {
        // if (global.isConnected) {
        this.setState({ isLoading: true });

        APIData = JSON.stringify({

        });
        PostData(API.GetSkill, APIData)
            .then((responseJson) => {
                this.setState({ isLoading: false, skilll: [] });
                var arr = [];
                responseJson.data.map((data, index) => {
                    arr.push(
                        data.skill,
                    )
                });
                this.setState({ skilllarray: arr });
            })
            .catch(() => {
                this.setState({ isLoading: false });
                Alert.alert(AppConstants.Messages.APIERROR);
            });
    }
    _renderRow(item, index) {

        var image = item.item.profile_img == '' ? Images.Bio : item.item.profile_img;
        return (
            <View style={styles.FlatlistContainer1}>
                <TouchableOpacity
                    onPress={() => { this.props.navigation.navigate(AppConstants.SCREENS.VIEWARTISTPROFILE, { artistid: item.item.id, auditoinrequestid: '' }) }}>
                    <View style={styles.FlatlistContainer}>
                        <TouchableOpacity onPress={() => { this.props.navigation.navigate(AppConstants.SCREENS.VIEWARTISTPROFILE, { artistid: item.item.id, auditoinrequestid: '' }) }} style={{ justifyContent: 'center' }}>
                            <Avatar
                                size={AppConstants.getDeviceHeight(9)}
                                overlayContainerStyle={{ backgroundColor: 'black' }}
                                containerStyle={{ alignSelf: 'center', backgroundColor: null, marginLeft: AppConstants.getDeviceWidth(2), marginBottom: AppConstants.getDeviceHeight(1), marginTop: AppConstants.getDeviceHeight(1) }}
                                rounded
                                source={{ uri: image }} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { this.props.navigation.navigate(AppConstants.SCREENS.VIEWARTISTPROFILE, { artistid: item.item.id, auditoinrequestid: '' }) }} style={{ width: AppConstants.getDeviceWidth(55), justifyContent: 'center', }}>
                            <Text style={styles.projecttitle}>{item.item.name}</Text>
                            {item.item.show_request_btn == 'Yes' ? null :
                                item.item.request_status == 'Accept' ? <View>
                                    <Text style={styles.projectdesc}>Mobile No: {item.item.mobile_no}</Text>
                                    <Text style={styles.projectdesc}>Email: {item.item.email_id}</Text>
                                </View> : item.item.show_request_btn == 'No' ? item.item.request_status == '' ? <View>
                                    <Text style={styles.projectdesc}>Mobile No: {item.item.mobile_no}</Text>
                                    <Text style={[styles.projectdesc, { marginBottom: AppConstants.getDeviceHeight(0.5) }]}>Email: {item.item.email_id}</Text>
                                </View> : null : null}
                        </TouchableOpacity>
                        <View style={styles.dateview}>
                            {item.item.show_request_btn == 'No' ? item.item.request_status == '' ? null : item.item.request_status == 'Pending' ? <View style={styles.dateview2}>
                                <Text style={styles.time1}>Status:</Text>
                                <Text style={styles.time}> {item.item.request_status}</Text>
                            </View> : null : <TouchableOpacity onPress={() => { this.SendProfileRequest(item.item.id) }} style={styles.shortlist}>
                                    <Text style={styles.shorting}>Send Request</Text>
                                </TouchableOpacity>}
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }


    renderFooter = () => {
        if (this.state.isRefreshing == true) {
            return (
                <ActivityIndicator
                    style={{ color: '#000' }}
                />
            );
        }
        return null;

    };
    handleLoadMore = () => {
        if (this.state.nexturl == '') {
            // this.refs.toast1.show('No Artist Found')
        } else {

            if (!this.onEndReachedCalledDuringMomentum) {
                this.page = this.page + 1
                this.setState({ page: this.state.page + 1, isRefreshing: true, isLoading: false });
                if (this.state.page == 1) {
                    this.setState({ page: this.state.page + 1, isRefreshing: true, isLoading: false });
                }
                this.WsAllProject(this.state.projecttext, this.state.dataSource[0], this.page);
                this.onEndReachedCalledDuringMomentum = true;
            }
        }


    };
    onRefresh() {

    }

    render() {
        return (
            <View style={styles.mainContainer}>

                <StatusBar barStyle="light-content" backgroundCopr={AppConstants.SHADOWCOLORS.INTRO} animated translucent></StatusBar>
                <OfflineNotice />
                <View style={styles.mainContainer}>
                    <Toast
                        ref="toast"
                        style={[AppConstants.CommonStyles.ToastStyle1, { justifyContent: 'flex-end', height: AppConstants.getDeviceHeight(11), }]}
                        position="top"
                        positionValue={AppConstants.getDeviceHeight(0)}
                        fadeInDuration={750}
                        fadeOutDuration={1000}
                        textStyle={AppConstants.CommonStyles.ToastTextStyle}
                    />
                    <Toast
                        ref="toast1"
                        style={[AppConstants.CommonStyles.ToastStyle2, { height: AppConstants.getDeviceHeight(6.5), }]}
                        position='bottom'
                        positionValue={200}
                        fadeInDuration={1000}
                        fadeOutDuration={1000}
                        textStyle={AppConstants.CommonStyles.ToastTextStyle1} />
                    <View style={styles.textinputview}>
                        <TextInput style={styles.textboxText}
                            placeholder="Search Artist"
                            placeholderTextColor={AppConstants.COLORS.PLACEHOLDARLOGIN}
                            underlineColorAndroid="transparent"                                        // underlineColorAndroid="transparent"
                            returnKeyType={"search"}
                            value={this.state.projecttext}
                            autoCapitalize="none"
                            onSubmitEditing={() => {
                                this.setState({ page: 1 })
                                this.page=1;
                                array = [];
                                this.WsAllProject(this.state.projecttext, this.state.dataSource[0], 1);
                            }}
                            onChangeText={(text) => this.setState({ projecttext: text })} />
                        <Icons onPress={() => { this.WsAllProject() }} name="search1" color={AppConstants.COLORS.LANGAUGE} style={{ position: 'absolute', marginLeft: AppConstants.getDeviceWidth(5), paddingTop: AppConstants.getDeviceHeight(2) }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS18)} />
                        {this.state.projecttext == '' ? null : <Icons onPress={() => {
                            this.setState({ projecttext: '', Project: [], })
                            array = [];

                            this.WsAllProject('', this.state.dataSource[0], 1);
                        }} name="closecircle" color={AppConstants.COLORS.LANGAUGE} style={{ position: 'absolute', right: 0, marginRight: AppConstants.getDeviceWidth(5), paddingTop: AppConstants.getDeviceHeight(2) }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS18)} />}
                    </View>

                    <FlatList
                        scrollEnabled={true}
                        data={this.state.Project}

                        style={{ borderBottomWidth: 0 }}
                        containerStyle={{ borderBottomWidth: 0 }}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={this._renderRow.bind(this)}

                        onEndReachedThreshold={0}
                        onEndReached={(this.handleLoadMore.bind(this))}
                        ListFooterComponent={this.renderFooter(this.state.isRefreshing)}
                        onMomentumScrollBegin={() => {
                                this.onEndReachedCalledDuringMomentum = false;
                        }}
                    />
                </View>

                {this.state.Project == '' && this.state.isLoading == false ?
                    <View style={styles.viewerorr}>
                        <Text style={styles.error}>There are no artists to show yet!</Text>
                    </View> : null}
                {this.state.isLoading ? <View style={AppConstants.CommonStyles.spinner}>
                    <ActivityIndicator size='large' ></ActivityIndicator>
                </View> : null}
               
                {this.state.modal1 == 0 ? <TouchableOpacity onPress={() => {
                    this.setState({ modal1: '1', Project: [] })
                    this.Getskill();
                }} style={styles.filter}>
                    <Icon name="filter" color={AppConstants.COLORS.WHITE} style={styles.filtericon} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS26)} />

                </TouchableOpacity> : null}
                <Modal
                    isVisible={this.state.modal1 == 0 ? false : true}
                    deviceHeight={AppConstants.getDeviceHeight(100)}
                    deviceWidth={AppConstants.getDeviceWidth(100)}>
                    <View style={styles.mainContainer1}>
                        <View style={{ height: AppConstants.getDeviceHeight(4), justifyContent: 'center', }}>
                        </View>
                        <CustomMultiPicker
                            options={this.state.skilllarray}
                            search={true} // should show search bar?
                            multiple={true} //
                            placeholder={"Search"}
                            placeholderTextColor={'#757575'}
                            returnValue={"label"} // label or value
                            callback={(res) => { this.setState({ dataSource: res }); }} // callback, array of selected items
                            rowBackgroundColor={"#eee"}
                            close={(call) => {
                                this.WsAllProject(this.state.projecttext, this.state.dataSource[0], 1);
                                this.setState({ modal1: 0 })
                            }}
                            rowHeight={AppConstants.getDeviceHeight(7)}
                            rowRadius={5}
                            iconColor={AppConstants.COLORS.HEDARBACKGROUND}
                            iconSize={27}
                            skill={'Select Skills'}
                            selectedIconName={"ios-checkmark-circle-outline"}
                            unselectedIconName={"ios-radio-button-off"}
                            scrollViewHeight={AppConstants.getDeviceHeight(52)}
                            icon={0}
                            selected={this.state.dataSource}
                        />
                    </View>
                    <TouchableOpacity style={{ marginBottom: AppConstants.getDeviceHeight(10), borderBottomRightRadius: AppConstants.getDeviceWidth(2), borderBottomLeftRadius: AppConstants.getDeviceWidth(2), height: AppConstants.getDeviceHeight(6), width: AppConstants.getDeviceWidth(90), backgroundColor: AppConstants.COLORS.PROFILEINACTIVE, justifyContent: 'center', }}
                        onPress={() => {
                            array = [];
                            this.setState({ page: 0, Project: [], })
                            this.setState({ modal1: 0 })
                            this.WsAllProject(this.state.projecttext, this.state.dataSource[0]);
                        }}>
                        <Text style={{
                            fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
                            fontWeight: '600',
                            alignSelf: 'center',
                            color: AppConstants.COLORS.PROFILEBACKGROUND,
                        }}>
                            DONE
                </Text>
                    </TouchableOpacity>
                    {this.state.isLoading ? <View style={AppConstants.CommonStyles.spinner}>
                        <ActivityIndicator size='large' ></ActivityIndicator>
                    </View> : null}
                </Modal>
            </View>

        );
    }
}