import React, { Component } from 'react';
import { View, Text, ListView, TouchableOpacity, ActivityIndicator, Image, FlatList,Alert } from 'react-native';
import styles from './ChatmodalStyle';
import Modal from "react-native-modal";
import * as AppConstants from '../Helper/AppConstants';
import Toast, { DURATION } from 'react-native-easy-toast';
import Close from "react-native-vector-icons/MaterialCommunityIcons";

import { PostData } from "../WebServices/webAPIRequest";
import API from "../WebServices/API";
import CustomMultiPicker from "./multipleSelect";



export default class Chatmodal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            TextInputValueHolder: '',
            value: 0,
            open: false,
            dataSource: '',
            skilll: [],
            isLoading: false,
            selected: this.props.pass,
        }
        console.log('pass value',this.state.selected)
    }
    componentDidMount() {
        this.Getskill();
        this.setState({ open: true });
    }
    componentWillUnmount() {

        // this.openModal();
        // this.closeModal();
    }
    Getskill() {
        // if (global.isConnected) {
        this.setState({ isLoading: true });

        APIData = JSON.stringify({

        });
        PostData(API.GetSkill, APIData)
            .then((responseJson) => {
                this.setState({ isLoading: false, skilll: [] });
                var arr = [];
                responseJson.data.map((data, index) => {
                    arr.push(
                        data.skill,
                    )
                });
                this.setState({ skilll: arr });
            })
            .catch(() => {
                this.setState({ isLoading: false });
                this.refs.toast.show(AppConstants.Messages.APIERROR, DURATION.LENGTH_LONG);
            });
    }


    // modalDidOpen = () => { };
    // modalDidClose = () => {

    //     this.props.func(this.state.dataSource);
    //     this.setState({ open: false });
    // };
    // openModal = () => this.setState({ open: true });
    // closeModal = () => this.setState({ open: false });
    render() {
        const { navigate } = this.props.navigation;

        return (
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center", height: AppConstants.getDeviceHeight(100), }}>
                <Toast
                    ref="toast"
                    style={[AppConstants.CommonStyles.ToastStyle, { justifyContent: 'flex-end', height: AppConstants.getDeviceHeight(8), }]}
                    position="top"
                    positionValue={AppConstants.getDeviceHeight(0)}
                    fadeInDuration={750}
                    fadeOutDuration={1000}
                    textStyle={AppConstants.CommonStyles.ToastTextStyle}
                />
                <Modal

                    // backdrop={true}
                    // open={this.state.open}
                    // closeOnTouchOutside={true}
                    isVisible={this.state.open}
                    deviceHeight={AppConstants.getDeviceHeight(100)}
                    deviceWidth={AppConstants.getDeviceWidth(100)}
                    onModalHide={() => {
                      
                        console.log('enter')
                        
                      }}
                // modalDidOpen={this.modalDidOpen}
                // modalDidClose={this.modalDidClose}
                // modalStyle={styles.model}
                >

                    <View style={styles.mainContainer}>
                        <View style={{ height: AppConstants.getDeviceHeight(4), justifyContent: 'center', }}>
                            {/* <View style={styles.sentat}>
                                <Text style={styles.text}>Select Skills</Text>
                                <Close
                                    name='close'
                                    style={{
                                        position: 'absolute',
                                        alignSelf: 'flex-end',
                                    }}
                                    color={AppConstants.COLORS.HEDARBACKGROUND}
                                    onPress={() => {
                                        this.props.func1();
                                        this.setState({ open: false })
                                    }}
                                    size={AppConstants.FONTSIZE.FS22}
                                />
                            </View> */}
                        </View>


                        <CustomMultiPicker
                            options={this.state.skilll}
                            search={true} // should show search bar?
                            multiple={true} //
                            placeholder={"Search"}
                            placeholderTextColor={'#757575'}
                            returnValue={"label"} // label or value
                            callback={(res) => { this.setState({ dataSource: res }); }} // callback, array of selected items
                            rowBackgroundColor={"#eee"}
                            close={(call) => {
                                this.props.func1();
                                this.setState({ open: call })
                            }}
                            rowHeight={AppConstants.getDeviceHeight(7)}
                            rowRadius={5}
                            iconColor={AppConstants.COLORS.HEDARBACKGROUND}
                            iconSize={27}
                            skill={'Select Skills'}
                            selectedIconName={"ios-checkmark-circle-outline"}
                            unselectedIconName={"ios-radio-button-off"}
                            scrollViewHeight={AppConstants.getDeviceHeight(52)}
                            icon={0}
                            selected={this.state.selected}
                        />

                    </View>
                    <TouchableOpacity style={{ marginBottom: AppConstants.getDeviceHeight(10), borderBottomRightRadius: AppConstants.getDeviceWidth(2), borderBottomLeftRadius: AppConstants.getDeviceWidth(2), height: AppConstants.getDeviceHeight(6), width: AppConstants.getDeviceWidth(90), backgroundColor: AppConstants.COLORS.PROFILEINACTIVE, justifyContent: 'center', }}
                        onPress={() => {
                            var array = [...this.state.dataSource];                        
                            this.setState({ dataSource: array })
                            this.setState({ open: false })
                            this.props.func(this.state.dataSource);
                        }}>
                        <Text style={{
                            fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
                            fontWeight: '600',
                            alignSelf: 'center',
                            color: AppConstants.COLORS.PROFILEBACKGROUND,
                        }}>
                            DONE
                                    </Text>
                        {/* </LinearGradient> */}
                    </TouchableOpacity>



                </Modal>
                {this.state.isLoading1 ? <View style={AppConstants.CommonStyles.spinner}>
                    <ActivityIndicator size='large' ></ActivityIndicator>
                </View> : null}
            </View >
        );
    }

}
