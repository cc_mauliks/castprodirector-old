import React, {  Component } from 'react';
import {
  BackHandler,
  StyleSheet,
  View,
  Text,
  StatusBar,
  TouchableOpacity
} from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";
import * as AppConstants from '../Helper/AppConstants';
import Video, { ScrollView, Container } from '../../../components/Video'
const logo = 'https://your-url.com/logo.png'
const theme = {
  title: '#FFF',
  more: '#446984',
  center: '#7B8F99',
  fullscreen: '#446984',
  volume: '#A5957B',
  scrubberThumb: '#234458',
  scrubberBar: '#DBD5C7',
  seconds: '#DBD5C7',
  duration: '#DBD5C7',
  progress: '#446984',
  loading: '#DBD5C7'
}

export default class Videonew extends Component {
  constructor(props) {
    super(props);
  }
  
  componentDidMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    StatusBar.setHidden(true);
  }

  componentWillUnmount() {
    this.backHandler.remove()
  }

  handleBackPress = () => {
    this.props.navigation.goBack(); // works best when the goBack is async
    return true;
  }
  
  render() {
    return (
      <View style={styles.container}>
        <StatusBar hidden />
        {/* <ScrollView style={styles.videoContainer}> */}


        {/* <Container style={styles.videoContainer}> */}
        <Video
          style={{ height: '100%' }}
          // autoPlay={false}
          url={this.props.navigation.state.params.video}
          navigation1={this.props.navigation}
          logo={logo}
          placeholder={this.props.navigation.state.params.thumbnail}
          rotateToFullScreen
          theme={theme}
        />

        <TouchableOpacity onPress={() => { this.props.navigation.goBack() }} style={{ position: 'absolute', width: AppConstants.getDeviceHeight(5), height: AppConstants.getDeviceHeight(5), marginLeft: AppConstants.getDeviceHeight(3), top: 0 }}>
          <Icon name="close" color='#7B8F99' size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS22)} style={{ alignSelf: 'center' }} />
        </TouchableOpacity>
      </View >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  },

  videoContainer: {
    height: 50,
    margin: 10
  }

});

