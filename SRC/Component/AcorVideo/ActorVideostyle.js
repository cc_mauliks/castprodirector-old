import * as AppConstants from '../Helper/AppConstants';
export default {
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  mainContainer1: {
    flex: 1,
  },
  error: {
    fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
    fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
    color: AppConstants.COLORS.PROFILEBACKGROUND,
    marginTop: 0,
    textAlign: 'center',
    fontWeight: '500',
  },
  navigationimage: {
    height: AppConstants.getDeviceHeight(30),
    width: AppConstants.getDeviceWidth(100),
  },
  imagtebackground: {
    height: AppConstants.getDeviceHeight(30),
    width: AppConstants.getDeviceWidth(100),
    justifyContent: 'center'
  },
  name: {
    marginLeft: AppConstants.getDeviceWidth(5),
    fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS22),
    fontWeight: '600',
    fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
    alignSelf: 'center',
    color: AppConstants.COLORS.PROFILEINACTIVE,
    marginRight: AppConstants.getDeviceWidth(20),
  }
}