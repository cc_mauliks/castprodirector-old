import React, { Component } from 'react';
import {
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    ActivityIndicator,
    Alert,
    ImageBackground,
    BackHandler,
    Platform,
} from 'react-native';
import styles from './ActorVideostyle'
import * as AppConstants from '../Helper/AppConstants';
import OfflineNotice from '../Helper/OfflineNotice';
import { PostData, internet } from '../WebServices/webAPIRequest';
import API from '../WebServices/API';
import RNFetchBlob from 'rn-fetch-blob'
import Icon from "react-native-vector-icons/MaterialIcons";

export default class ActorVideo extends Component {
    videoPlayer;
    state = {
        rate: 1,
        volume: 1,
        muted: false,
        resizeMode: 'contain',
        duration: 0.0,
        currentTime: 0.0,
        controls: false,
        paused: true,
        skin: 'custom',
        ignoreSilentSwitch: null,
        isBuffering: false,
        video_thumbnail: '',
        uniqueId: ''
    };
    constructor(props) {
        super(props);
        this.onLoad = this.onLoad.bind(this);
        this.onProgress = this.onProgress.bind(this);
        this.onBuffer = this.onBuffer.bind(this);
        this.state = {
            video: '',
            isLoading: false,
            isLoading1: false,
            opacity: 0,
            name: '',
            videofilename: '',
            video: '',
            ModalVisibleStatus: false,
            thumbnail: '',
            isFullScreen: false,
            isLoading: true,
            paused: false,
        };
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.lists = [
            this.props.navigation.addListener('didFocus', () => { this.componentDidFocus() })
        ];
    }
    componentDidFocus() {
        this.ProfielGet();
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    handleBackButtonClick() {
        this.props.navigation.navigate(AppConstants.SCREENS.ACTORPROFILE);
        return true;
    }

    ProfielGet() {
        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({
                actor_id: this.props.navigation.state.params.artistid,
            });
            PostData(API.ViewartistProfile, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false });
                    if (responseJson.statusCode == 1) {
                        this.setState({
                            video: responseJson.actor_video,
                            thumbnail: responseJson.video_thumbnail,
                            name: responseJson.name
                        });
                    }
                    else {
                        Alert.alert(responseJson.message);
                    }
                })
                .catch(() => {
                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }
    }
    Downloadfile(url) {
        const Path = RNFetchBlob.fs.dirs.DocumentDir + '/' + this.state.videofilename + '.mp4';
        RNFetchBlob
            .config({
                addAndroidDownloads: {
                    title: 'Topup Report',
                    useDownloadManager: true,
                    mediaScannable: true,
                    notification: true,
                    description: 'File downloaded by download manager.',
                    path: Path
                },
            })
            .fetch('GET', url)
            .then((res) => {
                this.setState({ video: res.path(), isLoading: false })
            }).catch(() => {
            });
    }
    onLoad(data) {
        this.setState({ duration: data.duration });
    }

    onProgress(data) {
        this.setState({ currentTime: data.currentTime });
    }

    onBuffer({ isBuffering }) {

        this.setState({ isBuffering: isBuffering ? 1 : 0 });
    }

    ShowModalFunction(visible, url) {
        this.setState({
            ModalVisibleStatus: visible,
        });
    }
    getCurrentTimePercentage() {
        if (this.state.currentTime > 0) {
            return parseFloat(this.state.currentTime) / parseFloat(this.state.duration);
        } else {
            return 0;
        }
    }
    render() {
        if (this.state.isLoading) {
            return AppConstants.ShowActivityIndicator();
        }
        return (
            <View style={styles.mainContainer}>
                <StatusBar barStyle="light-content" backgroundCopr={AppConstants.SHADOWCOLORS.INTRO} animated translucent></StatusBar>
                <OfflineNotice />
                <View style={styles.mainContainer1}>
                    <View style={{ width: AppConstants.getDeviceWidth(100), height: AppConstants.getDeviceHeight(8), flexDirection: 'row' }}>
                        <Text style={styles.name}>{this.state.name}</Text>
                    </View>
                    <View style={styles.mainContainer1}>
                        {this.state.thumbnail == '' ? <View style={styles.mainContainer}>
                            {this.state.isLoading1 ? null : <Text style={styles.error}>There is no video added yet.</Text>}
                        </View> : <TouchableOpacity onPress={() => { this.props.navigation.navigate('Videonew', { video: this.state.video, thumbnail: this.state.thumbnail }) }} style={styles.navigationimage}>
                                <ImageBackground source={{ uri: this.state.thumbnail }} style={styles.imagtebackground}>
                                    <Icon name="play-circle-outline" color='#7B8F99' size={AppConstants.moderateScale(60)} style={{ alignSelf: 'center' }} />
                                </ImageBackground>
                            </TouchableOpacity>
                        }
                        <ActivityIndicator
                            animating
                            size="large"
                            color={'red'}
                            style={[styles.activityIndicator, { opacity: this.state.opacity }]}
                        />
                    </View>
                </View>
                {this.state.isLoading1 ? <View style={AppConstants.CommonStyles.spinner}>
                    <ActivityIndicator size='large' ></ActivityIndicator>
                </View> : null}
            </View>
        );
    }

}