import * as AppConstants from '../Helper/AppConstants';
export default {
    mainContainer: {
        flex: 1,
        marginBottom: AppConstants.getDeviceHeight(4),
        backgroundColor: AppConstants.COLORS.VIEWBACKGROUND
    },
    ongoingtitle: {
        color: AppConstants.COLORS.PROFILEINACTIVE,
        marginLeft: AppConstants.getDeviceWidth(5),
        marginTop: AppConstants.getDeviceHeight(1),
        alignSelf: 'center',
        fontWeight: 'bold'
    },
    line: {
        height: AppConstants.getDeviceHeight(0.2),
        backgroundColor: AppConstants.COLORS.HEDARBACKGROUND,
        width: AppConstants.getDeviceWidth(100),
        marginTop: AppConstants.getDeviceHeight(1)
    },
    FlatlistContainer: {

        // height: AppConstants.getDeviceHeight(10),
        // width: AppConstants.getDeviceWidth(100),
        marginTop: AppConstants.getDeviceHeight(0.5),
        // backgroundColor: AppConstants.COLORS.BACKHOME,
        shadowOffset: { width: 0, height: 2 },
        // shadowColor: AppConstants.SHADOWCOLORS.TEXT_SHADOW,
        // shadowOpacity: 0.8,
        // shadowRadius: 2,
        // elevation: 1,

    },
    FlatlistContainer1: {
        flexDirection: 'row',
        // height: AppConstants.getDeviceHeight(38),
        flex: 1,
        width: AppConstants.getDeviceWidth(96),
        marginTop: AppConstants.getDeviceHeight(0.2),
        backgroundColor: AppConstants.COLORS.WHITE,
        shadowOffset: { width: 0, height: 2 },
        shadowColor: AppConstants.SHADOWCOLORS.TEXT_SHADOW,
        shadowOpacity: 0.8,
        shadowRadius: 3,
        elevation: 3,
        borderRadius: 3,
        marginLeft: AppConstants.getDeviceWidth(2),
        marginRight: AppConstants.getDeviceWidth(2),
        marginBottom: AppConstants.getDeviceWidth(2),
        marginTop: AppConstants.getDeviceWidth(2),
    },
    projecttitle: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        marginTop: AppConstants.getDeviceWidth(1),
        // fontWeight: 'bold',
        // marginLeft: AppConstants.getDeviceWidth(5)
    },
    projecttitle1: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.FORGOTTEXT,
        marginTop: AppConstants.getDeviceWidth(1),
        // fontWeight: 'bold',
        // marginLeft: AppConstants.getDeviceWidth(5)
    },
    image: {
        backgroundColor: AppConstants.COLORS.PROFILEBACKGROUND,
        height: AppConstants.getDeviceHeight(25),
        width: AppConstants.getDeviceWidth(96),
       
        marginTop: AppConstants.getDeviceWidth(1),
    },
    projectrole: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND, fontWeight: 'bold',
        marginLeft: AppConstants.getDeviceWidth(5)
    },
    projectrole1: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,

    },
    projectdesc: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS10),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        marginLeft: AppConstants.getDeviceWidth(5)
    },
    projectdate: {
        fontSize: AppConstants.moderateScale(9.5),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.datecolor,
        fontWeight: 'bold',
        marginLeft: AppConstants.getDeviceWidth(5),
        marginRight: AppConstants.getDeviceWidth(0.5)
    },
    viewerorr: {
        flex: 1,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        justifyContent: 'center',
        // marginTop: AppConstants.getDeviceHeight(6.6),
        marginBottom: AppConstants.getDeviceHeight(8)
    },
    error: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        marginTop: 0,
        textAlign: 'center',
        fontWeight: '500',
    },
    hedarstyel: {
        paddingTop: 2,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 2,
        fontSize: 22,

        color: "#fff",
        width: AppConstants.getDeviceWidth(100),
        height: AppConstants.getDeviceHeight(5),
        backgroundColor: AppConstants.COLORS.PROFILEINACTIVE, justifyContent: 'center'
    },
    hedaertext:{
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        alignSelf:'center',
        fontWeight: 'bold',
    }
}