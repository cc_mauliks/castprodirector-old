import React, { Component } from 'react';
import {
    View,
    StatusBar,
    Text,
    ScrollView,
    FlatList,
    SectionList,
    TouchableOpacity,
    Image,
    ActivityIndicator
} from 'react-native';
import styles from './DashboardStyle'
import * as AppConstants from '../Helper/AppConstants';
import OfflineNotice from '../Helper/OfflineNotice';
import Toast, { DURATION } from 'react-native-easy-toast'
import API from "../WebServices/API";
import { PostData, internet } from '../WebServices/webAPIRequest';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Icons from "react-native-vector-icons/FontAwesome";
import Iconss from "react-native-vector-icons/Entypo";
import SplashScreen from 'react-native-splash-screen'

const Images = {
    noimage: 'noimage'
}
const Separator = () =>
    <View style={{ height: 10, backgroundColor: 'yellow' }} />

export default class Dashboard extends Component {
    constructor(props) {
        super(props);
        // ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            isLoading: false,
            CLoseProject: [],
            OngoingProject: [],
            rolecount: '',
            AudtionCount: '',
            Allarray: []
        };
        this.renderItem = this.renderItem.bind(this)
        // this._renderRow = this._renderRow.bind(this);
        this.lists = [
            this.props.navigation.addListener('didFocus', () => { this.componentDidFocus() })
        ];
        SplashScreen.hide();
    }
    componentDidFocus() {
        this.WsAllProject();

    }
    WsAllProject() {

        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({

            });
            var array = [];
            PostData(API.AllProject, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false });
                    if (responseJson.statusCode == 1) {
                        this.setState({
                            OngoingProject: responseJson.ongoing_projects.data,
                            CLoseProject: responseJson.closed_projects.data,
                        });
                        if (responseJson.ongoing_projects.data == '') {
                            this.setState({ Allarray: [] });
                        }
                        else if (responseJson.closed_projects.data == '') {
                            array.push({ title: 'Ongoing Projects', data: this.state.OngoingProject })
                            this.setState({ Allarray: array });
                        } else {
                            array.push({ title: 'Ongoing Projects', data: this.state.OngoingProject }, { title: 'Closed Projects', data: this.state.CLoseProject })
                            this.setState({ Allarray: array });
                        }
                    }
                    else {
                        this.setState({
                            OngoingProject: [],
                            CLoseProject: [],
                        });
                    }
                }).catch(() => {
                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }

    }
    renderItem(item, index) {
        return (
            <View style={styles.FlatlistContainer1}>
                <TouchableOpacity
                    onPress={() => { this.props.navigation.navigate(AppConstants.SCREENS.PROJECTDETAIL, { projectid: item.item.project_id }) }}>
                    <View style={styles.FlatlistContainer}>
                        <View style={{ flexDirection: 'row' }}>
                            <Icon name="message-video" color={AppConstants.COLORS.PROFILEBACKGROUND} style={{ alignSelf: 'center', marginLeft: AppConstants.getDeviceWidth(2), marginTop: AppConstants.getDeviceHeight(1) }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS18)} />
                            <Text style={[styles.projecttitle, { marginLeft: AppConstants.getDeviceWidth(1), fontWeight: 'bold' }]}>Project Name: </Text>
                            <Text style={[styles.projecttitle]}>{item.item.title}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Icons name="home" color={AppConstants.COLORS.PROFILEBACKGROUND} style={{ alignSelf: 'center', marginLeft: AppConstants.getDeviceWidth(2), marginTop: AppConstants.getDeviceHeight(0.5) }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS18)} />
                            <Text style={[styles.projecttitle, { marginLeft: AppConstants.getDeviceWidth(1), fontWeight: 'bold' }]}>Production House: </Text>
                            <Text style={[styles.projecttitle]}>{item.item.company}</Text>
                        </View>
                        <View style={styles.image}>
                            <Image source={{ uri: item.item.logo == '' ? Images.noimage : item.item.logo }} style={{ flex: 1 ,resizeMode:'contain'}}>
                            </Image>
                        </View>
                        <View style={{ flexDirection: 'row',marginBottom:AppConstants.getDeviceHeight(0.5),}}>
                            <Icons name="users" color={AppConstants.COLORS.FORGOTTEXT} style={{ alignSelf: 'center', marginLeft: AppConstants.getDeviceWidth(2), marginTop: AppConstants.getDeviceHeight(0.5) }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS15)} />
                            <Text style={[styles.projecttitle1, { marginLeft: AppConstants.getDeviceWidth(2), fontWeight: 'bold' }]}>Role: </Text>
                            <Text style={[styles.projecttitle1]}>{item.item.role_count}</Text>
                            <View style={{ position: 'absolute', right: 0, flexDirection: 'row' }}>
                                <Iconss name="folder-video" color={AppConstants.COLORS.FORGOTTEXT} style={{ alignSelf: 'center', marginLeft: AppConstants.getDeviceWidth(2), marginTop: AppConstants.getDeviceHeight(0.5) }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS18)} />
                                <Text style={[styles.projecttitle1, { marginLeft: AppConstants.getDeviceWidth(1), fontWeight: 'bold' }]}>Audition: </Text>
                                <Text style={[styles.projecttitle1, { marginRight: AppConstants.getDeviceWidth(2), }]}>{item.item.audition_count}</Text>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }


    renderHeader({ section }) {
        return (
            <View collapsable={false} style={styles.hedarstyel}>
                <Text style={styles.hedaertext}>{section.title}</Text>
            </View>
        )
    }


    render() {
        return (
            <View style={styles.mainContainer}>
                <StatusBar barStyle="light-content" backgroundCopr={AppConstants.SHADOWCOLORS.INTRO} animated translucent></StatusBar>
                <OfflineNotice />
                <View style={styles.mainContainer}>
                    <Toast
                        ref="toast"
                        style={[AppConstants.CommonStyles.ToastStyle1, { justifyContent: 'flex-end', height: AppConstants.getDeviceHeight(11), }]}
                        position="top"
                        positionValue={AppConstants.getDeviceHeight(0)}
                        fadeInDuration={750}
                        fadeOutDuration={1000}
                        textStyle={AppConstants.CommonStyles.ToastTextStyle}
                    />
                  
                    {this.state.Allarray == '' ? null : <SectionList
                        sections={this.state.Allarray}
                        renderSectionHeader={this.renderHeader}
                        renderItem={this.renderItem.bind(this)}
                        stickySectionHeadersEnabled={true}
                        maxToRenderPerBatch={3}
                        initialNumToRender={3}

                    />}

                </View>
                {this.state.CLoseProject == '' && this.state.OngoingProject == '' && this.state.isLoading == false ?
                    <View style={styles.viewerorr}>
                        <Text style={styles.error}>There are no projects available.</Text>
                    </View> : null}
                {this.state.isLoading ? <View style={AppConstants.CommonStyles.spinner}>
                    <ActivityIndicator size='large' ></ActivityIndicator>
                </View> : null}
            </View>
        );
    }
}