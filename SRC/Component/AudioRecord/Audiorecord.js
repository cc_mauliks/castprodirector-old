import React, { Component } from 'react';

import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Platform,
    TouchableOpacity,
    ActivityIndicator,
    Alert
} from 'react-native';
import { RNS3 } from 'react-native-s3-upload';
import RNFetchBlob from 'rn-fetch-blob'

import Sound from 'react-native-sound';
import { AudioRecorder, AudioUtils } from 'react-native-audio';
import * as AppConstants from '../Helper/AppConstants';
import Icons from "react-native-vector-icons/Foundation";
import Permissions from "react-native-permissions";

export default class Audiorecord extends Component {

    state = {
        currentTime: 0.0,
        recording: false,
        paused: false,
        stoppedRecording: false,
        finished: false,
        audioPath: AudioUtils.DocumentDirectoryPath + '/test3.aac',
        hasPermission: undefined,
        filesize: '',
        filepathadd: ''

    };

    prepareRecordingPath(audioPath) {
        AudioRecorder.prepareRecordingAtPath(audioPath, {
            SampleRate: 22050,
            Channels: 1,
            AudioQuality: "Low",
            AudioEncoding: "aac",
            AudioEncodingBitRate: 32000
        });
    }

    componentDidMount() {
        AudioRecorder.requestAuthorization().then((isAuthorised) => {

            this.setState({ hasPermission: isAuthorised });

            if (!isAuthorised) return;
            this.prepareRecordingPath(this.state.audioPath);

            AudioRecorder.onProgress = (data) => {
                this.setState({ currentTime: Math.floor(data.currentTime) });
            };

            AudioRecorder.onFinished = ((data) => {
            
                // Android callback comes in the form of a promise instead.
                if (Platform.OS === 'ios') {
                    this._finishRecording(data.status === "OK", data.audioFileURL, data.audioFileSize);
                }
            }).catch((error)=>{
            });
        });
    }

    _renderButton(title, onPress, active) {
        var style = (active) ? styles.activeButtonText : styles.buttonText;

        return (
            <TouchableHighlight style={styles.button} onPress={onPress}>
                <Text style={style}>
                    {title}
                </Text>
            </TouchableHighlight>
        );
    }

    _renderPauseButton(onPress, active) {
        var style = (active) ? styles.activeButtonText : styles.buttonText;
        var title = this.state.paused ? "RESUME" : "PAUSE";
        return (
            <TouchableHighlight style={styles.button} onPress={onPress}>
                <Text style={style}>
                    {title}
                </Text>
            </TouchableHighlight>
        );
    }

    async _pause() {
        if (!this.state.recording) {
            return;
        }

        try {
            const filePath = await AudioRecorder.pauseRecording();
            this.setState({ paused: true });
        } catch (error) {
        }
    }

    async _resume() {
        if (!this.state.paused) {
            return;
        }

        try {
            await AudioRecorder.resumeRecording();
            this.setState({ paused: false });
        } catch (error) {
        }
    }

    async _stop() {
        if (!this.state.recording) {
            return;
        }

        this.setState({ stoppedRecording: true, recording: false, paused: false });

        try {
            const filePath = await AudioRecorder.stopRecording();

            if (Platform.OS === 'android') {
                this._finishRecording(true, filePath);
            }
            return filePath;
        } catch (error) {
        }
    }

    async _play() {


        if (this.state.recording) {
            await this._stop();
        }
        if (this.state.audioPath == '') {
            Alert.alert("No Audio Found. Please record Audio to play");
        }
        else {
            this.props.navigation.navigate(AppConstants.SCREENS.AUDIOPLYER, { audio: this.state.audioPath })
        }

        // These timeouts are a hacky workaround for some issues with react-native-sound.
        // See https://github.com/zmxv/react-native-sound/issues/89.
        //         Sound.setCategory('Playback');

        //         setTimeout(() => {
        //             var sound = new Sound(this.state.audioPath, Sound.MAIN_BUNDLE, (error) => {
        //                 if (error) {
        //                 }
        //             });
        //             setTimeout(() => {
        //                 sound.play((success) => {
        //                     if (success) {
        //                     } else {
        //                         Alert.alert(
        //                             "No Audio Found. Please record Audio to play"
        //                         );
        //                     }
        //                 });
        //             }, 100);
        //         }, 100);
    }

    async _record() {
        if (this.state.recording) {
            return;
        }
       


        if (this.state.stoppedRecording) {
            this.prepareRecordingPath(this.state.audioPath);
        }

        this.setState({ recording: true, paused: false });

        try {
            const filePath = await AudioRecorder.startRecording();
        } catch (error) {
        }
    }

    _finishRecording(didSucceed, filePath, fileSize) {

        this.setState({ finished: didSucceed });
        var randomNumber = Math.floor(Math.random() * 100) + 1;

        this.setState({
            filesize: fileSize + randomNumber,
            filepathadd: filePath
        })

    }
    submittmethod() {
        this.setState({ isLoading: true });
        const file = {
            uri: Platform.OS == 'ios' ? this.state.filepathadd : 'file:///' + this.state.filepathadd,
            name: this.state.filesize,
            type: "audio/mpeg"
        }
        const options = {
            keyPrefix: "uploads/",
            bucket: "castpro",
            region: "us-east-2",
            accessKey: "AKIAVSA77EO2WHI4DT7L",
            secretKey: "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",
            successActionStatus: 201,

        }

        RNS3.put(file, options).then(response => {
            this.setState({ isLoading: false });
            if (response.status !== 201) {

                Alert.alert("Failed to upload image to S3");
            }
            else {

                global.recording = response.body.postResponse.location;
                global.recordingname = response.body.postResponse.etag;
                this.props.navigation.goBack();
                this.setState({ isLoading: false });

            }
        });
    }

    render() {

        return (
            <View style={styles.container}>
                <Icons name="sound" color={AppConstants.COLORS.WHITE} style={{ alignSelf: 'center', marginTop: AppConstants.getDeviceHeight(2) }} size={AppConstants.moderateScale(150)} />

                <View style={{ flexDirection: 'row' }}>
                    <Icons name="video" color={AppConstants.COLORS.PROFILEBACKGROUND} style={{ alignSelf: 'center', marginTop: AppConstants.getDeviceHeight(2) }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS20)} />

                </View>
                <View style={styles.controls}>
                    {this._renderButton("RECORD", () => {
                        if (!this.state.hasPermission) {
                            Permissions.check("microphone").then(response => {
                                if (response == "denied") {
                                    Permissions.request("microphone").then(response => {
                                        if (response == "denied") {
                                            Alert.alert(
                                                "Please allow Permissions to access Microphone from Settings -> Apps -> Castpro -> Microphone"
                                            );
                                        } else if (response == 'authorized') {
                                            this.prepareRecordingPath(this.state.audioPath);

                                            AudioRecorder.onProgress = (data) => {
                                                this.setState({ currentTime: Math.floor(data.currentTime) });
                                            };

                                            AudioRecorder.onFinished = (data) => {
                                                // Android callback comes in the form of a promise instead.
                                                if (Platform.OS === 'ios') {
                                                    this._finishRecording(data.status === "OK", data.audioFileURL, data.audioFileSize);

                                                }

                                            };

                                        }

                                    });
                                }
                                else {
                                    this.setState({ hasPermission: true });
                                    this._record()
                                }
                                if (response == "undetermined") {
                                    Permissions.request("microphone").then(response => {
                                        if (response == "denied") {
                                            Alert.alert(
                                                "Please allow Permissions to access Microphone from Settings -> Apps -> Castpro -> Microphone"
                                            );
                                        }
                                        else if (response == 'authorized') {
                                            this.prepareRecordingPath(this.state.audioPath);

                                            AudioRecorder.onProgress = (data) => {
                                                this.setState({ currentTime: Math.floor(data.currentTime) });
                                            };

                                            AudioRecorder.onFinished = (data) => {
                                                // Android callback comes in the form of a promise instead.
                                                if (Platform.OS === 'ios') {
                                                    this._finishRecording(data.status === "OK", data.audioFileURL, data.audioFileSize);
                                                }

                                            };

                                        }

                                    });
                                }
                                else {
                                    if (response == "denied") {
                                    }
                                    else {

                                        this.setState({ hasPermission: true });
                                        this._record()
                                    }
                                }
                            });

                            return;
                        }
                        else {
                           
                            this.prepareRecordingPath(this.state.audioPath);

                            AudioRecorder.onProgress = (data) => {
                                this.setState({ currentTime: Math.floor(data.currentTime) });
                            };

                            AudioRecorder.onFinished = (data) => {
                                // Android callback comes in the form of a promise instead.
                                if (Platform.OS === 'ios') {
                                    this._finishRecording(data.status === "OK", data.audioFileURL, data.audioFileSize);
                                }

                            };

                            this.setState({ hasPermission: true });
                            this._record()
                        }
                    }, this.state.recording)}
                    {this._renderButton("PLAY", () => { this._play() })}
                    {this._renderButton("STOP", () => { this._stop() })}
                    {/* {this._renderButton("PAUSE", () => {this._pause()} )} */}
                    {this._renderPauseButton(() => { this.state.paused ? this._resume() : this._pause() })}
                    <Text style={styles.progressText}>{this.state.currentTime}s</Text>

                    <TouchableOpacity onPress={() => {

                        this.submittmethod();

                    }} style={{
                        width: AppConstants.getDeviceWidth(25),
                        height: AppConstants.getDeviceHeight(5),
                        backgroundColor: AppConstants.COLORS.PROFILEINACTIVE,
                        marginLeft: AppConstants.getDeviceWidth(5),
                        justifyContent: 'center',
                        marginTop: AppConstants.getDeviceHeight(1.5),
                        marginBottom: AppConstants.getDeviceHeight(2)
                    }}>
                        <Text style={{
                            fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
                            fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
                            color: AppConstants.COLORS.PROFILEBACKGROUND,
                            alignSelf: 'center',
                            fontWeight: 'bold'
                        }}>Upload </Text>
                    </TouchableOpacity>
                </View>
                {this.state.isLoading ? <View style={AppConstants.CommonStyles.spinner}>
                    <ActivityIndicator size='large' ></ActivityIndicator>
                </View> : null}
            </View>
        );
    }
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: AppConstants.COLORS.PROFILEBACKGROUND,
    },
    controls: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    progressText: {
        paddingTop: 30,
        fontSize: 50,
        color: "#fff"
    },
    button: {
        padding: 10
    },
    disabledButtonText: {
        color: '#eee'
    },
    buttonText: {
        fontSize: 20,
        color: "#fff"
    },
    activeButtonText: {
        fontSize: 20,
        color: "#B81F00"
    }

});
