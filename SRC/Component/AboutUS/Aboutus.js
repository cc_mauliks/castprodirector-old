import React, { Component } from 'react';
import {
    ScrollView,
    ActivityIndicator,
    View,
} from 'react-native';
import styles from './AboutusStyle'
import HTML from 'react-native-render-html';
import * as AppConstants from '../Helper/AppConstants';
import API from "../WebServices/API";
import { PostData, internet } from '../WebServices/webAPIRequest';
export default class Aboutus extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            terms:''
        }
        this.lists = [
            this.props.navigation.addListener('didFocus', () => { this.componentDidFocus() })
        ];
    }
    componentDidFocus() {
        this.AboutUs();
    }
    AboutUs() {
        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({
            });
            PostData(API.Aboutus, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false, terms: responseJson.data.Director.about_text });
                })
                .catch(() => {
                    this.setState({ isLoading: false });
                });
        }
    }
    render() {
        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{ flex: 1 }}>
                    {this.state.terms==''?null:<HTML html={this.state.terms} baseFontStyle={{ color: AppConstants.COLORS.BLACK, }} 
                    containerStyle={{ paddingLeft: AppConstants.getDeviceWidth(2), paddingRight: AppConstants.getDeviceWidth(2) }} />}
                </ScrollView>
                {this.state.isLoading ? <View style={AppConstants.CommonStyles.spinner}>
                    <ActivityIndicator size='large' ></ActivityIndicator>
                </View> : null}
            </View>
        )
    }

}