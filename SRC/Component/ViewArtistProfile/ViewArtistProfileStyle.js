import * as AppConstants from '../Helper/AppConstants';
export default {
    mainContainer: {
        flex: 1,
    },
    profile: {
        flex: 1, resizeMode: 'contain'
    },
    Profilenamedetail: {
        width: AppConstants.getDeviceWidth(100),
        flexDirection: 'row',
        height: AppConstants.getDeviceHeight(6),
        alignItems: 'center',
    },
    Profiledetail: {
        width: AppConstants.getDeviceWidth(100),
        flexDirection: 'row',
        alignItems: 'center',
    },
    profieldetailtextfix: {
        color: AppConstants.COLORS.BLACK,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        marginLeft: AppConstants.getDeviceWidth(3),
        paddingTop: AppConstants.getDeviceHeight(1),
        fontWeight: 'bold',
    },
    introvideo: {
        color: AppConstants.COLORS.BLACK,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        marginLeft: AppConstants.getDeviceWidth(4),
        alignSelf: 'center'
    },
   
    profieldetailtext: {
        color: AppConstants.COLORS.BLACK,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),

        paddingTop: AppConstants.getDeviceHeight(1),
    },
    profilenametetxt: {
        color: AppConstants.COLORS.PROFILEINACTIVE,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS18),
        fontWeight: 'bold',
        marginLeft: AppConstants.getDeviceWidth(3),
    },
    profieldetailtext1: {
        width: AppConstants.getDeviceWidth(80),
        color: AppConstants.COLORS.BLACK,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),

        paddingTop: AppConstants.getDeviceHeight(1),
        paddingRight: AppConstants.getDeviceWidth(10),
    },
    button: {
        width: AppConstants.getDeviceWidth(100),
        marginTop: AppConstants.getDeviceHeight(5),
        height: AppConstants.getDeviceHeight(3),
        flex: 1,
        justifyContent: "center",
        marginBottom: AppConstants.getDeviceHeight(2)
    },
    Signup: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontWeight: 'bold',
        alignSelf: 'center',
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
    },
    requestbutton: {
        backgroundColor: AppConstants.COLORS.PROFILEINACTIVE,
        height: AppConstants.getDeviceHeight(5),
        marginBottom: AppConstants.getDeviceHeight(2),
        alignItems: 'center',
        justifyContent: 'center',
        width: AppConstants.getDeviceWidth(55),
        alignSelf: 'center',

    },
    requestbutton1: {
        backgroundColor: AppConstants.COLORS.PROFILEINACTIVE,
        height: AppConstants.getDeviceHeight(5),
        marginBottom: AppConstants.getDeviceHeight(2),
        alignItems: 'center',
        justifyContent: 'center',
        width: AppConstants.getDeviceWidth(35),
        alignSelf: 'center'
    },
    dropdowncontainer: {
        width: AppConstants.getDeviceWidth(86),
        height: AppConstants.getDeviceHeight(4),
        marginLeft: AppConstants.getDeviceWidth(2.5),
        backgroundColor: AppConstants.COLORS.PROFILETEXINPUT,
        marginTop: AppConstants.getDeviceHeight(1),

    },
    profilepikview: {
        width: AppConstants.getDeviceWidth(100),
        height: AppConstants.getDeviceHeight(40),
        backgroundColor: AppConstants.COLORS.PROFILEBACKGROUND,
        marginTop: AppConstants.getDeviceHeight(0.4),
    },
    intoricon: {
        alignItems: 'center',
        position: 'absolute',
        marginLeft: AppConstants.getDeviceWidth(3)
    },
    intorview: {
        alignSelf: 'center',
        width: AppConstants.getDeviceWidth(88),
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: AppConstants.getDeviceHeight(2),
    },
    introtouch: { justifyContent: 'center', width: AppConstants.getDeviceWidth(35), height: AppConstants.getDeviceHeight(4), backgroundColor: AppConstants.COLORS.PROFILEINACTIVE, marginLeft: AppConstants.getDeviceWidth(5) },
    introtouch1: { justifyContent: 'center', width: AppConstants.getDeviceWidth(35), height: AppConstants.getDeviceHeight(4), backgroundColor: AppConstants.COLORS.PROFILEINACTIVE, marginRight: AppConstants.getDeviceWidth(5) },
    dropdownicon: { position: 'absolute', right: 0, marginRight: AppConstants.getDeviceWidth(2), paddingTop: AppConstants.getDeviceHeight(4) },
    viewaccept: { width: AppConstants.getDeviceWidth(100), height: AppConstants.getDeviceHeight(0.1), backgroundColor: AppConstants.COLORS.PROFILEBACKGROUND, marginTop: AppConstants.getDeviceHeight(1) }
}