import React, { Component } from 'react';
import {
    Image,
    Keyboard,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    ActivityIndicator,
    Alert
} from 'react-native';
import styles from './ViewArtistProfileStyle'
import * as AppConstants from '../Helper/AppConstants';
import OfflineNotice from '../Helper/OfflineNotice';
import Toast, { DURATION } from 'react-native-easy-toast';
import API from "../WebServices/API";
import { PostData, internet } from '../WebServices/webAPIRequest';
import Icons from "react-native-vector-icons/FontAwesome";
import { Dropdown } from 'react-native-material-dropdown';
import Iconss from "react-native-vector-icons/AntDesign";
const Images = {
    noimage: 'noimage'
}
export default class ViewArtistProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Search: '',
            thumbnail: '',
            Age: '',
            Motherlanguage: '',
            Otheranguage: '',
            Skill: '',
            Profilepic: '',
            Profilerequestbutton: '',
            ProfileAuditionbutton: '',
            Profilerequeststatus: '',
            Phonenumber: '',
            Emailid: '',
            Actorid: this.props.navigation.state.params.artistid,
            Height: '',
            OngoingProject: [],
            Projectname: '',
            Rolename: '',
            Projectname1: '',
            Projectname11: false,
            Rolename1: '',
            Rolename11: false,
            array: [],
            arrayrole: [],
            Projectid: '',
            rolearryid: [],
            SelectRoleid: '',
            name: '',
            bodytype: '',

        }
        this.lists = [
            this.props.navigation.addListener('didFocus', () => { this.componentDidFocus() })
        ];
    }
    componentDidFocus() {
        this.ViewArtistProfiel();
        this.WsAllProject();
    }
    validator = () => {
        if (this.state.Projectid == '') {
            this.setState({ Projectname11: true })
        }
        else if (this.state.Rolename == '') {
            this.setState({ Rolename11: true })
        }
        else {
            this.SendAuditionrequest();
        }
    }
    WsAllProject() {
        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({
            });
            PostData(API.AllProject, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false });
                    if (responseJson.statusCode == 1) {
                        var arr = [];
                        responseJson.ongoing_projects.data.map((data) => {
                            arr.push({
                                value: data.title
                            });
                        })
                        this.setState({ OngoingProject: arr, array: responseJson.ongoing_projects.data, arrayrole: [], rolearryid: [] })
                    }
                    else {
                    }
                }).catch(() => {
                    this.setState({ isLoading: false });
                    this.refs.toast.show(AppConstants.Messages.APIERROR, DURATION.LENGTH_LONG);
                });
        }
    }
    wsRole(valeu) {

        if (global.isConnected) {
            this.setState({ isLoading: true, arrayrole: [], rolearryid: [], Rolename: '' });
            APIData = JSON.stringify({
                project_id: valeu
            });
            PostData(API.GetRole, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false });
                    if (responseJson.statusCode == 1) {
                        var arr = [];
                        responseJson.rolesData.map((data) => {
                            arr.push({
                                value: data.character_name
                            });
                        })
                        this.setState({ arrayrole: arr, rolearryid: responseJson.rolesData })
                    }
                    else {
                    }
                }).catch(() => {
                    this.setState({ isLoading: false });
                    this.refs.toast.show(AppConstants.Messages.APIERROR, DURATION.LENGTH_LONG);
                });
        }
    }
    ViewArtistProfiel() {
        Keyboard.dismiss();
        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({
                actor_id: this.props.navigation.state.params.artistid,
                audition_request_id: this.props.navigation.state.params.auditoinrequestid,
            });
            PostData(API.ViewartistProfile, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false });
                    if (responseJson.statusCode == 1) {
                        this.setState({
                            Age: responseJson.age,
                            Motherlanguage: responseJson.first_language,
                            Otheranguage: responseJson.other_language,
                            Skill: responseJson.skills,
                            Profilepic: responseJson.profile_image,
                            Profilerequestbutton: responseJson.show_profile_request_btn,
                            ProfileAuditionbutton: responseJson.show_audition_request_btn,
                            Profilerequeststatus: responseJson.profile_request_status,
                            Phonenumber: responseJson.phone_no,
                            Emailid: responseJson.email_address,
                            Actorid: responseJson.actor_id,
                            Height: responseJson.height,
                            name: responseJson.name,
                            bodytype: responseJson.body_type,
                            Projectname1: responseJson.project_name,
                            Rolename1: responseJson.role_name,
                        });
                    }
                    else {
                        this.setState({ isLoading: false });
                        Alert.alert(responseJson.message);
                    }
                }).catch(() => {
                    this.setState({ isLoading: false });
                });
        }
    }
    SendProfileRequest() {
        Keyboard.dismiss();
        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({
                actor_id: this.state.Actorid,
                status: 'Pending'
            });
            PostData(API.RequestForProfile, APIData)
                .then((responseJson) => {
                    if (responseJson.statusCode == 1) {
                        this.refs.toast1.show(responseJson.message)
                        this.ViewArtistProfiel();
                    }
                    else {
                        this.setState({ isLoading: false });
                        Alert.alert(responseJson.message);
                    }
                }).catch(() => {
                    this.setState({ isLoading: false });
                });
        }
    }
    SendAuditionrequest() {
        Keyboard.dismiss();
        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({
                actor_id: this.state.Actorid,
                project_id: this.state.Projectid,
                role_id: this.state.SelectRoleid,
                status: 'Pending'
            });
            PostData(API.SendAuditionrequest, APIData)
                .then((responseJson) => {
                    if (responseJson.statusCode == 1) {
                        this.refs.toast1.show(responseJson.message)
                        this.setState({ Projectid: '', SelectRoleid: '', Projectname: '', Rolename: '' })
                        this.ViewArtistProfiel();
                    }
                    else {
                        this.setState({ isLoading: false });
                        Alert.alert(responseJson.message);
                    }
                }).catch(() => {
                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }
    }
    render() {
        return (
            <View style={styles.mainContainer}>
                <StatusBar barStyle="light-content" backgroundCopr={AppConstants.SHADOWCOLORS.INTRO} animated translucent></StatusBar>
                <OfflineNotice />
                <View style={styles.mainContainer}>
                    <Toast
                        ref="toast"
                        style={[AppConstants.CommonStyles.ToastStyle1, { justifyContent: 'flex-end', height: AppConstants.getDeviceHeight(11), }]}
                        position="top"
                        positionValue={AppConstants.getDeviceHeight(0)}
                        fadeInDuration={750}
                        fadeOutDuration={1000}
                        textStyle={AppConstants.CommonStyles.ToastTextStyle}
                    />
                    <Toast
                        ref="toast1"
                        style={[AppConstants.CommonStyles.ToastStyle2, { height: AppConstants.getDeviceHeight(6.5), }]}
                        position='bottom'
                        positionValue={200}
                        fadeInDuration={1000}
                        fadeOutDuration={1000}
                        textStyle={AppConstants.CommonStyles.ToastTextStyle1} />
                    <ScrollView contentContainerStyle={{ flexGrow: 1, marginBottom: AppConstants.getDeviceHeight(10), }}>
                        <View style={styles.profilepikview}>
                            <Image source={{ uri: this.state.Profilepic == '' ? Images.noimage : this.state.Profilepic }} style={styles.profile}></Image>
                        </View>

                        <View style={styles.Profilenamedetail}>
                            <Text style={styles.profilenametetxt}>{this.state.name}
                            </Text>

                        </View>
                        <View style={styles.Profiledetail}>
                            <Text style={styles.profieldetailtextfix}>Age:
                        </Text>
                            <Text style={styles.profieldetailtext}> {this.state.Age}
                            </Text>
                        </View>
                        <View style={styles.Profiledetail}>
                            <Text style={styles.profieldetailtextfix}>First Language:
                        </Text>
                            <Text style={styles.profieldetailtext}> {this.state.Motherlanguage}
                            </Text>
                        </View>
                        <View style={styles.Profiledetail}>
                            <Text style={styles.profieldetailtextfix}>Other Languages Known:
                        </Text>
                            <Text style={styles.profieldetailtext}> {this.state.Otheranguage}
                            </Text>
                        </View>
                        <View style={styles.Profiledetail}>
                            <Text style={styles.profieldetailtextfix}>Skills:
                        </Text>
                            <Text style={styles.profieldetailtext}> {this.state.Skill}
                            </Text>
                        </View>
                        <View style={styles.Profiledetail}>
                            <Text style={styles.profieldetailtextfix}>Height:
                        </Text>
                            <Text style={styles.profieldetailtext}> {this.state.Height}
                            </Text>
                        </View>
                        <View style={styles.Profiledetail}>
                            <Text style={styles.profieldetailtextfix}>Body Type:
                        </Text>
                            <Text style={styles.profieldetailtext}> {this.state.bodytype}
                            </Text>
                        </View>
                        {this.state.Profilerequestbutton == 'Yes' ? null : this.state.ProfileAuditionbutton == 'Yes' ? this.state.Profilerequeststatus == '' ? <View>
                            <View style={styles.Profiledetail}>
                                <Text style={styles.profieldetailtextfix}>Phone number:
                        </Text>
                                <Text style={styles.profieldetailtext}> {this.state.Phonenumber}
                                </Text>
                            </View>
                            <View style={styles.Profiledetail}>
                                <Text style={styles.profieldetailtextfix}>Email Id:
                        </Text>
                                <Text style={styles.profieldetailtext}> {this.state.Emailid}
                                </Text>
                            </View>
                        </View> : this.state.Profilerequeststatus == 'Accept' ? <View>
                            <View style={styles.Profiledetail}>
                                <Text style={styles.profieldetailtextfix}>Phone number:
                        </Text>
                                <Text style={styles.profieldetailtext}> {this.state.Phonenumber}
                                </Text>
                            </View>
                            <View style={styles.Profiledetail}>
                                <Text style={styles.profieldetailtextfix}>Email Id:
                        </Text>
                                <Text style={styles.profieldetailtext}> {this.state.Emailid}
                                </Text>
                            </View>
                        </View> : null : null}
                        {this.state.Profilerequeststatus == 'Pending' ? <View style={styles.Profiledetail}>
                            <Text style={styles.profieldetailtextfix}>Profile Request Status:
                        </Text>
                            <Text style={styles.profieldetailtext1}> {this.state.Profilerequeststatus}
                            </Text>
                        </View> : null}
                        {this.state.Profilerequestbutton == 'Yes' ? null : this.state.ProfileAuditionbutton == 'Yes' && this.state.Profilerequeststatus == '' ?
                            <View onPress={() => { this.props.navigation.navigate(AppConstants.SCREENS.ACTROVIDEO, { artistid: this.state.Actorid }) }}
                                style={styles.intorview}>
                                <TouchableOpacity onPress={() => { this.props.navigation.navigate('ActorVideo', { artistid: this.state.Actorid }) }}
                                    style={styles.introtouch}>
                                    <Icons name="video-camera" style={styles.intoricon} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS16)} />
                                    <Text style={styles.introvideo}>Intro Video</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => { this.props.navigation.navigate(AppConstants.SCREENS.ACTORPHOTO, { artistid: this.state.Actorid }) }}
                                    style={styles.introtouch1}>
                                    <Icons name="image" style={styles.intoricon} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS16)} />
                                    <Text style={styles.introvideo}>Intro Image</Text>
                                </TouchableOpacity>
                            </View> : this.state.ProfileAuditionbutton == 'Yes' && this.state.Profilerequeststatus == 'Accept' ?
                                <View style={styles.intorview}>
                                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('ActorVideo', { artistid: this.state.Actorid }) }}
                                        style={styles.introtouch}>
                                        <Icons name="video-camera" style={styles.intoricon} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS16)} />
                                        <Text style={styles.introvideo}>Intro Video</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => { this.props.navigation.navigate(AppConstants.SCREENS.ACTORPHOTO, { artistid: this.state.Actorid }) }}
                                        style={styles.introtouch1}>
                                        <Icons name="image" style={styles.intoricon} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS16)} />
                                        <Text style={styles.introvideo}>Intro Image</Text>
                                    </TouchableOpacity>
                                </View> : null}

                        {this.state.Profilerequestbutton == 'Yes' ? null : this.state.ProfileAuditionbutton == 'Yes' && this.state.Profilerequeststatus == '' ? <View>
                            <View style={styles.viewaccept}></View>

                            <Text style={styles.profieldetailtextfix}>Select Project:
                        </Text>
                            {this.state.Projectname1 == '' ? <Dropdown
                                containerStyle={styles.dropdowncontainer}
                                customStyles={{ placeholderText: { fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.PLANVIEWTEXT), }, }}
                                value={this.state.Projectname}
                                labelHeight={0}
                                style={{ fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12), paddingLeft: AppConstants.getDeviceWidth(1), }}
                                inputContainerStyle={{ borderBottomColor: "transparent" }}
                                labelPadding={0}
                                renderAccessory={() => <Iconss name="down" style={styles.dropdownicon}  size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS14)} />}
                                data={this.state.OngoingProject}
                                onChangeText={id => {
                                    let filteredData = this.state.array.filter(function (item) {
                                        return item.title.includes(id);
                                    });
                                    this.setState({ Projectid: filteredData[0].project_id, Projectname: id, arrayrole: [], rolearryid: [], Projectname11: false });
                                    this.wsRole(filteredData[0].project_id);
                                }} /> : <View style={styles.dropdowncontainer}>
                                    <Text style={styles.profieldetailtext}> {this.state.Projectname1}
                                    </Text>
                                </View>}
                            {this.state.Projectname11 == true ? <Text style={AppConstants.CommonStyles.errorcolor}>{AppConstants.Messages.PROJECT}</Text> : null}

                            <Text style={styles.profieldetailtextfix}>Select Role:
                        </Text>
                            {this.state.Rolename1 == '' ? <Dropdown
                                containerStyle={styles.dropdowncontainer}
                                customStyles={{ placeholderText: { fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.PLANVIEWTEXT), }, }}
                                value={this.state.Rolename}
                                labelHeight={0}
                                style={{ fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12), paddingLeft: AppConstants.getDeviceWidth(1), }}
                                inputContainerStyle={{ borderBottomColor: "transparent" }}
                                labelPadding={0}
                                renderAccessory={() => <Iconss name="down" style={styles.dropdownicon}  size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS14)} />}
                                data={this.state.arrayrole}
                                onChangeText={role => {
                                    this.setState({ Rolename: role });
                                    let filteredData = this.state.rolearryid.filter(function (item) {
                                        return item.character_name.includes(role);
                                    });
                                    this.setState({ SelectRoleid: filteredData[0].role_id, Rolename11: false })
                                }} />
                                : <View style={styles.dropdowncontainer}>
                                    <Text style={styles.profieldetailtext}> {this.state.Rolename1}
                                    </Text>
                                </View>}
                            {this.state.Rolename11 == true ? <Text style={AppConstants.CommonStyles.errorcolor}>{AppConstants.Messages.ROLE}</Text> : null}

                        </View> : this.state.ProfileAuditionbutton == 'Yes' && this.state.Profilerequeststatus == 'Accept' ? <View>
                            <View style={styles.viewaccept}></View>

                            <Text style={styles.profieldetailtextfix}>Select Project:
                        </Text>
                            {this.state.Projectname1 == '' ? <Dropdown
                                containerStyle={styles.dropdowncontainer}
                                customStyles={{ placeholderText: { fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.PLANVIEWTEXT), }, }}
                                value={this.state.Projectname}
                                labelHeight={0}
                                style={{ fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12), paddingLeft: AppConstants.getDeviceWidth(1), }}
                                inputContainerStyle={{ borderBottomColor: "transparent" }}
                                labelPadding={0}
                                dropdownOffset={{ top: 5 }}
                                renderAccessory={() => <Iconss name="down" style={styles.dropdownicon} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS14)} />}
                                data={this.state.OngoingProject}
                                onChangeText={id => {
                                    let filteredData = this.state.array.filter(function (item) {
                                        return item.title.includes(id);
                                    });
                                    this.setState({ Projectid: filteredData[0].project_id, Projectname: id });
                                    this.wsRole(filteredData[0].project_id);
                                }} /> : <View style={styles.dropdowncontainer}>
                                    <Text style={styles.profieldetailtext}> {this.state.Projectname1}
                                    </Text>
                                </View>}
                            {this.state.Projectname11 == true ? <Text style={AppConstants.CommonStyles.errorcolor}>{AppConstants.Messages.PROJECT}</Text> : null}

                            <Text style={styles.profieldetailtextfix}>Select Role:
                        </Text>
                            {this.state.arrayrole == '' && this.state.Rolename1 == '' ? <Text style={[styles.profieldetailtext1, { marginLeft: AppConstants.getDeviceWidth(3) }]}>No Roles</Text> : this.state.Rolename1 == '' ? <Dropdown
                                containerStyle={styles.dropdowncontainer}
                                customStyles={{ placeholderText: { fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.PLANVIEWTEXT), }, }}
                                value={this.state.Rolename}
                                labelHeight={0}
                                style={{ fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12), paddingLeft: AppConstants.getDeviceWidth(1), }}
                                inputContainerStyle={{ borderBottomColor: "transparent" }}
                                labelPadding={0}
                                renderAccessory={() => <Iconss name="down" style={styles.dropdownicon} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS14)} />}
                                data={this.state.arrayrole}
                                onChangeText={role => {
                                    this.setState({ Rolename: role });
                                    let filteredData = this.state.rolearryid.filter(function (item) {
                                        return item.character_name.includes(role);
                                    });
                                    this.setState({ SelectRoleid: filteredData[0].role_id })
                                }} /> : <View style={styles.dropdowncontainer}>
                                    <Text style={styles.profieldetailtext}> {this.state.Rolename1}
                                    </Text>
                                </View>}
                            {this.state.Rolename11 == true ? <Text style={AppConstants.CommonStyles.errorcolor}>{AppConstants.Messages.PROJECT}</Text> : null}
                        </View> : null}
                        <View style={styles.button}>
                            {this.state.Profilerequestbutton == 'Yes' ? <TouchableOpacity style={styles.requestbutton1} onPress={() => { this.SendProfileRequest() }}>
                                <Text style={styles.Signup}>
                                    SEND REQUEST
                                    </Text>
                            </TouchableOpacity> : this.state.ProfileAuditionbutton == 'Yes' ? this.state.Projectname1 == '' ? <TouchableOpacity style={styles.requestbutton} onPress={() => { this.validator() }}>
                                <Text style={styles.Signup}>
                                    SEND AUDITION REQUEST
                                    </Text>
                            </TouchableOpacity> : null : null}
                        </View>
                    </ScrollView>
                </View>
                {this.state.isLoading ? <View style={AppConstants.CommonStyles.spinner}>
                    <ActivityIndicator size='large' ></ActivityIndicator>
                </View> : null}
            </View>
        )
    }
}