import * as AppConstants from '../Helper/AppConstants';
export default {
    mainContainer: {
        flex: 1,
        backgroundColor: AppConstants.COLORS.VIEWBACKGROUND

    },
    projecttitle: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        fontWeight: 'bold',
        marginLeft: AppConstants.getDeviceWidth(5),
        marginTop: AppConstants.getDeviceHeight(1)
    },
    Role: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        fontWeight: 'bold',
        marginLeft: AppConstants.getDeviceWidth(3),
        // marginTop: AppConstants.getDeviceHeight(1)
    },
    projecttitle1: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        fontWeight: 'bold',
        marginLeft: AppConstants.getDeviceWidth(5),

        alignSelf: 'center'
    },
    projecttitle2: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEINACTIVE,
        marginRight: AppConstants.getDeviceWidth(1),
        marginLeft: AppConstants.getDeviceWidth(5),
        // marginTop: AppConstants.getDeviceHeight(2),
        alignSelf: 'center'
    },
    projectdesc: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        marginLeft: AppConstants.getDeviceWidth(5),
        marginTop: AppConstants.getDeviceHeight(1)
    },
    status: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        marginLeft: AppConstants.getDeviceWidth(2),
        marginTop: AppConstants.getDeviceHeight(1),
        marginRight: AppConstants.getDeviceWidth(5)
    },
    projectdesc1: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        marginLeft: AppConstants.getDeviceWidth(3),
        marginTop: AppConstants.getDeviceHeight(0.5),
        marginBottom: AppConstants.getDeviceHeight(1),
        marginRight: AppConstants.getDeviceWidth(7),

    },
    projectdate: {
        fontSize: AppConstants.moderateScale(12),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.datecolor,
        fontWeight: 'bold',
        marginLeft: AppConstants.getDeviceWidth(5),
        marginRight: AppConstants.getDeviceWidth(0.5),
        alignSelf: 'center',
        alignItems: 'center',
        textAlign: 'center',
    },
    line: {
        width: AppConstants.getDeviceWidth(0.5),
        height: AppConstants.getDeviceHeight(5),
        backgroundColor: AppConstants.COLORS.datecolor,
      
    },
    FlatlistContainer: {
        width: AppConstants.getDeviceWidth(98),
        marginTop: AppConstants.getDeviceHeight(0.5),
        justifyContent: 'center',
        shadowOffset: { width: 0, height: 2 },
        shadowColor: AppConstants.SHADOWCOLORS.TEXT_SHADOW,


    },
    datepicker: {
        width: AppConstants.getDeviceWidth(42),
        height: AppConstants.getDeviceHeight(5),
        backgroundColor: AppConstants.COLORS.BACKHOME,
        marginLeft: AppConstants.getDeviceWidth(5),
        justifyContent: 'center',
        marginTop:AppConstants.getDeviceHeight(1)
    },
    datepickerdate: {
        alignSelf: 'center',
        alignItems: 'center',
        textAlign: 'center',
        // paddingRight: AppConstants.getDeviceWidth(5),
     
        paddingBottom: AppConstants.getDeviceWidth(2),
        color: '#000',
        marginTop:AppConstants.getDeviceHeight(1)
    },
    button1: {
        width: AppConstants.getDeviceWidth(100),
        marginTop: AppConstants.getDeviceHeight(8),
        height: AppConstants.getDeviceHeight(3),
        justifyContent: "center",
        marginBottom: AppConstants.getDeviceHeight(5)
    },
    Signup: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        fontWeight: 'bold',
        alignSelf: 'center',
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,

    },
    savetouchable: {
        backgroundColor: AppConstants.COLORS.PROFILEINACTIVE,
        height: AppConstants.getDeviceHeight(5),
        marginBottom: AppConstants.getDeviceHeight(2),
        alignItems: 'center',
        justifyContent: 'center',
        width: AppConstants.getDeviceWidth(35),
        alignSelf: 'center'
    },

    imagebackground: {
        width: AppConstants.getDeviceWidth(100),
        height: AppConstants.getDeviceHeight(40),
        backgroundColor: AppConstants.COLORS.PROFILEBACKGROUND,
        marginTop: AppConstants.getDeviceHeight(0.4),
    },
    plus: {

        position: 'absolute', right: 0, flexDirection: 'row',
        marginRight: AppConstants.getDeviceWidth(2)
    },
    FlatlistContainer1: {
        flexDirection: 'row',
        // height: AppConstants.getDeviceWidth(70),
        flex: 1,
        width: AppConstants.getDeviceWidth(96),
        marginTop: AppConstants.getDeviceHeight(0.2),
        backgroundColor: AppConstants.COLORS.WHITE,
        shadowOffset: { width: 0, height: 2 },
        shadowColor: AppConstants.SHADOWCOLORS.TEXT_SHADOW,
        shadowOpacity: 0.8,
        shadowRadius: 3,
        elevation: 3,
        borderRadius: 3,
        marginLeft: AppConstants.getDeviceWidth(2),
        marginRight: AppConstants.getDeviceWidth(2),
        marginBottom: AppConstants.getDeviceWidth(2),
        marginTop: AppConstants.getDeviceWidth(2),
    },
    modelStyle: {
        flex: 1,
        backgroundColor: AppConstants.COLORS.BLACK,
    },
    fullImageStyle: {
        height: AppConstants.getDeviceHeight(85),
        width: AppConstants.getDeviceWidth(100),
        marginTop: AppConstants.getDeviceHeight(2),
    },
    viewerorr: {
        flex: 1,
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        justifyContent: 'center',
        marginTop: AppConstants.getDeviceHeight(8),
        marginBottom: AppConstants.getDeviceHeight(8)
    },
    error: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        marginTop: 0,
        textAlign: 'center',
        fontWeight: '500',
    },
}