import React, { Component } from 'react';
import {
    Image,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    ActivityIndicator,
    FlatList,
    Keyboard,
    Alert,
    Platform,
    Modal,
} from 'react-native';
import styles from './ProjectdetailStyle'
import * as AppConstants from '../Helper/AppConstants';
import OfflineNotice from '../Helper/OfflineNotice';
import Toast, { DURATION } from 'react-native-easy-toast'
import Icon from "react-native-vector-icons/MaterialIcons";
import Icons from "react-native-vector-icons/FontAwesome";
import API from "../WebServices/API";
import { PostData, internet } from '../WebServices/webAPIRequest';
import DatePicker from 'react-native-datepicker';
import Iconss from "react-native-vector-icons/MaterialIcons";
import ImageViewer from 'react-native-image-zoom-viewer';

const Images = {
    Profileicon: 'profileicon',
    Bio: 'bio',
    Photos: 'photo',
    IntroVideo: 'introvideo',
    Worklink: 'worklink',
    noimage: 'noimage'
}

export default class Projectdetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            picture: Images.Photos,
            Search: '',
            thumbnail: '',
            projectroles: [],
            projecttitle: '',
            project_brief: '',
            startdate: '',
            enddate: '',
            projectid: '',
            Status: '',
            Visibility: '',
            VisibaleUpdatedate: 0,
            Producationhouse: '',
            open_closed_status: '',
            ModalVisibleStatus: false,
            imageuri: '',
            imagename: '',
        };
        this._renderRow = this._renderRow.bind(this);
        this.lists = [
            this.props.navigation.addListener('didFocus', () => { this.componentDidFocus() })
        ];
    }
    ShowModalFunction(visible, imageURL, imagemame) {
        this.setState({
            ModalVisibleStatus: visible,
            imageuri: imageURL,
            imagename: imagemame
        });
    }
    componentDidFocus() {
        this.setState({ VisibaleUpdatedate: 0 })
        this.Projectdetail();
    }
    Projectdetail() {
        Keyboard.dismiss();
        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({
                project_id: this.props.navigation.state.params.projectid,
            });
            PostData(API.ViewProjectDetail, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false });
                    if (responseJson.statusCode == 1) {
                        this.setState({
                            projecttitle: responseJson.data.project_name,
                            project_brief: responseJson.data.project_brief,
                            startdate: responseJson.data.start_date,
                            enddate: responseJson.data.end_date,
                            projectroles: responseJson.data.project_roles,
                            Status: responseJson.data.project_status,
                            Visibility: responseJson.data.visibility,
                            picture: responseJson.data.logo,
                            Producationhouse: responseJson.data.company,
                            open_closed_status: responseJson.data.open_closed_status,
                        });
                    }
                    else {
                        Alert.alert(responseJson.message);
                    }
                }).catch((error) => {

                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }
    }
    Updatedate() {
        Keyboard.dismiss();
        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({
                start_date: this.state.startdate,
                end_date: this.state.enddate,
                project_id: this.props.navigation.state.params.projectid
            });
            PostData(API.ProjectUpdate, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false });
                    if (responseJson.statusCode == 1) {
                        this.refs.toast1.show(responseJson.message)
                    }
                    else {
                        Alert.alert(responseJson.message);
                    }
                }).catch((error) => {

                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }
    }
    _renderRow(item, index, ) {
        return (
            <View style={styles.FlatlistContainer1}>
                <TouchableOpacity
                    onPress={() => { this.props.navigation.navigate(AppConstants.SCREENS.VIEWROLE, { Roleid: item.item.role_id }) }}>
                    <View style={styles.FlatlistContainer}>
                        <Text style={styles.Role}>{item.item.character_name} ({item.item.audition_count})</Text>
                        <View style={{ width: AppConstants.getDeviceWidth(88) }}>
                            <Text style={styles.projectdesc1}>{item.item.character_brief}</Text>
                        </View>
                        <Icons name="angle-right" color={AppConstants.COLORS.PROFILEINACTIVE}
                            style={{ position: 'absolute', right: 0, marginRight: AppConstants.getDeviceWidth(6), alignSelf: 'center' }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS28)} />
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
    render() {
        if (this.state.ModalVisibleStatus) {
            return (
                <Modal
                    transparent={false}
                    animationType={'fade'}
                    closeOnTouchOutside={false}
                    visible={this.state.ModalVisibleStatus}
                    onRequestClose={() => {
                        this.ShowModalFunction(!this.state.ModalVisibleStatus, '');
                    }}>
                    <View style={styles.modelStyle}>
                        <Iconss name="cancel" onPress={() => {

                            this.ShowModalFunction(!this.state.ModalVisibleStatus, '')
                        }} color="white" style={{ marginTop: AppConstants.getDeviceHeight(5), marginLeft: AppConstants.getDeviceWidth(4) }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS25)} />

                        <Text style={{ justifyContent: 'center', marginTop: AppConstants.getDeviceHeight(5), marginLeft: AppConstants.getDeviceHeight(23), position: 'absolute', color: AppConstants.COLORS.WHITE }}>{this.state.imagename}</Text>
                        <ImageViewer style={styles.fullImageStyle} imageUrls={[{
                            url: this.state.imageuri,
                            props: {
                            }
                        }]} />
                    </View>
                </Modal>
            );
        } else {
            return (
                <View style={styles.mainContainer}>
                    <StatusBar barStyle="light-content" backgroundCopr={AppConstants.SHADOWCOLORS.INTRO} animated translucent></StatusBar>
                    <OfflineNotice />
                    <Toast
                        ref="toast"
                        style={[AppConstants.CommonStyles.ToastStyle1, { justifyContent: 'flex-end', height: AppConstants.getDeviceHeight(11), }]}
                        position="top"
                        positionValue={AppConstants.getDeviceHeight(0)}
                        fadeInDuration={750}
                        fadeOutDuration={1000}
                        textStyle={AppConstants.CommonStyles.ToastTextStyle}
                    />
                    <Toast
                        ref="toast1"
                        style={[AppConstants.CommonStyles.ToastStyle2, { height: AppConstants.getDeviceHeight(6.5), }]}
                        position='bottom'
                        positionValue={200}
                        fadeInDuration={1000}
                        fadeOutDuration={1000}
                        textStyle={AppConstants.CommonStyles.ToastTextStyle1} />
                    <View style={styles.mainContainer}>
                        <ScrollView contentContainerStyle={{ flexGrow: 1, marginBottom: AppConstants.getDeviceHeight(10), }}>

                            <TouchableOpacity style={styles.imagebackground} onPress={()=>{this.ShowModalFunction(true, this.state.picture, '')}}>
                                <Image source={{ uri: this.state.picture == '' ? Images.noimage : this.state.picture }} style={{ flex: 1, resizeMode: 'contain' }}>

                                </Image>
                            </TouchableOpacity>

                            <Text style={styles.projecttitle}>Project Name: {this.state.projecttitle}</Text>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.projecttitle}>Production House:</Text>
                                <Text style={styles.status}>{this.state.Producationhouse}</Text>
                            </View>
                            <Text style={[styles.projecttitle]}>Project Brief</Text>
                            <Text style={styles.projectdesc}>{this.state.project_brief}</Text>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={[styles.projecttitle]}>Status:</Text>
                                <Text style={styles.status}>{this.state.Status}</Text>

                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={[styles.projecttitle]}>Visibility:</Text>
                                <Text style={styles.status}>{this.state.Visibility}</Text>
                            </View>
                            <Text style={styles.projectdesc}>Start Date</Text>
                            <View style={{ flexDirection: 'row', marginTop: AppConstants.getDeviceHeight(0.5) }}>
                                <DatePicker
                                    style={styles.datepicker}
                                    date={this.state.startdate}
                                    mode="date"
                                    placeholder="DD/MM/YYYY"
                                    format="DD-MM-YYYY"
                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"

                                    showIcon={true}
                                    iconComponent={<View style={{ flexDirection: 'row', justifyContent: 'center', height: AppConstants.getDeviceHeight(5), }}>
                                        <View style={[styles.line, { marginTop: Platform.isPad == true ? AppConstants.getDeviceHeight(2) : 0 }]}></View>
                                        <View style={{
                                            width: AppConstants.getDeviceWidth(10),
                                            height: AppConstants.getDeviceHeight(5),
                                            backgroundColor: AppConstants.COLORS.BACKHOME,
                                            marginTop: Platform.isPad == true ? AppConstants.getDeviceHeight(2) : 0,
                                            justifyContent: 'center',
                                        }}>
                                            <Icon name="date-range" color={AppConstants.COLORS.datecolor} style={{ marginLeft: AppConstants.getDeviceWidth(1), alignItems: 'center', marginTop: AppConstants.moderateScale(1) }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS28)} />

                                        </View>
                                    </View>
                                    }
                                    customStyles={{
                                        dateIcon: {
                                            position: 'absolute',
                                            left: 0,

                                            marginLeft: 0
                                        },
                                        dateInput: {
                                            borderLeftWidth: 0,
                                            borderRightWidth: 0,
                                            borderTopWidth: 0,
                                            borderBottomWidth: 0,
                                        },
                                        borderBottomColor: AppConstants.COLORS.WHITE,
                                        dateIcon: null,
                                        dateText: styles.datepickerdate,
                                        placeholderText: styles.projectdate
                                    }}
                                    onDateChange={(date) => {
                                        this.setState({ startdate: date, VisibaleUpdatedate: 1 })
                                    }} />
                                {this.state.open_closed_status == "Close" ? <TouchableOpacity style={{ marginLeft: AppConstants.getDeviceWidth(5), position: 'absolute', width: AppConstants.getDeviceWidth(45), height: AppConstants.getDeviceHeight(5), }}></TouchableOpacity> : null}


                            </View>
                            <Text style={styles.projectdesc}>End Date</Text>
                            <View style={{ flexDirection: 'row', marginTop: AppConstants.getDeviceHeight(0.5) }}>
                                <DatePicker
                                    style={styles.datepicker}
                                    date={this.state.enddate}
                                    mode="date"
                                    placeholder="DD/MM/YYYY"
                                    format="DD-MM-YYYY"

                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    showIcon={true}
                                    iconComponent={<View style={{ flexDirection: 'row', justifyContent: 'center', height: AppConstants.getDeviceHeight(5), }}>
                                        <View style={[styles.line, { marginTop: Platform.isPad == true ? AppConstants.getDeviceHeight(2) : 0 }]}></View>
                                        <View style={{
                                            width: AppConstants.getDeviceWidth(10),
                                            height: AppConstants.getDeviceHeight(5),
                                            backgroundColor: AppConstants.COLORS.BACKHOME,
                                            marginTop: Platform.isPad == true ? AppConstants.getDeviceHeight(2) : 0,
                                            justifyContent: 'center',
                                        }}>
                                            <Icon name="date-range" color={AppConstants.COLORS.datecolor} style={{ marginLeft: AppConstants.getDeviceWidth(1), alignItems: 'center', marginTop: AppConstants.moderateScale(1) }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS28)} />

                                        </View>
                                    </View>
                                    }
                                    customStyles={{
                                        dateIcon: {
                                            position: 'absolute',
                                            left: 0,

                                            marginLeft: 0
                                        },
                                        dateInput: {
                                            borderLeftWidth: 0,
                                            borderRightWidth: 0,
                                            borderTopWidth: 0,
                                            borderBottomWidth: 0,
                                        },
                                        borderBottomColor: AppConstants.COLORS.WHITE,
                                        dateIcon: null,
                                        dateText: styles.datepickerdate,
                                        placeholderText: styles.projectdate
                                    }}
                                    onDateChange={(date) => {
                                        this.setState({ enddate: date, VisibaleUpdatedate: 1 })
                                    }} />
                                {this.state.open_closed_status == "Close" ? <TouchableOpacity style={{ marginLeft: AppConstants.getDeviceWidth(5), position: 'absolute', width: AppConstants.getDeviceWidth(45), height: AppConstants.getDeviceHeight(5), }}></TouchableOpacity> : null}


                            </View>
                            <View style={{ width: AppConstants.getDeviceWidth(100), height: AppConstants.moderateScale(0.5), marginBottom: AppConstants.getDeviceHeight(2), marginTop: AppConstants.getDeviceHeight(3), backgroundColor: AppConstants.COLORS.PROFILEBACKGROUND }}></View>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', height: AppConstants.getDeviceHeight(6), }}>
                                <Text style={styles.projecttitle1}>Role</Text>
                                <TouchableOpacity onPress={() => { this.props.navigation.navigate(AppConstants.SCREENS.ADDROLE, { projectid: this.props.navigation.state.params.projectid, projectname: this.state.projecttitle }) }} style={styles.plus}>

                                    <Text style={styles.projecttitle2}>Add Role</Text>

                                    <Icons name="plus-square-o" color={AppConstants.COLORS.PROFILEINACTIVE} style={{ alignSelf: 'center', marginTop: AppConstants.getDeviceHeight(0.5) }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS24)} />


                                </TouchableOpacity>
                            </View>
                            {this.state.projectroles == '' ? <View style={styles.viewerorr}>
                                <Text style={styles.error}> Start adding Roles by clicking on Add Role!</Text>
                            </View> :
                                <FlatList
                                    style={{ marginTop: AppConstants.getDeviceHeight(1) }}
                                    scrollEnabled={true}
                                    data={this.state.projectroles}
                                    keyExtractor={(item, index) => index.toString()}
                                    renderItem={this._renderRow.bind(this)} />}
                        </ScrollView>
                    </View>
                    {this.state.isLoading ? <View style={AppConstants.CommonStyles.spinner}>
                        <ActivityIndicator size='large' ></ActivityIndicator>
                    </View> : null}
                    {this.state.VisibaleUpdatedate == 1 ? <View style={styles.button1}>
                        <TouchableOpacity style={styles.savetouchable} onPress={() => { this.Updatedate() }}>
                            <Text style={styles.Signup}>
                                UPDATE
                                    </Text>
                        </TouchableOpacity>
                    </View> : null}
                </View>
            )
        }
    }
}