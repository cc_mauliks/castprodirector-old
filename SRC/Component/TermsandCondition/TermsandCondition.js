import React, { Component } from 'react';
import {
    ScrollView,
    View,
    ActivityIndicator
} from 'react-native';
import API from "../WebServices/API";
import { PostData, internet } from '../WebServices/webAPIRequest';
import styles from './TermsandConditionStyle'
import * as AppConstants from '../Helper/AppConstants';
import HTML from 'react-native-render-html';

export default class TermsandCondition extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            terms: '',
        }
    }
    componentDidMount() {
        this.TermsAndCondition()
    }
    TermsAndCondition() {
        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({
            });
            PostData(API.Termsandcondition, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false, terms: responseJson.data.Actor.tc_text });
                })
                .catch(() => {
                    this.setState({ isLoading: false });
                    this.refs.toast.show(AppConstants.Messages.APIERROR, DURATION.LENGTH_LONG);
                });
        }
    }
    render() {
        if (this.state.isLoading) {
            return AppConstants.ShowActivityIndicator();
        }
        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{ flex: 1 }}>
                    <HTML html={this.state.terms} baseFontStyle={{ color: AppConstants.COLORS.BLACK, }} containerStyle={{ paddingLeft: AppConstants.getDeviceWidth(2), paddingRight: AppConstants.getDeviceWidth(2) }} />

                </ScrollView>

            </View>
        )
    }
}