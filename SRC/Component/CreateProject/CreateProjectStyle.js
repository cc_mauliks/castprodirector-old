import * as AppConstants from '../Helper/AppConstants';
export default {
    mainContainer: {
        flex: 1,
    },
    projectdesc: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        marginLeft: AppConstants.getDeviceWidth(5),
        marginTop: AppConstants.getDeviceHeight(2)
    },
    projectdesc1: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS13),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PLACEHOLDAR,


    },
    textboxText1: {
        width: AppConstants.getDeviceWidth(90),
        height: AppConstants.getDeviceHeight(4),
        color: AppConstants.COLORS.BLACK,
        backgroundColor: AppConstants.COLORS.PROFILETEXINPUT,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS13),
        marginLeft: AppConstants.getDeviceWidth(5),
        marginRight: AppConstants.getDeviceWidth(7),
        paddingLeft: AppConstants.getDeviceWidth(2),
        justifyContent: 'center',
        padding: 0,
        margin: 0,
        marginTop: AppConstants.getDeviceHeight(1),
    },
    textmultiline: {
        width: AppConstants.getDeviceWidth(90),
        height: AppConstants.getDeviceHeight(11),
        color: AppConstants.COLORS.BLACK,
        backgroundColor: AppConstants.COLORS.PROFILETEXINPUT,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS13),
        marginLeft: AppConstants.getDeviceWidth(5),
        marginRight: AppConstants.getDeviceWidth(2),
        paddingLeft: AppConstants.getDeviceWidth(2),
        marginTop: AppConstants.getDeviceHeight(1)
    },
    textboxText: {
        width: AppConstants.getDeviceWidth(40),
        height: AppConstants.getDeviceHeight(5),
        backgroundColor: AppConstants.COLORS.BACKHOME,
        marginTop: AppConstants.getDeviceHeight(1),
        marginLeft: AppConstants.getDeviceWidth(5),
        flexDirection: 'row',
    },
    projectdate: {
        fontSize: AppConstants.moderateScale(12),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.datecolor,
        fontWeight: 'bold',
        marginLeft: AppConstants.getDeviceWidth(5),
        marginRight: AppConstants.getDeviceWidth(0.5),
        alignSelf: 'center',
        alignItems: 'center',
        textAlign: 'center',

    },
    line: {
        width: AppConstants.getDeviceWidth(0.5),
        height: AppConstants.getDeviceHeight(5),
        backgroundColor: AppConstants.COLORS.datecolor,
        // marginBottom: AppConstants.getDeviceHeight(1)
    },
    button1: {
        width: AppConstants.getDeviceWidth(100),
        marginTop: AppConstants.getDeviceHeight(8),
        height: AppConstants.getDeviceHeight(3),
        justifyContent: "center",
    },
    savetouchable: {
        backgroundColor: AppConstants.COLORS.PROFILEINACTIVE,
        height: AppConstants.getDeviceHeight(5),
        marginBottom: AppConstants.getDeviceHeight(2),
        alignItems: 'center',
        justifyContent: 'center',
        width: AppConstants.getDeviceWidth(35),
        alignSelf: 'center'
    },
    datepicker: {
        width: AppConstants.getDeviceWidth(42),
        height: AppConstants.getDeviceHeight(5),
        backgroundColor: AppConstants.COLORS.BACKHOME,
        marginLeft: AppConstants.getDeviceWidth(5),
        justifyContent: 'center',
        marginTop: AppConstants.getDeviceHeight(1)
    },
    datepickerdate: {
        alignSelf: 'center',
        alignItems: 'center',
        textAlign: 'center',
        paddingBottom: AppConstants.getDeviceWidth(2),
        color: '#000',
        marginTop: AppConstants.getDeviceHeight(1)
    },
    FlatlistContainer: {
        flexDirection: 'row',
        width: AppConstants.getDeviceWidth(100),
        marginTop: AppConstants.getDeviceHeight(0.5),
        shadowOffset: { width: 0, height: 2 },
        shadowColor: AppConstants.SHADOWCOLORS.TEXT_SHADOW,
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
    },
    profileview: {
        flexDirection: 'row',
        height: AppConstants.getDeviceHeight(15),
    },
    profile: {
        width: AppConstants.getDeviceHeight(13),
        height: AppConstants.getDeviceHeight(13),
        borderRadius: AppConstants.getDeviceHeight(13) / 2,
        marginLeft: AppConstants.getDeviceWidth(5),
        alignSelf: 'center',
    },
    lin2: {
        width: AppConstants.getDeviceWidth(100),
        height: AppConstants.getDeviceHeight(0.5),
        backgroundColor: AppConstants.COLORS.PLACEHOLDAR,
        marginTop: AppConstants.getDeviceHeight(1)
    },
    textboxText: {
        width: AppConstants.getDeviceWidth(86),
        height: AppConstants.getDeviceHeight(5),
        color: AppConstants.COLORS.BLACK,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS13),
        marginLeft: AppConstants.getDeviceWidth(2),
        paddingLeft: AppConstants.getDeviceWidth(10),
        backgroundColor: AppConstants.COLORS.WHITE,
        padding: 0,
        margin: 0,
        border: 5,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: AppConstants.COLORS.PROFILEBACKGROUND,
        marginTop: AppConstants.getDeviceHeight(2)
    },
    textinputview: {
        flexDirection: 'row',
        height: AppConstants.getDeviceHeight(7),
        width: AppConstants.getDeviceWidth(86),
    },
    cancelicon: {
        position: 'absolute',
        right: 0,
        paddingTop: AppConstants.getDeviceHeight(2),
        alignSelf: 'center'
    },
    searchicon: {
        position: 'absolute',
        marginLeft: AppConstants.getDeviceWidth(5),
        paddingTop: AppConstants.getDeviceHeight(2),
        alignSelf: 'center'
    },
    modal: {
        width: AppConstants.getDeviceWidth(90),
        height: AppConstants.getDeviceHeight(80),
        backgroundColor: AppConstants.COLORS.VIEWBACKGROUND,
        marginTop: AppConstants.getDeviceHeight(5),
        borderRadius: 5
    },
    modaltextstyle: {
        alignSelf: 'center',
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
    },
    switchview: {
        width: AppConstants.getDeviceWidth(100),
        marginTop: AppConstants.getDeviceHeight(1),
        flexDirection: 'row',
        height: AppConstants.getDeviceHeight(7),
    },
    switch: {
        height: AppConstants.getDeviceWidth(5),
        width: AppConstants.getDeviceWidth(30),
        paddingTop: 0,
        marginLeft: AppConstants.getDeviceWidth(4),
        marginTop: AppConstants.getDeviceHeight(2)
    },
    dateicon: {
        marginLeft: AppConstants.getDeviceWidth(1),
        alignItems: 'center',
        marginTop: AppConstants.moderateScale(1)
    },
    done: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontWeight: '600',
        alignSelf: 'center',
        color: AppConstants.COLORS.PROFILEBACKGROUND,
    },
    doneview: {
        borderBottomRightRadius: AppConstants.getDeviceWidth(1.5),
        borderBottomLeftRadius: AppConstants.getDeviceWidth(1.5),
        height: AppConstants.getDeviceHeight(5),
        width: AppConstants.getDeviceWidth(90),
        backgroundColor: AppConstants.COLORS.PROFILEINACTIVE,
        justifyContent: 'center',
    },
    close:{
        position: 'absolute',
        right: 0,
        marginRight: AppConstants.getDeviceWidth(1)
    }
}