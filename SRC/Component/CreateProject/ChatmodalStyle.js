import * as AppConstants from '../Helper/AppConstants';
import { Platform } from 'react-native';


export default {
    mainContainer: {
        flex: 1,
        marginBottom:AppConstants.getDeviceHeight(5),
    },
    FlatlistContainer: {
        flexDirection: 'row',
     
        width: AppConstants.getDeviceWidth(99.50),
        shadowOffset: { width: 0, height: 2 },
        shadowColor: AppConstants.SHADOWCOLORS.TEXT_SHADOW,
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
    },
    sentat: {
        // marginRight: AppConstants.getDeviceHeight(2),
        // marginLeft: AppConstants.getDeviceHeight(0.5),
    },
    textnameContainer: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS15),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_1,
        borderRadius: 5,
        borderWidth: 1,
        borderTopRightRadius: 40,
        borderTopLeftRadius: 40,
        borderBottomLeftRadius: 40,
        borderBottomRightRadius: 40,
        borderWidth: 1.5,
        borderColor: AppConstants.SHADOWCOLORS.INTRO,
        backgroundColor: AppConstants.COLORS.TABVIEW,
        padding: AppConstants.getDeviceWidth(3),
        // margin:AppConstants.getDeviceWidth(1)
        marginRight: AppConstants.getDeviceHeight(1),
        marginLeft: AppConstants.getDeviceHeight(1),
        marginTop: AppConstants.getDeviceHeight(0.3),
    },
    header: {
        height: AppConstants.getDeviceWidth(10),

        borderRadius: Platform.OS === 'ios' ? 30 : 5,
        width: AppConstants.getDeviceWidth(89.47),
    },
    text: {
        justifyContent: "center",
        alignItems: "center",
        justifyContent: 'space-between',
        alignSelf: 'center',
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        fontWeight: AppConstants.WEIGHT.FONTWEIGHT
    },
    header1: {
        height: AppConstants.getDeviceWidth(13),
        borderTopRightRadius: Platform.OS === 'ios' ? 30 : 5,
        borderTopLeftRadius: Platform.OS === 'ios' ? 30 : 5,
        width: AppConstants.getDeviceWidth(89.47),
    },
    swipeline: {
        width: AppConstants.getDeviceWidth(0.5),
        //width: AppConstants.getDeviceWidth(0.05),
        //backgroundColor: AppConstants.COLORS.GRADIENT_1,
    },
    textname: {
        color: AppConstants.COLORS.BLACK,
        fontFamily: AppConstants.FONTFAMILY.fontFamily_1,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS13),
    },
    model: {
        margin: 0,
        justifyContent: "center",
        alignItems: "center",
        padding: 0,
        borderRadius: Platform.OS === 'ios' ?  5 : 5,
        height: AppConstants.getDeviceHeight(72.69),
        width: AppConstants.getDeviceWidth(89.47),
        marginLeft: AppConstants.getDeviceWidth(6),
       
    },
}
