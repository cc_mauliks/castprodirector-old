import React, { Component } from 'react';
import {
    TextInput,
    Image,
    ScrollView,
    View,
    Text,
    StatusBar,
    ActivityIndicator,
    TouchableOpacity,
    FlatList,
    Alert,
    NativeModules,
    Keyboard,
    Platform
} from 'react-native';
import styles from './CreateProjectStyle'
import * as AppConstants from '../Helper/AppConstants';
import OfflineNotice from '../Helper/OfflineNotice';
import Toast, { DURATION } from 'react-native-easy-toast'
import Icon from "react-native-vector-icons/MaterialIcons";
import DatePicker from 'react-native-datepicker';
import ImagePicker from "react-native-image-picker";
var ImagePicker1 = NativeModules.ImageCropPicker;
import SwitchSelector from "react-native-switch-selector";
import Modal from "react-native-modal";
import Icons from "react-native-vector-icons/AntDesign";
import Permissions from "react-native-permissions";
import Close from "react-native-vector-icons/MaterialCommunityIcons";
import Iconradio from 'react-native-vector-icons/Ionicons';
import API from "../WebServices/API";
import { PostData, internet } from '../WebServices/webAPIRequest';
var number = 0;
const Images = {
    Profileimage: 'profileimage',
}
const options = {
    quality: 1.0,
    cameraType: 'front',
    maxWidth: 1024,
    maxHeight: 1024,
    storageOptions: {
        skipBackup: true
    }
};
const options1 = [
    { label: "01:00", value: "1" },
    { label: "01:30", value: "1.5" },
    { label: "02:00", value: "2" }
];
var selectarray = [];
export default class CreateProject extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ImageSource: Images.Profileimage,
            isLoading: false,
            title: '',
            Description: '',
            producationhousename: '',
            startdate: '',
            enddate: '',

            title1: false,
            Description1: false,
            producationhousename1: false,
            startdate1: false,
            enddate1: false,

            Director: [],
            DirectorList: [],
            switchOn1: false,
            switchOn2: true,
            modal1: 0,

            Base: '',
            rightname: '', leftname: 'Public',
            language: '',
            dataSource: [],
            directorname: '',
            isRefreshing: false,
            nexturl: '',
            projecttext: ''
        }
        this.page = 1;
        this.onEndReachedCalledDuringMomentum = true;
        this._renderRow = this._renderRow.bind(this);
        this._renderRow1 = this._renderRow1.bind(this);
        this.lists = [
            this.props.navigation.addListener('didFocus', () => { this.componentDidFocus() })
        ];
    }

    componentDidFocus() {
        selectarray = [];
        this.Getlanguage(1);
    }
    async Getlanguage(page, name) {
        this.setState({ isLoading: true });
        APIData = JSON.stringify({
            director_name: name
        });
        PostData(API.GetDirectorList + '?page=' + page, APIData)
            .then((responseJson) => {
                responseJson.data = responseJson.data.map(item => {
                    var data = selectarray.filter(function (item1) {
                        return item1.id == item.id;
                    })
                    if (data == '') {
                        item.isSelect = false;
                    }
                    else {
                        if (data[0].id == item.id && data[0].isSelect == true) {
                            item.isSelect = true;
                        }
                        else {
                            item.isSelect = false;
                        }
                    }
                    return item;
                });
                this.setState({
                    isLoading: false,
                    isRefreshing: false,
                    dataSource: [
                        ...this.state.dataSource,
                        ...responseJson.data
                    ],
                    nexturl: responseJson.next_page_url
                });
            })
            .catch(() => {
                this.setState({ isLoading: false });
                Alert.alert(AppConstants.Messages.APIERROR);
            });
    }
    onPress1 = () => {
        this.setState({ switchOn1: !this.state.switchOn1, rightname: '', leftname: '' });
        if (this.state.switchOn1 == false) {
            this.setState({ rightname: 'Private', leftname: '' });
        } else {
            this.setState({ rightname: '', leftname: 'Public' });
        }
    }
    onPress2 = () => {
        this.setState({ switchOn2: !this.state.switchOn2 });
    }
    renderFooter = () => {
        if (this.state.isRefreshing == true) {
            return (
                <ActivityIndicator
                    style={{ color: '#000' }} />
            );
        }
        return null;
    };
    handleLoadMore = () => {
        if (this.state.nexturl == '') {
        } else {
            if (!this.onEndReachedCalledDuringMomentum) {
                this.page = this.page + 1
                this.setState({ page: this.state.page + 1, isRefreshing: true, isLoading: false });
                this.Getlanguage(this.page);
                this.onEndReachedCalledDuringMomentum = true;
            }
        }
    };
    takePhoto() {
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
            } else if (response.error) {
                Permissions.check("camera").then(response => {
                    if (response == "denied") {
                        Permissions.request("camera").then(response => {
                            if (response == "denied") {
                                Alert.alert(
                                    "Please allow Permissions to access Camera from Settings -> Apps -> Castpro -> Camera"
                                );
                            }
                        });
                    }
                });
            } else if (response.customButton) {
            } else {
                if (response.data != null) {
                    ImagePicker1.openCropper({
                        path: response.uri,
                        width: AppConstants.getDeviceWidth(100),
                        mediaType: 'photo',
                        height: AppConstants.getDeviceHeight(30),
                        includeBase64: true,
                    }).then(image => {
                        var base64Icon = 'data:image/png;base64,' + image.data;
                        this.setState({
                            Base: image.data,
                            ImageSource: base64Icon,
                        });
                        ImagePicker1.clean().then(() => {
                        }).catch(e => {
                            alert(e);
                        });
                    });
                }
            }
        });
    }
    _renderRow(item, index) {

        number = number + 1;

        var NewText = item.item.split("-");
        return (
            <View>

                <View style={styles.FlatlistContainer}>
                    <View style={{ width: AppConstants.getDeviceWidth(100), flexDirection: 'row' }}>
                        <Text style={[styles.projectrole, { marginLeft: AppConstants.getDeviceWidth(5) }]}> {NewText[0]}</Text>
                    </View>
                </View>
            </View>
        );
    }
    selectItem = data => {
        data.item.isSelect = !data.item.isSelect;

        const index = this.state.dataSource.findIndex(
            (item) => {
                data.item.id === item.id
            }
        );
        var indexvalue = selectarray.indexOf(data.item)
        var data22 = selectarray.filter(function (item1) {
            return item1.id == data.item.id;
        })
        if (data22 == '') {
            selectarray.push(data.item)
        }
        else {
            selectarray.splice(indexvalue, 1);
        }
        this.state.dataSource[index] = data.item;
        this.setState({
            dataSource: this.state.dataSource
        });
    };
    _renderRow1(item, index) {
        return (
            <TouchableOpacity onPress={() => { this.selectItem(item) }}>
                <View style={{ width: AppConstants.getDeviceWidth(90), marginTop: AppConstants.getDeviceHeight(1), flexDirection: 'row' }}>
                    <View style={{ width: AppConstants.getDeviceWidth(75), flexDirection: 'row' }}>

                        <Text style={[styles.projectrole, { marginLeft: AppConstants.getDeviceWidth(5) }]}> {item.item.name}</Text>
                    </View>
                    <Iconradio style={{ position: 'absolute', right: 0, marginRight: AppConstants.getDeviceWidth(2), alignSelf: 'center' }} name={item.item.isSelect == false ? 'ios-radio-button-off' : 'ios-checkmark-circle-outline'} color={this.props.iconColor} size={AppConstants.moderateScale(25)} />
                </View>
                <View style={styles.lin2}></View>
            </TouchableOpacity>
        );
    }
    validator() {
        if (this.state.title == '') {
            this.setState({ title1: true });
        }
        else if (this.state.Description == '') {
            this.setState({ Description1: true })
        }
        else if (this.state.producationhousename == '') {
            this.setState({ producationhousename1: true })
        }
        else if (this.state.startdate == '') {
            this.setState({ startdate1: true })
        }
        else if (this.state.enddate == '') {
            this.setState({ enddate1: true })
        }
        else {
            this.Createproject();
        }
    }
    Createproject() {
        Keyboard.dismiss();
        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({
                project_name: this.state.title,
                project_brief: this.state.Description,
                company: this.state.producationhousename,
                visibility: this.state.switchOn1 == false ? 'Public' : 'Private',
                project_status: this.state.switchOn2 == false ? 'Inactive' : 'Active',
                director_id: this.state.Director,
                start_date: this.state.startdate,
                end_date: this.state.enddate,
                open_closed_status: 'Open',
                project_logo: this.state.Base
            });
            PostData(API.CreateProject, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false });
                    if (responseJson.statusCode == 1) {
                        this.refs.toast1.show(responseJson.message, DURATION.LENGTH_SHORT, true);
                        this.timeoutHandle = setTimeout(() => {
                            this.props.navigation.goBack();
                        }, 2000);
                    }
                    else {
                        Alert.alert(responseJson.message);
                    }
                }).catch(() => {
                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <StatusBar barStyle="light-content" backgroundCopr={AppConstants.SHADOWCOLORS.INTRO} animated translucent></StatusBar>
                <OfflineNotice />
                <View style={styles.mainContainer}>
                    <Toast
                        ref="toast"
                        style={[AppConstants.CommonStyles.ToastStyle1, { justifyContent: 'flex-end', height: AppConstants.getDeviceHeight(11), }]}
                        position="top"
                        positionValue={AppConstants.getDeviceHeight(0)}
                        fadeInDuration={750}
                        fadeOutDuration={1000}
                        textStyle={AppConstants.CommonStyles.ToastTextStyle}
                    />
                    <Toast
                        ref="toast1"
                        style={[AppConstants.CommonStyles.ToastStyle2, { height: AppConstants.getDeviceHeight(6.5), }]}
                        position='bottom'
                        positionValue={200}
                        fadeInDuration={1000}
                        fadeOutDuration={1000}
                        textStyle={AppConstants.CommonStyles.ToastTextStyle1}
                    />
                    <ScrollView contentContainerStyle={{ flexGrow: 1, }} style={{ marginBottom: AppConstants.getDeviceHeight(1) }}>
                        <View style={styles.profileview}>
                            <TouchableOpacity style={styles.profileview} onPress={() => { this.takePhoto() }}>
                                <Image style={styles.profile} source={{ uri: this.state.ImageSource }} ></Image>
                            </TouchableOpacity>
                        </View>
                        <Text style={[styles.projectdesc]}>Project Title </Text>
                        <TextInput style={styles.textboxText1}
                            placeholderTextColor={AppConstants.COLORS.PLACEHOLDAR}
                            underlineColorAndroid="transparent"
                            value={this.state.title}
                            blurOnSubmit={false}
                            placeholder={'Enter Project title'}
                            multiline={true}
                            autoCapitalize="none"
                            onChangeText={(title) => this.setState({ title, title1: false })} />
                        {this.state.title1 == true ? <Text style={AppConstants.CommonStyles.errorcolor}>{AppConstants.Messages.PROJECTTITLE}</Text> : null}
                        <Text style={[styles.projectdesc]}>Project Description</Text>
                        <TextInput style={styles.textmultiline}
                            placeholderTextColor={AppConstants.COLORS.PLACEHOLDAR}
                            underlineColorAndroid="transparent"
                            value={this.state.Description}
                            blurOnSubmit={false}
                            placeholder={'Enter Short Description'}
                            multiline={true}
                            autoCapitalize="none"
                            onChangeText={(Description) => this.setState({ Description, Description1: false })} />
                        {this.state.Description1 == true ? <Text style={AppConstants.CommonStyles.errorcolor}>{AppConstants.Messages.DESCRIPTION}</Text> : null}

                        <Text style={[styles.projectdesc]}>Production House Name </Text>
                        <TextInput style={styles.textboxText1}
                            placeholderTextColor={AppConstants.COLORS.PLACEHOLDAR}
                            underlineColorAndroid="transparent"
                            value={this.state.producationhousename}
                            blurOnSubmit={false}
                            returnKeyType={"done"}
                            placeholder={'Enter Production House Name'}
                            multiline={true}
                            autoCapitalize="none"
                            onSubmitEditing={() => {
                                Keyboard.dismiss();
                            }}
                            onChangeText={(producationhousename) => this.setState({ producationhousename, producationhousename1: false })} />
                        {this.state.producationhousename1 == true ? <Text style={AppConstants.CommonStyles.errorcolor}>{AppConstants.Messages.PRODUCATIONHOUSENAME}</Text> : null}

                        <Text style={[styles.projectdesc]}>Add More Director  </Text>
                        <TouchableOpacity onPress={() => { this.setState({ modal1: 1 }) }} style={styles.textboxText1}>
                            <Text style={styles.projectdesc1}>Select other Directors to share the project with  </Text>
                        </TouchableOpacity>
                        <FlatList
                            scrollEnabled={true}
                            data={this.state.DirectorList}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={this._renderRow.bind(this)} />
                        <View style={styles.switchview}>
                            <Text style={[styles.projectdesc, { justifyContent: 'center', alignSelf: 'center', marginTop: 0 }]}>Visibility </Text>
                            <SwitchSelector
                                initial={0}
                                hasPadding={false}
                                height={AppConstants.getDeviceHeight(4)}
                                width={AppConstants.getDeviceHeight(10)}
                                onPress={value => this.setState({ switchOn1: value })}
                                textColor={AppConstants.COLORS.PROFILEINACTIVE} //'#7a44cf'
                                selectedColor={AppConstants.COLORS.WHITE}
                                buttonColor={AppConstants.COLORS.PROFILEINACTIVE}
                                borderColor={AppConstants.COLORS.PROFILEINACTIVE}
                                hasPadding
                                textStyle={{ fontSize: AppConstants.moderateScale(11) }}
                                selectedTextStyle={{ fontSize: AppConstants.moderateScale(10) }}
                                style={styles.switch}
                                options={[
                                    { label: "Public", value: false, }, //images.feminino = require('./path_to/assets/img/feminino.png')
                                    { label: "Private", value: true, } //images.masculino = require('./path_to/assets/img/masculino.png')
                                ]}
                            />
                        </View>
                        <View style={styles.switchview}>
                            <Text style={[styles.projectdesc, { justifyContent: 'center', alignSelf: 'center', marginTop: 0 }]}>Status </Text>
                            <SwitchSelector
                                initial={0}
                                hasPadding={false}
                                height={AppConstants.getDeviceHeight(4)}
                                width={AppConstants.getDeviceHeight(10)}
                                onPress={(value) => {
                                    this.setState({ switchOn2: value })
                                }}
                                textColor={AppConstants.COLORS.PROFILEINACTIVE} //'#7a44cf'
                                selectedColor={AppConstants.COLORS.WHITE}
                                buttonColor={AppConstants.COLORS.PROFILEINACTIVE}
                                borderColor={AppConstants.COLORS.PROFILEINACTIVE}
                                hasPadding
                                textStyle={{ fontSize: AppConstants.moderateScale(11) }}
                                selectedTextStyle={{ fontSize: AppConstants.moderateScale(10) }}
                                style={styles.switch}
                                options={[
                                    { label: "Active", value: true, },
                                    { label: "Inactive", value: false, }, //images.feminino = require('./path_to/assets/img/feminino.png')
                                ]}
                            />
                        </View>
                        <Text style={styles.projectdesc}>Start Date</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <DatePicker
                                style={styles.datepicker}
                                date={this.state.startdate}
                                mode="date"
                                placeholder="DD/MM/YYYY"
                                format="DD-MM-YYYY"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                showIcon={true}
                                iconComponent={<View style={{ flexDirection: 'row', justifyContent: 'center', height: AppConstants.getDeviceHeight(5), }}>
                                    <View style={[styles.line, {
                                        marginTop: Platform.isPad == true ? AppConstants.getDeviceHeight(2) : 0
                                    }]}></View>
                                    <View style={{
                                        width: AppConstants.getDeviceWidth(10),
                                        height: AppConstants.getDeviceHeight(5),
                                        backgroundColor: AppConstants.COLORS.BACKHOME,
                                        marginTop: Platform.isPad == true ? AppConstants.getDeviceHeight(2) : 0,
                                        justifyContent: 'center',
                                    }}>
                                        <Icon name="date-range" color={AppConstants.COLORS.datecolor} style={styles.dateicon} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS28)} />
                                    </View>
                                </View>
                                }
                                customStyles={{
                                    dateIcon: {
                                        position: 'absolute',
                                        left: 0,
                                        marginLeft: 0
                                    },
                                    dateInput: {
                                        borderLeftWidth: 0,
                                        borderRightWidth: 0,
                                        borderTopWidth: 0,
                                        borderBottomWidth: 0,
                                    },
                                    borderBottomColor: AppConstants.COLORS.WHITE,
                                    dateIcon: null,
                                    dateText: styles.datepickerdate,
                                    placeholderText: styles.projectdate
                                }}
                                onDateChange={(date) => {
                                    this.setState({ startdate: date, startdate1: false })
                                }} />

                        </View>
                        {this.state.startdate1 == true ? <Text style={AppConstants.CommonStyles.errorcolor}>{AppConstants.Messages.STARTDATE}</Text> : null}

                        <Text style={styles.projectdesc}>End Date</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <DatePicker
                                style={styles.datepicker}
                                date={this.state.enddate}
                                mode="date"
                                placeholder="DD/MM/YYYY"
                                format="DD-MM-YYYY"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                showIcon={true}
                                iconComponent={<View style={{ flexDirection: 'row', justifyContent: 'center', height: AppConstants.getDeviceHeight(5), }}>
                                    <View style={[styles.line, {
                                        marginTop: Platform.isPad == true ? AppConstants.getDeviceHeight(2) : 0
                                    }]}></View>
                                    <View style={{
                                        width: AppConstants.getDeviceWidth(10),
                                        height: AppConstants.getDeviceHeight(5),
                                        backgroundColor: AppConstants.COLORS.BACKHOME,
                                        marginTop: Platform.isPad == true ? AppConstants.getDeviceHeight(2) : 0,
                                        justifyContent: 'center',
                                    }}>
                                        <Icon name="date-range" color={AppConstants.COLORS.datecolor} style={styles.dateicon} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS28)} />

                                    </View>
                                </View>
                                }
                                customStyles={{
                                    dateIcon: {
                                        position: 'absolute',
                                        left: 0,
                                        marginLeft: 0
                                    },
                                    dateInput: {
                                        borderLeftWidth: 0,
                                        borderRightWidth: 0,
                                        borderTopWidth: 0,
                                        borderBottomWidth: 0,
                                    },
                                    borderBottomColor: AppConstants.COLORS.WHITE,
                                    dateIcon: null,
                                    dateText: styles.datepickerdate,
                                    placeholderText: styles.projectdate
                                }}
                                onDateChange={(date) => {
                                    this.setState({ enddate: date, enddate1: false })
                                }} />

                        </View>
                        {this.state.enddate1 == true ? <Text style={AppConstants.CommonStyles.errorcolor}>{AppConstants.Messages.ENDDATE}</Text> : null}

                        <View style={styles.button1}>
                            <TouchableOpacity style={styles.savetouchable} onPress={() => { this.validator() }}>
                                <Text style={styles.Signup}>
                                    Create
                            </Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
                <Modal
                    isVisible={this.state.modal1 == 0 ? false : true}
                    deviceHeight={AppConstants.getDeviceHeight(100)}
                    deviceWidth={AppConstants.getDeviceWidth(100)}>
                    <View style={styles.modal}>
                        <View style={{ height: AppConstants.getDeviceHeight(4), justifyContent: 'center', }}>
                            <View style={{ width: AppConstants.getDeviceWidth(90), justifyContent: 'center', height: AppConstants.getDeviceHeight(5), }}>
                                <Text style={styles.modaltextstyle}>select directors to share the project with</Text>
                                <Close
                                    name='close'
                                    style={styles.close}
                                    color={AppConstants.COLORS.HEDARBACKGROUND}
                                    onPress={() => {
                                        this.setState({ modal1: 0, directorname: '' })
                                    }}
                                    size={AppConstants.FONTSIZE.FS22}
                                />
                            </View>
                        </View>
                        <View style={styles.lin2}></View>
                        <View style={styles.textinputview}>
                            <TextInput style={styles.textboxText}
                                placeholder="Search Artist"
                                placeholderTextColor={AppConstants.COLORS.PLACEHOLDARLOGIN}
                                underlineColorAndroid="transparent"                                        // underlineColorAndroid="transparent"
                                returnKeyType={"search"}
                                value={this.state.directorname}
                                autoCapitalize="none"
                                onSubmitEditing={() => {

                                    this.setState({ dataSource: [] });
                                    this.Getlanguage(1, this.state.directorname);
                                }}
                                onChangeText={(text) => this.setState({ directorname: text })} />
                            <Icons name="search1" color={AppConstants.COLORS.LANGAUGE}
                                style={styles.searchicon} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS18)} />
                            <Icons onPress={() => {
                                this.setState({ directorname: '', dataSource: [] });
                                this.Getlanguage(1, '');
                            }} name="closecircle" color={AppConstants.COLORS.LANGAUGE}
                                style={styles.cancelicon} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS18)} />

                        </View>
                        <FlatList
                            scrollEnabled={true}
                            data={this.state.dataSource}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={this._renderRow1.bind(this)}
                            onEndReachedThreshold={4}
                            onEndReached={(this.handleLoadMore.bind(this))}
                            ListFooterComponent={this.renderFooter(this.state.isRefreshing)}
                            onMomentumScrollBegin={() => {
                                this.onEndReachedCalledDuringMomentum = false;
                                if (this.state.nexturl == '') {
                                    this.refs.toast1.show('No Artist Found')
                                } else {
                                    this.page = this.page + 1
                                    this.setState({ isRefreshing: true, isLoading: false });
                                    if (!this.onEndReachedCalledDuringMomentum) {
                                        this.Getlanguage(this.page, '');
                                        this.onEndReachedCalledDuringMomentum = true;
                                    }
                                }
                            }}
                        />
                        <TouchableOpacity style={styles.doneview} onPress={() => {
                            var data = this.state.dataSource.filter(function (item) {
                                return item.isSelect == true;
                            }).map(function ({ id }) {
                                return id
                            });
                            var data1 = this.state.dataSource.filter(function (item) {
                                return item.isSelect == true;
                            }).map(function ({ name }) {
                                return name
                            });
                            this.setState({ Director: data, DirectorList: data1, modal1: 0 })
                        }}>
                            <Text style={styles.done}>
                                DONE
                                    </Text>

                        </TouchableOpacity>
                    </View>
                </Modal>
                {this.state.isLoading ? <View style={AppConstants.CommonStyles.spinner}>
                    <ActivityIndicator size='large' ></ActivityIndicator>
                </View> : null}
            </View>
        )
    }
}