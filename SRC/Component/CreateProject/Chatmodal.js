import React, { Component } from 'react';
import { View, Text, ListView, TouchableOpacity, ActivityIndicator, Image, FlatList } from 'react-native';
import styles from './ChatmodalStyle';
import Modal from "react-native-simple-modal";
import * as AppConstants from '../Helper/AppConstants';
import Toast, { DURATION } from 'react-native-easy-toast';
import Close from "react-native-vector-icons/MaterialCommunityIcons";

import { PostData } from "../WebServices/webAPIRequest";
import API from "../WebServices/API";
import CustomMultiPicker from "./multipleSelect";

var array = [];

export default class MultilanguageModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            TextInputValueHolder: '',
            value: 0,
            open: false,
            dataSource: '',
            selected: this.props.pass,
            language: [],
            isLoading: false

        }
    }
    componentDidMount() {
        this.Getlanguage();
        this.setState({ open: true });
    }
    componentWillUnmount() {

        this.openModal();
        this.closeModal();
    }
    Getlanguage() {
        // if (global.isConnected) {
        this.setState({ isLoading: true });

        APIData = JSON.stringify({

        });
        PostData(API.GetDirectorList, APIData)
            .then((responseJson) => {
                this.setState({ isLoading: false, language: [] });
                var arr = [];
                responseJson.data.map((data, index) => {
                    arr.push(
                       data.name+'-'+data.id
                    )
                });
                this.setState({ language: arr });
            })
            .catch(() => {
                this.setState({ isLoading: false });
                this.refs.toast.show(AppConstants.Messages.APIERROR, DURATION.LENGTH_LONG);
            });
    }
    modalDidOpen = () => { };
    modalDidClose = () => {
        
        this.props.func(this.state.dataSource);
        this.setState({ open: false });
    };
    openModal = () => this.setState({ open: true });
    closeModal = () => this.setState({ open: false });
    render() {
        const { navigate } = this.props.navigation;

        return (
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                <Toast
                    ref="toast"
                    style={[AppConstants.CommonStyles.ToastStyle, { justifyContent: 'flex-end', height: AppConstants.getDeviceHeight(8), }]}
                    position="top"
                    positionValue={AppConstants.getDeviceHeight(0)}
                    fadeInDuration={750}
                    fadeOutDuration={1000}
                    textStyle={AppConstants.CommonStyles.ToastTextStyle}
                />
                <Modal
                    closeOnTouchOutside={false}
                    backdrop={true}
                    open={this.state.open}
                    modalDidOpen={this.modalDidOpen}
                    modalDidClose={this.modalDidClose}
                    modalStyle={styles.model}>

                    <View style={styles.mainContainer}>
                        <View style={{ height: AppConstants.getDeviceHeight(4), justifyContent: 'center', }}>


                            <View style={styles.sentat}>
                                <Text style={styles.text}>select directors to share the project with</Text>
                                <Close
                                    name='close'
                                    style={{
                                        position: 'absolute',
                                        alignSelf: 'flex-end'
                                    }}
                                    color={AppConstants.COLORS.HEDARBACKGROUND}
                                    onPress={() => {
                                        this.props.func1();
                                        this.setState({ open: false })
                                    }}
                                    size={AppConstants.FONTSIZE.FS22}
                                />
                            </View>
                        </View>
                        <CustomMultiPicker
                            options={this.state.language}
                            search={true} // should show search bar?
                            multiple={true} //
                            placeholder={"Search"}
                            placeholderTextColor={'#757575'}
                            returnValue={"label"} // label or value
                            callback={(res) => {
                                array = res
                            }} // callback, array of selected items
                            rowBackgroundColor={"#eee"}
                            rowHeight={40}
                            rowRadius={5}
                            marginBotom={AppConstants.getDeviceHeight(5)}
                            callback={(res) => { this.setState({ dataSource: res }); }} 
                            iconColor={AppConstants.COLORS.HEDARBACKGROUND}
                            iconSize={27}
                            selectedIconName={"ios-checkmark-circle-outline"}
                            unselectedIconName={"ios-radio-button-off"}
                            scrollViewHeight={130}
                            icon={1}
                            selected={this.state.selected}
                        />
                    </View>
                    <TouchableOpacity style={{ borderBottomRightRadius: AppConstants.getDeviceWidth(1.5), borderBottomLeftRadius: AppConstants.getDeviceWidth(1.5), height: AppConstants.getDeviceHeight(5), width: AppConstants.getDeviceWidth(90), backgroundColor: AppConstants.COLORS.PROFILEINACTIVE, justifyContent: 'center', }} onPress={() => {
                        var array = [...this.state.dataSource];
                        // array.splice(0, 1)
                        this.setState({ dataSource: array })
                        this.setState({ open: false })
                    }}>
                        <Text style={{
                            fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
                            fontWeight: '600',
                            alignSelf: 'center',
                            color: AppConstants.COLORS.PROFILEBACKGROUND,
                        }}>
                            DONE
                                    </Text>

                    </TouchableOpacity>
                    {this.state.isLoading ? <View style={AppConstants.CommonStyles.spinner}>
                        <ActivityIndicator size='large' ></ActivityIndicator>
                    </View> : null}
                  
                </Modal>
               
            </View >
        );
    }

}
