import * as AppConstants from '../Helper/AppConstants';
export default {
    mainContainer: {
        flex: 1,
       
    },
    ongoingtitle: {
        color: AppConstants.COLORS.BLACK,
        marginLeft: AppConstants.getDeviceWidth(5),
        marginTop: AppConstants.getDeviceHeight(1)
    },
    shorted: {
        color: AppConstants.COLORS.PROFILEINACTIVE,
        marginLeft: AppConstants.getDeviceWidth(5),
        marginTop: AppConstants.getDeviceHeight(1),
        alignSelf:'center',
        position:'absolute',
        right:0,
        fontWeight:'bold',
        marginRight:AppConstants.getDeviceWidth(5),
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
    },
    Comment: {
        color: AppConstants.COLORS.BLACK,
        marginLeft: AppConstants.getDeviceWidth(5),
        marginTop: AppConstants.getDeviceHeight(1),
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
    },
    videotouch: {
        height: AppConstants.getDeviceHeight(25),
        width: AppConstants.getDeviceWidth(100),
        backgroundColor: 'black',
        marginTop: AppConstants.getDeviceHeight(1)
    },
    roleandshortlist: {
        width: AppConstants.getDeviceWidth(100),
        height: AppConstants.getDeviceHeight(6),
        flexDirection: 'row',
    },
    projectdate: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        fontWeight: 'bold',
        marginLeft: AppConstants.getDeviceWidth(5),
        marginRight: AppConstants.getDeviceWidth(0.5),
        alignSelf: 'center',
    },
    shortlist: {
        width: AppConstants.getDeviceWidth(25),
        height: AppConstants.getDeviceHeight(4),
        backgroundColor: AppConstants.COLORS.PROFILEINACTIVE,
        position: 'absolute',
        right: 0,
        marginRight: AppConstants.getDeviceWidth(5),
        alignSelf: 'center',
        flexDirection: 'row'
    },
    star: {
        alignSelf: 'center',
        marginLeft: AppConstants.getDeviceWidth(3)
    },
    shorting: {
        alignSelf: 'center',
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.BLACK,
        marginLeft: AppConstants.getDeviceWidth(1)
    },
    FlatlistContainer: {
        flexDirection: 'row',

        width: AppConstants.getDeviceWidth(100),
        marginTop: AppConstants.getDeviceHeight(2),
        // backgroundColor: AppConstants.COLORS.BACKHOME,
        shadowOffset: { width: 0, height: 2 },
        shadowColor: AppConstants.SHADOWCOLORS.TEXT_SHADOW,
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,

    },
    userdetail: {
        width: AppConstants.getDeviceWidth(80),
        marginRight: AppConstants.getDeviceWidth(5)
    },
    projectdesc: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS11),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        marginLeft: AppConstants.getDeviceWidth(5),
        marginRight: AppConstants.getDeviceWidth(5)
    },
    bottomtextinputview: {
        width: AppConstants.getDeviceWidth(100),
        height: AppConstants.getDeviceHeight(6),
        position:'absolute',
        bottom:0
    },

    textboxText: {
        width: AppConstants.getDeviceWidth(62),
        height: AppConstants.getDeviceHeight(4),
        color: AppConstants.COLORS.BLACK,
        backgroundColor: AppConstants.COLORS.PROFILETEXINPUT,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS13),
        marginLeft: AppConstants.getDeviceWidth(7),
        marginRight: AppConstants.getDeviceWidth(7),
        paddingLeft: AppConstants.getDeviceWidth(2),
        justifyContent: 'center',
        padding: 0,
        margin: 0,
    },
    shorting1: {
        alignSelf: 'center',
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.BLACK,
        justifyContent: 'center'
    },
    shortlist1: {
        width: AppConstants.getDeviceWidth(25),
        height: AppConstants.getDeviceHeight(4),
        backgroundColor: AppConstants.COLORS.PROFILEINACTIVE,
        position: 'absolute',
        right: 0,
        marginRight: AppConstants.getDeviceWidth(5),
        justifyContent: 'center',

    },
   
}