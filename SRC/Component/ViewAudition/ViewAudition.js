import React, { Component } from 'react';
import {
    FlatList,
    TouchableOpacity,
    View,
    Text,
    StatusBar,
    ImageBackground,
    TextInput,
    Keyboard,
    ActivityIndicator,
    Alert
} from 'react-native';
import styles from './ViewAuditionStyle'
import * as AppConstants from '../Helper/AppConstants';
import OfflineNotice from '../Helper/OfflineNotice';
import Toast, { DURATION } from 'react-native-easy-toast';
import Icon from "react-native-vector-icons/MaterialIcons";
import Icons from "react-native-vector-icons/FontAwesome";
import { Avatar } from 'react-native-elements';
import API from "../WebServices/API";
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';

import { PostData, internet } from '../WebServices/webAPIRequest';
const Images = {
    Bio: 'bio',
}
export default class ViewAudition extends Component {
    constructor(props) {
        super(props);
        this.state = {
            projecttitle: '',
            Role: '',
            Comment: '',
            isLoading: false,
            dataSource: [],
            shorted: '',
            auditionVideo: '',
            thumbnail: '',
            audition_id: this.props.navigation.state.params.auditionid,
            auditionDetail: ''
        }
        this._renderRow = this._renderRow.bind(this);
        this.lists = [
            this.props.navigation.addListener('didFocus', () => { this.componentDidFocus() })
        ];
    }
    validator() {
        if (this.state.Comment == '') {
            this.refs.toast.show(AppConstants.Messages.COMMENT, DURATION.LENGTH_LONG);
        } else {
            this.Comment();
        }
    }
    componentDidFocus() {
        this.ViewDetail();
    }
    ViewDetail() {
        Keyboard.dismiss();
        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({
                audition_id: this.state.audition_id
            });
            PostData(API.ViewAudition, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false });
                    if (responseJson.statusCode == 1) {
                        this.setState({
                            projecttitle: responseJson.data.project_name,
                            Role: responseJson.data.role_name,
                            dataSource: responseJson.data.comments,
                            shorted: responseJson.data.shortlist_status,
                            auditionVideo: responseJson.data.audition_video,
                            thumbnail: responseJson.data.audition_video_thumbnail,
                            auditionDetail: responseJson.data.audition_detail,
                        });
                    }
                    else {
                        Alert.alert(responseJson.message);
                    }
                }).catch(() => {
                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }
    }
    Comment() {
        Keyboard.dismiss();
        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({
                audition_id: this.state.audition_id,
                comment: this.state.Comment
            });
            PostData(API.AuditionComment, APIData)
                .then((responseJson) => {
                    if (responseJson.statusCode == 1) {
                        this.setState({ Comment: '' });
                        this.ViewDetail();
                    }
                    else {
                        Alert.alert(responseJson.message);
                    }
                }).catch(() => {
                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }
    }
    Sortlist() {
        Keyboard.dismiss();
        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({
                audition_id: this.state.audition_id,
            });
            PostData(API.Shortlisted, APIData)
                .then((responseJson) => {
                    if (responseJson.statusCode == 1) {
                        this.refs.toast1.show(responseJson.message, DURATION.LENGTH_SHORT, true);
                        this.ViewDetail();
                    }
                    else {
                        Alert.alert(responseJson.message);
                    }
                }).catch(() => {
                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }
    }
    _renderRow(item, index, ) {
        var imageurl = item.item.profile_image == '' ? Images.Bio : item.item.profile_image;
        return (
            <View>
                <View style={styles.FlatlistContainer}>
                    <Avatar
                        size={AppConstants.getDeviceHeight(10)}
                        overlayContainerStyle={{ backgroundColor: 'black' }}
                        containerStyle={{ alignSelf: 'center', backgroundColor: null, marginLeft: AppConstants.getDeviceWidth(5), }}
                        rounded
                        source={{ uri: imageurl }} />
                    <View style={styles.userdetail}>
                        <Text style={[AppConstants.CommonStyles.Fontfamilandcolor16, styles.Comment]}>{item.item.name}</Text>
                        <Text style={styles.projectdesc}>{item.item.created}</Text>
                        <Text style={styles.projectdesc}>{item.item.comment}</Text>
                    </View>
                </View>

            </View>
        );
    }
    render() {
        return (
            <View style={styles.mainContainer}>
                <KeyboardAwareView doNotForceDismissKeyboardWhenLayoutChanges={true}>

                    <View style={styles.mainContainer}>
                        <StatusBar barStyle="light-content" backgroundCopr={AppConstants.SHADOWCOLORS.INTRO} animated translucent></StatusBar>
                        <OfflineNotice />
                        <View style={styles.mainContainer}>
                            <Toast
                                ref="toast"
                                style={[AppConstants.CommonStyles.ToastStyle1, { justifyContent: 'flex-end', height: AppConstants.getDeviceHeight(11), }]}
                                position="top"
                                positionValue={AppConstants.getDeviceHeight(0)}
                                fadeInDuration={750}
                                fadeOutDuration={1000}
                                textStyle={AppConstants.CommonStyles.ToastTextStyle}
                            />
                            <Toast
                                ref="toast1"
                                style={[AppConstants.CommonStyles.ToastStyle2, { height: AppConstants.getDeviceHeight(6.5), }]}
                                position='bottom'
                                positionValue={200}
                                fadeInDuration={1000}
                                fadeOutDuration={1000}
                                textStyle={AppConstants.CommonStyles.ToastTextStyle1} />
                            <Text style={[AppConstants.CommonStyles.Fontfamilandcolor16, styles.ongoingtitle]}>{this.state.projecttitle}</Text>
                            <TouchableOpacity onPress={() => { this.props.navigation.navigate('Videonew', { video: this.state.auditionVideo, thumbnail: this.state.thumbnail }) }} style={styles.videotouch}>
                                <ImageBackground onPress={() => { this.Sortlist() }} source={{ uri: this.state.thumbnail }} style={{ height: AppConstants.getDeviceHeight(25), width: AppConstants.getDeviceWidth(100), justifyContent: 'center' }}>
                                    <Icon name="play-circle-outline" color='#7B8F99' size={AppConstants.moderateScale(60)} style={{ alignSelf: 'center' }} />
                                </ImageBackground>
                            </TouchableOpacity>
                            <View style={styles.roleandshortlist}>
                                <Text style={styles.projectdate}>Role: {this.state.Role}</Text>
                                {this.state.shorted == 'Yes' ? <Text style={[AppConstants.CommonStyles.Fontfamilandcolor16, styles.shorted]}>Shortlisted</Text> :
                                    <TouchableOpacity onPress={() => { this.Sortlist() }} style={styles.shortlist}>
                                        <Icons name="star" color='black' size={AppConstants.moderateScale(15)} style={styles.star} />
                                        <Text style={styles.shorting}>Shortlist</Text>
                                    </TouchableOpacity>}

                            </View>
                            <Text numberOfLines={4} style={[AppConstants.CommonStyles.Fontfamilandcolor16, styles.Comment]}>Brief: {this.state.auditionDetail}</Text>
                            <Text style={[AppConstants.CommonStyles.Fontfamilandcolor16, styles.Comment]}>Comment</Text>

                            {this.state.dataSource == '' && this.state.isLoading == false ? <Text style={{ alignSelf: 'center', marginTop: AppConstants.getDeviceHeight(20) }}> No Comments yet!</Text> : <FlatList
                                style={{ marginBottom: AppConstants.getDeviceHeight(1) }}
                                scrollEnabled={true}
                                data={this.state.dataSource}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={this._renderRow.bind(this)} />}

                            <View style={styles.bottomtextinputview}>
                                <TextInput style={styles.textboxText}
                                    placeholder="Write a feedback comment"
                                    placeholderTextColor={AppConstants.COLORS.PLACEHOLDARLOGIN}
                                    underlineColorAndroid="transparent"                                        // underlineColorAndroid="transparent"
                                    returnKeyType={"done"}
                                    value={this.state.Comment}
                                    autoCapitalize="none"

                                    onChangeText={(Comment) => this.setState({ Comment })} />
                                <TouchableOpacity onPress={() => { this.validator() }} style={styles.shortlist1}>
                                    <Text style={styles.shorting1}>Submit</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </KeyboardAwareView>
                {this.state.isLoading ? <View style={AppConstants.CommonStyles.spinner}>
                    <ActivityIndicator size='large' ></ActivityIndicator>
                </View> : null}
            </View>
        )
    }
}