import React, { Component } from 'react';
import {
    TextInput,
    Image,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    Platform,
    Keyboard,
    Alert,
    ActivityIndicator,
} from 'react-native';
import styles from './OtpStyle'
import * as AppConstants from '../Helper/AppConstants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import OfflineNotice from '../Helper/OfflineNotice';
import Toast, { DURATION } from 'react-native-easy-toast'
import { PostData, internet } from '../WebServices/webAPIRequest';
import Iconss from "react-native-vector-icons/MaterialCommunityIcons";
import API from "../WebServices/API";
import AsyncStorage from '@react-native-community/async-storage';

const Images = {
    Loginlogo: 'loginlogo',
}
export default class Otp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            verifyotp: '',
            verifyotp1: false,
            matchotp: global.otp,
            matchotp1: false,
            isLoading: false
        };
    }
    validator() {
        if (this.state.verifyotp == '') {
            this.setState({ verifyotp1: true })
            // this.refs.toast.show(AppConstants.Messages.NOOTP, DURATION.LENGTH_LONG);
        }
        else if (this.state.matchotp != this.state.verifyotp) {
            this.setState({ matchotp1: true })
            // this.refs.toast.show(AppConstants.Messages.OTPMTACH, DURATION.LENGTH_LONG);
        }
        else {
            this.WSResendOTP('verified');
        }
    }
    WSResendOTP(value) {
        Keyboard.dismiss();
        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({
                user_id: global.userid,
                action: value,
            });
            PostData(API.userVerifyotp, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false });
                    if (responseJson.statusCode == 1) {
                        if (value == 'resend_otp') {
                            this.setState({
                                matchotp: responseJson.otp
                            });
                            Alert.alert(responseJson.message);
                        }
                        else {
                            global.firstname = responseJson.user_data.first_name
                            global.lastname = responseJson.user_data.last_name
                            global.email = responseJson.user_data.email_address
                            global.phone = responseJson.user_data.phone_no
                            global.token = responseJson.token
                            AsyncStorage.setItem('director', JSON.stringify({
                                userId: responseJson.userId,
                                authToken: responseJson.token,
                                firstname: responseJson.user_data.first_name,
                                lastname: responseJson.user_data.last_name,
                                email: responseJson.user_data.email_address,
                                phone: responseJson.user_data.phone_no,
                                profilepik: '',
                            }));
                            AsyncStorage.setItem('profilestatus', JSON.stringify({ profilestatus: responseJson.profile_complete_status }));
                            this.setState({ verifyotp: '' });
                            this.props.navigation.replace(AppConstants.SCREENS.DASHBOARD);
                        }
                    }
                    else {
                        Alert.alert(responseJson.message);
                    }

                }).catch((error) => {
                    
                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }

    }
    render() {
        return (
            <KeyboardAwareView doNotForceDismissKeyboardWhenLayoutChanges={true}>
                <StatusBar barStyle="light-content" backgroundCopr={AppConstants.SHADOWCOLORS.INTRO} animated translucent></StatusBar>
                <OfflineNotice />
                <View style={styles.mainContainer}>
                    <Toast
                        ref="toast"
                        style={[AppConstants.CommonStyles.ToastStyle, { justifyContent: 'flex-end', height: AppConstants.getDeviceHeight(8), }]}
                        position="top"
                        positionValue={AppConstants.getDeviceHeight(0)}
                        fadeInDuration={750}
                        fadeOutDuration={1000}
                        textStyle={AppConstants.CommonStyles.ToastTextStyle} />
                    <View>
                        <Iconss
                            onPress={() => this.props.navigation.goBack()}
                            name="arrow-left"
                            style={{ marginLeft: AppConstants.getDeviceWidth(4), marginTop: AppConstants.getDeviceHeight(this.props.navigation.getParam('header') != null && !this.props.navigation.getParam('header') ? 6.8 : Platform.OS === 'ios' ? 0 : 3) }}
                            size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS25)}
                            color={AppConstants.COLORS.WHITE} />
                    </View>
                    <View style={styles.logoContainer}>
                        <Image style={styles.lginlogo} source={{ uri: Images.Loginlogo }} />
                    </View>
                    <View style={styles.TextContainer}>
                        <Text style={styles.Text}>
                            Sending OTP to your registered Email Id and Phone Number
                                </Text>
                    </View>
                    <View style={styles.textboxContainer}>
                        <TextInput style={styles.textboxText}
                            placeholder="Enter OTP"
                            placeholderTextColor={AppConstants.COLORS.PLACEHOLDARLOGIN}
                            underlineColorAndroid="transparent"
                            returnKeyType={"done"}
                            keyboardType={"phone-pad"}
                            value={this.state.verifyotp}
                            blurOnSubmit={false}
                            autoCapitalize="none"
                            maxLength={20}
                            onSubmitEditing={() => {
                                Keyboard.dismiss();
                            }}
                            onChangeText={(OTP) => this.setState({ verifyotp: OTP, matchotp1: false, verifyotp1: false })} />
                        <View style={{
                            width: AppConstants.getDeviceWidth(87),
                            height: AppConstants.getDeviceHeight(0.1),
                            backgroundColor: AppConstants.COLORS.LANGAUGE,
                        }}></View>
                        {this.state.verifyotp1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(2), marginTop: AppConstants.getDeviceHeight(1) }]}>{AppConstants.Messages.NOOTP}</Text> : null}
                        {this.state.matchotp1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(2), marginTop: AppConstants.getDeviceHeight(1) }]}>{AppConstants.Messages.OTPMTACH}</Text> : null}

                    </View>
                    <View style={styles.staticTextContainer}>
                        <Text style={styles.staticText}>Didn’t get your OTP?</Text>
                    </View>

                    <View style={styles.btnReSendContainer}>
                        <TouchableOpacity style={styles.btnReSend} onPress={() => this.WSResendOTP('resend_otp')} >
                            <Text style={styles.txtReSend}>Re-Send</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ height: AppConstants.getDeviceHeight(15), marginTop: AppConstants.getDeviceHeight(2) }}>
                        <TouchableOpacity style={{
                            height: AppConstants.getDeviceHeight(15), marginTop: AppConstants.getDeviceHeight(2), marginLeft: AppConstants.getDeviceWidth(5),
                            marginRight: AppConstants.getDeviceWidth(5),
                        }} onPress={() => { this.validator() }}>
                            <Text style={styles.logintext}>
                                Veriify OTP
                                    </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                {this.state.isLoading ? <View style={AppConstants.CommonStyles.spinner}>
                    <ActivityIndicator size='large' color={AppConstants.COLORS.SIGNUP}></ActivityIndicator>
                </View> : null}
            </KeyboardAwareView>
        )
    }
}