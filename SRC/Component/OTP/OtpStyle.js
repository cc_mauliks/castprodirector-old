import * as AppConstants from '../Helper/AppConstants';
export default {
    mainContainer: {
        flex: 1,
        backgroundColor:AppConstants.COLORS.PROFILEBACKGROUND 
    },
    backgroundImage: {
        flex: 1,
    },
    logoContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: AppConstants.getDeviceHeight(3),
    },
    lginlogo: {
        aspectRatio: 1,
        height: AppConstants.getDeviceHeight(20),
        width: AppConstants.getDeviceHeight(20),
    },
    textboxContainer: {
        marginTop: AppConstants.getDeviceHeight(4),
        width: AppConstants.getDeviceWidth(90),
        height: AppConstants.getDeviceHeight(5),
        marginLeft: AppConstants.getDeviceWidth(5),
        marginRight: AppConstants.getDeviceWidth(5),
        alignItems: 'center',
    },
    textboxText: {
        width: AppConstants.getDeviceWidth(90),
        height: AppConstants.getDeviceHeight(6.6),
        color: AppConstants.COLORS.WHITE,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS13),
        paddingLeft: AppConstants.getDeviceWidth(3.53),
    },
    staticTextContainer: {
        marginTop: AppConstants.getDeviceHeight(5),
        alignItems: 'center',
        justifyContent: 'center',
    },
    staticText: {
        color: AppConstants.COLORS.WHITE,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.TEXTVIEWTEXT),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_1,
        textAlign: 'center'
    },
    txtReSend: {
        color: AppConstants.COLORS.WHITE,
        fontFamily: AppConstants.FONTFAMILY.fontFamily_1,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS15),
        textAlign: 'center',
    },
    btnReSendContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    btnReSend: {
        width: AppConstants.getDeviceWidth(35.73),
        height: AppConstants.getDeviceHeight(4),
        borderRadius: AppConstants.getDeviceWidth(AppConstants.BORDERRADIUS.BUTTON_BORDERRADIUS),
        backgroundColor: AppConstants.COLORS.REDIUS_BUTTON,
        alignItems: 'center',
        justifyContent: 'center',
    },
    TextContainer: {
        marginTop: AppConstants.getDeviceHeight(5.25),
        alignItems: 'center',
        justifyContent: 'center',
    },
    Text: {
        width: AppConstants.getDeviceWidth(85),
        color: AppConstants.COLORS.WHITE,
        fontFamily: AppConstants.FONTFAMILY.fontFamily_1,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS15),
        textAlign: 'center',
    },
    logintext: {
        color: AppConstants.COLORS.PROFILEINACTIVE,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontWeight: '600',
        alignSelf: 'center',
        marginTop: AppConstants.getDeviceHeight(1),
        marginBottom: AppConstants.getDeviceHeight(10)
    },
}