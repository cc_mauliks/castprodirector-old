import React, { Component } from 'react';
import {
    Image,
    TextInput,
    View,
    Text,
    StatusBar,
    Keyboard,
    TouchableOpacity,
    ActivityIndicator,
    ScrollView,
    Platform,
    Alert
} from 'react-native';
import styles from './CreatroleStyle'
import * as AppConstants from '../Helper/AppConstants';
import OfflineNotice from '../Helper/OfflineNotice';
import Toast, { DURATION } from 'react-native-easy-toast'
import ImagePicker from "react-native-image-picker";
import { RNS3 } from 'react-native-s3-upload';
import API from "../WebServices/API";
import { PostData, internet } from '../WebServices/webAPIRequest';
import TagInputskill from "../Taginputskill";
import ChatModal from "./Chatmodal";
import DatePicker from 'react-native-datepicker';
import DocumentPicker from 'react-native-document-picker';
import Icon from "react-native-vector-icons/MaterialIcons";
import RNThumbnail from 'react-native-thumbnail';
import RNFS from 'react-native-fs';
import Icons from "react-native-vector-icons/Entypo";
import Iconss from "react-native-vector-icons/MaterialCommunityIcons";
import Iconplay from "react-native-vector-icons/FontAwesome";
import Modal1 from "react-native-modal";
import Close from "react-native-vector-icons/MaterialCommunityIcons";
import CustomMultiPicker from "../../../Slectlist/multipleSelect";
import Permissions from "react-native-permissions";


const inputProps = {
    keyboardType: "default",
    autoFocus: true,
    style: {
        fontSize: 14,
        marginVertical: Platform.OS == "ios" ? 10 : -2
    }
};

export default class Directorprofile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            Videourl: '',
            Audiourl: '',
            Scripturl: '',
            Videourl1: false,
            Audiourl1: false,
            Scripturl1: false,
            startdate1: false,
            enddate1: false,
            Role1: false,
            tagsskill11: false,
            Description1: false,
            modal1: 0,
            tagsskill1: [],
            startdate: '',
            enddate: '',
            Role: '',
            Description: '',
            video_thumbnail: '',
            dataSource: [],
            Manuallyrecording: '',
            Audioname: '',
            videoname: '',
            Scriptname: ''

        };
        this.lists = [
            this.props.navigation.addListener('didFocus', () => { this.componentDidFocus() })
        ];
    }
    componentDidFocus() {
        this.setState({ Audiourl: global.recording, Audioname: global.recordingname });
    }
    labelExtractor1 = (tagsskill1) => tagsskill1;
    takeaudioandfile(value) {
        try {
            DocumentPicker.pick({
                // type: [DocumentPicker.types.],
            }).then((res) => {
                this.setState({ isLoading: true });
                if (value == 'mp3') {
                    if (res.type == 'audio/mpeg') {
                        this.UploadauioScript(value, res.uri, res.name)
                    }
                    else {
                        this.setState({ isLoading: false, Audiourl1: true });
                    }
                }
                else {
                    if (res.type == 'audio/mpeg') {
                        this.setState({ isLoading: false, Scripturl1: true });
                    }
                    else if (res.type == 'video/mp3') {
                        this.setState({ isLoading: false, Scripturl1: true });
                    }
                    else if (res.type == 'image/jpeg') {
                        this.setState({ isLoading: false, Scripturl1: true });
                    }
                    else if (res.type == 'video/mp4') {
                        this.setState({ isLoading: false, Scripturl1: true });
                    }
                    else {
                        this.UploadauioScript(value, res.uri, res.name)
                    }
                }

            });

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
            } else {
                throw err;
            }
        }


    }
    componentDidMount() {
        this.Getskill();
    }
    Getskill() {
        // if (global.isConnected) {
        this.setState({ isLoading: true });

        APIData = JSON.stringify({

        });
        PostData(API.GetSkill, APIData)
            .then((responseJson) => {
                this.setState({ isLoading: false, skilll: [] });
                var arr = [];
                responseJson.data.map((data, index) => {
                    arr.push(
                        data.skill,
                    )
                });
                this.setState({ skilll: arr });
            })
            .catch(() => {
                this.setState({ isLoading: false });
                Alert.alert(AppConstants.Messages.APIERROR);
            });
    }
    UploadauioScript(value, uri, name) {
        const file = {
            uri: uri,
            name: name,
            type: value == 'mp3' ? "audio/mpeg" : "application/pdf"
        }
        const options = {
            keyPrefix: "uploads/",
            bucket: "castpro",
            region: "us-east-2",
            accessKey: "AKIAVSA77EO2WHI4DT7L",
            secretKey: "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",
            successActionStatus: 201,

        }

        this.setState({ video: '' });
        RNS3.put(file, options).then(response => {
            this.setState({ isLoading: false });
            if (response.status !== 201) {

                Alert.alert("Failed to upload image to S3");
            }
            else {
                if (value == 'mp3') {
                    this.setState({
                        Audiourl: response.body.postResponse.location, Audioname: response.body.postResponse.etag, Audiourl1: false
                    });

                }
                else {
                    this.setState({ Scripturl: response.body.postResponse.location, Scriptname: response.body.postResponse.etag, Scripturl1: false })
                }


            }
        });
    }
    resetState1(valueenter) {
        valueenter.map((item) => {
            const value = this.state.tagsskill1.findIndex(obj => obj === item);
            if (value == -1) {
                this.setState({
                    tagsskill1: [...this.state.tagsskill1, item],
                });
            }
        });
        this.setState({ modal1: 0 });
    }
    reset1() {
        this.setState({ modal1: 0 });
    }
    onChangeTags1 = (tagsskill1) => {
        this.setState({ tagsskill1 });
    }
    takePhoto() {
        const options = {
            quality: 0.3,
            mediaType: 'video',
            cameraType: 'front',
            maxWidth: 1024,
            maxHeight: 1024,
            videoQuality: 'small',
            durationLimit: 0,
            allowsEditing: true,
            storageOptions: {
                cameraRoll: true,
                waitUntilSaved: true,
                skipBackup: true,
                path: 'mixed',

            },
        };
        ImagePicker.launchImageLibrary(options, response => {
            if (response.didCancel) {
            } else if (response.error) {
                Permissions.check("camera").then(response => {
                    if (response == "denied") {
                        Permissions.request("camera").then(response => {
                            if (response == "denied") {
                                Alert.alert(
                                    "Please allow Permissions to access Camera from Settings -> Apps -> CastPro -> Camera"
                                );
                            }
                        });
                    }
                });
            } else if (response.customButton) {
            } else {
                this.setState({ isLoading: true });
                const file = {
                    uri: response.uri,
                    name: response.fileName,
                    type: "video/mp4"
                }
                const options = {
                    keyPrefix: "uploads/",
                    bucket: "castpro",
                    region: "us-east-2",
                    accessKey: "AKIAVSA77EO2WHI4DT7L",
                    secretKey: "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",
                    successActionStatus: 201,
                }
                this.setState({ video: '' });
                RNS3.put(file, options).then(response => {
                    this.setState({ isLoading: false });
                    if (response.status !== 201) {
                        Alert.alert("Failed to upload image to S3");
                    }
                    else {
                        this.setState({
                            Videourl: response.body.postResponse.location, videoname: response.body.postResponse.etag, Videourl1: false
                        });
                    }
                });
                RNThumbnail.get(Platform.OS == 'ios' ? response.uri : response.path).then((result) => {
                    RNFS.readFile(Platform.OS == 'ios' ? 'file:///' + result.path : result.path, 'base64')
                        .then(res => {
                            this.setState({
                                thumbnail: Platform.OS == 'ios' ? 'file:///' + result.path : result.path,
                                width: result.width,
                                height: result.height, video_thumbnail: res
                            });

                        });


                }).catch(() => {
                })
            }
        });
    }
    validator() {
        if (this.state.Role == '') {
            this.setState({ Role1: true })
        }
        else if (this.state.tagsskill1 == '') {
            this.setState({ tagsskill11: true })
        }
        else if (this.state.Description == '') {
            this.setState({ Description1: true })
        }
        else if (this.state.startdate == '') {
            this.setState({ startdate1: true })
        }
        else if (this.state.enddate == '') {
            this.setState({ enddate1: true })
        }
        else {
            this.CreateRole();
        }
    }
    CreateRole() {
        Keyboard.dismiss();

        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({
                project_id: this.props.navigation.state.params.projectid,
                character_name: this.state.Role,
                character_brief: this.state.Description,
                start_date: this.state.startdate,
                end_date: this.state.enddate,
                audio_brief: this.state.Audiourl,
                video_brief: this.state.Videourl,
                role_skill: this.state.tagsskill1,
                role_script: this.state.Scripturl,
                video_thumbnail: this.state.video_thumbnail,
                role_video_file_name: this.state.videoname,
                role_audio_file_name: this.state.Audioname,
                role_script_file_name: this.state.Scriptname,
            });
            PostData(API.CreateRole, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false });
                    if (responseJson.statusCode == 1) {
                        this.setState({ Audiourl: '', Audioname: '' });
                        global.recording = '';
                        global.recordingname = '';
                        this.refs.toast1.show(responseJson.message, DURATION.LENGTH_SHORT, true);
                        this.timeoutHandle = setTimeout(() => {
                            this.props.navigation.goBack();
                        }, 2000);
                    }
                    else {
                        Alert.alert(responseJson.message);
                    }
                }).catch((error) => {
                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }
    }
    render() {
        return (
            <View style={styles.mainContainer}>
                <StatusBar barStyle="light-content" backgroundCopr={AppConstants.SHADOWCOLORS.INTRO} animated translucent></StatusBar>
                <OfflineNotice />
                <View style={styles.mainContainer}>
                    <Toast
                        ref="toast"
                        style={[AppConstants.CommonStyles.ToastStyle1, { justifyContent: 'flex-end', height: AppConstants.getDeviceHeight(11), }]}
                        position="top"
                        positionValue={AppConstants.getDeviceHeight(0)}
                        fadeInDuration={750}
                        fadeOutDuration={1000}
                        textStyle={AppConstants.CommonStyles.ToastTextStyle}
                    />
                    <Toast
                        ref="toast1"
                        style={[AppConstants.CommonStyles.ToastStyle2, { height: AppConstants.getDeviceHeight(6.5), }]}
                        position='bottom'
                        positionValue={200}
                        fadeInDuration={1000}
                        fadeOutDuration={1000}
                        textStyle={AppConstants.CommonStyles.ToastTextStyle1}
                    />
                    <ScrollView contentContainerStyle={{ flexGrow: 1, }} style={{ marginBottom: AppConstants.getDeviceHeight(1) }}>
                        <Text style={styles.projecttitle1}>{this.props.navigation.state.params.projectname}</Text>
                        <Text style={[styles.projectdesc]}>Role </Text>
                        <TextInput style={styles.textboxText1}
                            placeholderTextColor={AppConstants.COLORS.PLACEHOLDAR}
                            underlineColorAndroid="transparent"
                            value={this.state.Role}
                            blurOnSubmit={false}
                            placeholder={'Enter Role Title'}
                            multiline={true}
                            autoCapitalize="none"
                            onChangeText={(Role) => this.setState({ Role, Role1: false })} />
                        {this.state.Role1 == true ? <Text style={AppConstants.CommonStyles.errorcolor}>{AppConstants.Messages.ENTERROLE}</Text> : null}

                        <Text style={[styles.projectdesc]}>Add Skill </Text>
                        <TouchableOpacity onPress={() => this.setState({ modal1: '1' })} style={styles.textboxText1}>
                            <Text style={{ color: AppConstants.COLORS.PLACEHOLDAR }}>Select Skill </Text>
                        </TouchableOpacity>
                        <TagInputskill
                            autoFocus={true}
                            value={this.state.tagsskill1}
                            onChange={this.onChangeTags1}
                            labelExtractor={this.labelExtractor1}
                            tagColor="transparent"
                            scrollEnabled={true}
                            tagTextColor="black"
                            inputProps={inputProps} />
                        {this.state.tagsskill11 == true ? <Text style={AppConstants.CommonStyles.errorcolor}>{AppConstants.Messages.SKILL}</Text> : null}

                        <Text style={[styles.projectdesc]}>Description </Text>
                        <TextInput style={styles.textmultiline}
                            placeholderTextColor={AppConstants.COLORS.PLACEHOLDAR}
                            underlineColorAndroid="transparent"
                            value={this.state.Description}
                            blurOnSubmit={false}
                            placeholder={'Enter Short Description'}
                            multiline={true}
                            autoCapitalize="none"
                            onChangeText={(Description) => this.setState({ Description, Description1: false })} />

                        {this.state.Description1 == true ? <Text style={AppConstants.CommonStyles.errorcolor}>{AppConstants.Messages.DESCRIPTION}</Text> : null}

                        <Text style={styles.projectdesc}>Start Date</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <DatePicker
                                style={styles.datepicker}
                                date={this.state.startdate}
                                mode="date"
                                placeholder="DD/MM/YYYY"
                                format="DD-MM-YYYY"

                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                showIcon={true}
                                iconComponent={<View style={{ flexDirection: 'row', justifyContent: 'center', height: AppConstants.getDeviceHeight(5), }}>
                                    <View style={[styles.line, { marginTop: Platform.isPad == true ? AppConstants.getDeviceHeight(2) : 0 }]}></View>
                                    <View style={{
                                        width: AppConstants.getDeviceWidth(10),
                                        height: AppConstants.getDeviceHeight(5),
                                        backgroundColor: AppConstants.COLORS.BACKHOME,
                                        marginTop: Platform.isPad == true ? AppConstants.getDeviceHeight(2) : 0,
                                        justifyContent: 'center',
                                    }}>
                                        <Icon name="date-range" color={AppConstants.COLORS.datecolor} style={{ marginLeft: AppConstants.getDeviceWidth(1), alignItems: 'center', marginTop: AppConstants.moderateScale(1) }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS28)} />

                                    </View>
                                </View>
                                }
                                customStyles={{
                                    dateIcon: {
                                        position: 'absolute',
                                        left: 0,

                                        marginLeft: 0
                                    },
                                    dateInput: {
                                        borderLeftWidth: 0,
                                        borderRightWidth: 0,
                                        borderTopWidth: 0,
                                        borderBottomWidth: 0,
                                    },
                                    borderBottomColor: AppConstants.COLORS.WHITE,
                                    dateIcon: null,
                                    dateText: styles.datepickerdate,
                                    placeholderText: styles.projectdate
                                }}
                                onDateChange={(date) => {
                                    this.setState({ startdate: date, startdate1: false })
                                    // this.getAge(date);
                                }} />

                        </View>
                        {this.state.startdate1 == true ? <Text style={AppConstants.CommonStyles.errorcolor}>{AppConstants.Messages.STARTDATE}</Text> : null}

                        <Text style={styles.projectdesc}>End Date</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <DatePicker
                                style={styles.datepicker}
                                date={this.state.enddate}
                                mode="date"
                                placeholder="DD/MM/YYYY"
                                format="DD-MM-YYYY"

                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                showIcon={true}
                                iconComponent={<View style={{ flexDirection: 'row', justifyContent: 'center' , height: AppConstants.getDeviceHeight(5),}}>
                                    <View style={[styles.line, { marginTop: Platform.isPad == true ? AppConstants.getDeviceHeight(2) :0 }]}></View>
                                    <View style={{
                                        width: AppConstants.getDeviceWidth(10),
                                        height: AppConstants.getDeviceHeight(5),
                                        backgroundColor: AppConstants.COLORS.BACKHOME,
                                        marginTop: Platform.isPad == true ? AppConstants.getDeviceHeight(2) : 0,
                                        justifyContent: 'center',

                                    }}>
                                        <Icon name="date-range" color={AppConstants.COLORS.datecolor} style={{ marginLeft: AppConstants.getDeviceWidth(1), alignItems: 'center', marginTop: AppConstants.moderateScale(1) }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS28)} />

                                    </View>
                                </View>
                                }
                                customStyles={{
                                    dateIcon: {
                                        position: 'absolute',
                                        left: 0,

                                        marginLeft: 0
                                    },
                                    dateInput: {
                                        borderLeftWidth: 0,
                                        borderRightWidth: 0,
                                        borderTopWidth: 0,
                                        borderBottomWidth: 0,
                                    },
                                    borderBottomColor: AppConstants.COLORS.WHITE,
                                    dateIcon: null,
                                    dateText: styles.datepickerdate,
                                    placeholderText: styles.projectdate
                                }}
                                onDateChange={(date) => {
                                    this.setState({ enddate: date, enddate1: false })
                                }} />

                        </View>
                        {this.state.enddate1 == true ? <Text style={AppConstants.CommonStyles.errorcolor}>{AppConstants.Messages.ENDDATE}</Text> : null}

                        <View style={styles.lin1}></View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[styles.projectdesc, { marginTop: AppConstants.getDeviceHeight(2) }]}>Upload Video </Text>
                            <Icons name="video" color={AppConstants.COLORS.PROFILEBACKGROUND} style={{ alignSelf: 'center', marginTop: AppConstants.getDeviceHeight(2) }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS20)} />
                        </View>
                        <View style={{ flexDirection: 'row', }}>

                            {this.state.Videourl == '' ? <TouchableOpacity onPress={() => {
                                Permissions.request("photo").then(response => {

                                    if (response == "denied") {
                                        Alert.alert(
                                            "Please allow Permissions to access Camera from Settings -> Apps -> Castpro -> Camera"
                                        );
                                    }
                                    else {
                                        this.takePhoto();
                                    }
                                });

                            }} style={styles.submit}>
                                <Text style={[styles.projectdesc1, { alignSelf: 'center' }]}>Upload </Text>
                            </TouchableOpacity> :
                                <View style={{ flexDirection: 'row', marginTop: AppConstants.getDeviceHeight(1), marginBottom: AppConstants.getDeviceHeight(2), }}>
                                    <Text style={[styles.projectdesc2, { alignSelf: 'center' }]}>Uploaded successfully</Text>
                                    <Iconss onPress={() => { this.setState({ Videourl: '' }) }} name="delete" color={AppConstants.COLORS.PROFILEBACKGROUND}
                                        style={{ alignSelf: 'center', marginLeft: AppConstants.getDeviceWidth(2) }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS22)} />
                                </View>}
                        </View>
                        {this.state.Videourl1 == true ? <Text style={AppConstants.CommonStyles.errorcolor}>{AppConstants.Messages.PROJECTTITLE}</Text> : null}

                        <View style={styles.lin2}></View>

                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[styles.projectdesc]}>Upload Audio </Text>
                            <Icon name="audiotrack" color={AppConstants.COLORS.PROFILEBACKGROUND} style={{ alignSelf: 'center', marginTop: AppConstants.getDeviceHeight(2) }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS20)} />

                        </View>

                        <View style={{ flexDirection: 'row', }}>

                            {this.state.Audiourl == '' ? <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity onPress={() => {

                                    this.takeaudioandfile('mp3')

                                }} style={styles.submit}>
                                    <Text style={[styles.projectdesc1]}>Upload </Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => {
                                    global.recording = '';
                                    this.props.navigation.navigate(AppConstants.SCREENS.AUDIORECROD)
                                }} style={{ width: AppConstants.getDeviceHeight(4), height: AppConstants.getDeviceHeight(4), alignSelf: 'center', marginLeft: AppConstants.getDeviceWidth(5) }}>
                                    <Iconplay name="microphone" color={AppConstants.COLORS.datecolor} style={{ marginLeft: AppConstants.getDeviceWidth(1), alignItems: 'center', marginTop: AppConstants.moderateScale(1) }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS25)} />

                                </TouchableOpacity>
                            </View> :
                                <View style={{ flexDirection: 'row', marginTop: AppConstants.getDeviceHeight(1), marginBottom: AppConstants.getDeviceHeight(2), }}>
                                    <Text style={[styles.projectdesc2, { alignSelf: 'center' }]}>Uploaded successfully</Text>
                                    <Iconss onPress={() => { this.setState({ Audiourl: '' }) }} name="delete" color={AppConstants.COLORS.PROFILEBACKGROUND}
                                        style={{ alignSelf: 'center', marginLeft: AppConstants.getDeviceWidth(2) }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS22)} />
                                </View>}
                        </View>
                        {this.state.Audiourl1 == true ? <Text style={AppConstants.CommonStyles.errorcolor}>{AppConstants.Messages.AUDIOFILE}</Text> : null}

                        <View style={styles.lin2}></View>

                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[styles.projectdesc]}>Upload Script </Text>
                            <Icon name="description" color={AppConstants.COLORS.PROFILEBACKGROUND} style={{ alignSelf: 'center', marginTop: AppConstants.getDeviceHeight(2) }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS20)} />
                        </View>
                        <View style={{ flexDirection: 'row', }}>

                            {this.state.Scripturl == '' ? <TouchableOpacity onPress={() => { this.takeaudioandfile('file') }} style={styles.submit}>
                                <Text style={[styles.projectdesc1, { alignSelf: 'center' }]}>Upload </Text>
                            </TouchableOpacity> :
                                <View style={{ flexDirection: 'row', marginTop: AppConstants.getDeviceHeight(1), marginBottom: AppConstants.getDeviceHeight(2), }}>
                                    <Text style={[styles.projectdesc2, { alignSelf: 'center' }]}>Uploaded successfully</Text>
                                    <Iconss onPress={() => { this.setState({ Scripturl: '' }) }} name="delete" color={AppConstants.COLORS.PROFILEBACKGROUND}
                                        style={{ alignSelf: 'center', marginLeft: AppConstants.getDeviceWidth(2) }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS22)} />
                                </View>}
                        </View>
                        {this.state.Scripturl1 == true ? <Text style={AppConstants.CommonStyles.errorcolor}>{AppConstants.Messages.SCRIPT}</Text> : null}

                        <View style={styles.lin2}></View>

                        <View style={styles.button1}>
                            <TouchableOpacity style={styles.savetouchable} onPress={() => { this.validator() }}>
                                <Text style={styles.Signup}>
                                    CREATE
                            </Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>

                {/* {this.state.modal1 == 0 ?
                    <View></View>
                    :
                    <View style={styles.modalRenderView}>
                        <ChatModal
                            func={this.resetState1.bind(this)}
                            func1={this.reset1.bind(this)}
                            pass={this.state.tagsskill1}
                            navigation={this.props.navigation}
                        />
                    </View>} */}

                {this.state.modal1 == 0 ?
                    <View></View>
                    : <Modal1
                        isVisible={this.state.modal1 == 0 ? false : true}
                        deviceHeight={AppConstants.getDeviceHeight(100)}
                        deviceWidth={AppConstants.getDeviceWidth(100)}>
                        <View>
                            <View style={styles.modal}>
                                <View style={{ height: AppConstants.getDeviceHeight(4), justifyContent: 'center', }}>
                                    <View style={styles.sentat}>
                                        <Text style={styles.text}>Skill List</Text>
                                        <Close
                                            name='close'
                                            style={{
                                                position: 'absolute',
                                                alignSelf: 'flex-end'
                                            }}
                                            color={AppConstants.COLORS.HEDARBACKGROUND}
                                            onPress={() => {
                                                this.setState({ modal1: 0 });
                                            }}
                                            size={AppConstants.FONTSIZE.FS22}
                                        />
                                    </View>
                                </View>

                                <CustomMultiPicker
                                    options={this.state.skilll}
                                    search={true} // should show search bar?
                                    multiple={true} //
                                    placeholder={"Search"}
                                    placeholderTextColor={'#757575'}
                                    returnValue={"label"} // label or value
                                    callback={(res) => { this.setState({ dataSource: res }); }} // callback, array of selected items
                                    rowBackgroundColor={"#eee"}
                                    rowHeight={40}
                                    rowRadius={5}
                                    iconColor={AppConstants.COLORS.HEDARBACKGROUND}
                                    iconSize={27}

                                    selectedIconName={"ios-checkmark-circle-outline"}
                                    unselectedIconName={"ios-radio-button-off"}
                                    scrollViewHeight={AppConstants.getDeviceHeight(58)}
                                    icon={0}
                                    selected={this.state.tagsskill1}
                                />



                            </View>

                            <TouchableOpacity style={{ borderBottomRightRadius: AppConstants.getDeviceWidth(2), borderBottomLeftRadius: AppConstants.getDeviceWidth(2), height: AppConstants.getDeviceHeight(5), width: AppConstants.getDeviceWidth(90), backgroundColor: AppConstants.COLORS.PROFILEINACTIVE, justifyContent: 'center', }}
                                onPress={() => {

                                    if (this.state.dataSource == '') {

                                    }
                                    else {
                                        var array = [...this.state.dataSource];
                                        this.setState({
                                            tagsskill1: array,
                                            tagsskill11: false
                                        });
                                        this.setState({ modal1: 0 });

                                        this.setState({ open: false })
                                    }

                                }}>
                                <Text style={{
                                    fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
                                    fontWeight: '600',
                                    alignSelf: 'center',
                                    color: AppConstants.COLORS.PROFILEBACKGROUND,
                                }}>
                                    DONE
                                    </Text>
                            </TouchableOpacity>
                        </View>
                    </Modal1>}
                {this.state.isLoading ? <View style={AppConstants.CommonStyles.spinner}>
                    <ActivityIndicator size='large' ></ActivityIndicator>
                </View> : null}
            </View>
        )
    }
}