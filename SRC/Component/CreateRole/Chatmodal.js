import React, { Component } from 'react';
import { View, Text, ListView, TouchableOpacity, ActivityIndicator, Image, FlatList } from 'react-native';
import styles from './ChatmodalStyle';
import Modal from "react-native-simple-modal";
import * as AppConstants from '../Helper/AppConstants';
import Toast, { DURATION } from 'react-native-easy-toast';
import Close from "react-native-vector-icons/MaterialCommunityIcons";

import { PostData } from "../WebServices/webAPIRequest";
import API from "../WebServices/API";
import CustomMultiPicker from "../../../Slectlist/multipleSelect";



export default class Chatmodal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            TextInputValueHolder: '',
            value: 0,
            open: false,
            dataSource: '',
            skilll: [],
            isLoading: false,
            selected: this.props.pass,
        }
    }
    componentDidMount() {
        this.Getskill();
        this.setState({ open: true });
    }
    componentWillUnmount() {

        this.openModal();
        this.closeModal();
    }
    Getskill() {
        // if (global.isConnected) {
        this.setState({ isLoading: true });

        APIData = JSON.stringify({

        });
        PostData(API.GetSkill, APIData)
            .then((responseJson) => {
                this.setState({ isLoading: false ,skilll:[]});
                var arr = [];
                responseJson.data.map((data, index) => {
                    arr.push(
                       data.skill,
                    )                  
                });
                this.setState({ skilll: arr });
            })
            .catch(() => {
                this.setState({ isLoading: false });
                this.refs.toast.show(AppConstants.Messages.APIERROR, DURATION.LENGTH_LONG);
            });
    }


    modalDidOpen = () => { };
    modalDidClose = () => {

        this.props.func(this.state.dataSource);
        this.setState({ open: false });
    };
    openModal = () => this.setState({ open: true });
    closeModal = () => this.setState({ open: false });
    render() {
        const { navigate } = this.props.navigation;

        return (
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                <Toast
                    ref="toast"
                    style={[AppConstants.CommonStyles.ToastStyle, { justifyContent: 'flex-end', height: AppConstants.getDeviceHeight(8), }]}
                    position="top"
                    positionValue={AppConstants.getDeviceHeight(0)}
                    fadeInDuration={750}
                    fadeOutDuration={1000}
                    textStyle={AppConstants.CommonStyles.ToastTextStyle}
                />
                <Modal
                    closeOnTouchOutside={true}
                    backdrop={true}
                    open={this.state.open}
                    closeOnTouchOutside={false}
                    modalDidOpen={this.modalDidOpen}
                    modalDidClose={this.modalDidClose}
                    modalStyle={styles.model}>

                    <View style={styles.mainContainer}>
                        <View style={{ height: AppConstants.getDeviceHeight(4), justifyContent: 'center', }}>
                            <View style={styles.sentat}>
                                <Text style={styles.text}>Skill List</Text>
                                <Close
                                    name='close'
                                    style={{
                                        position: 'absolute',
                                        alignSelf: 'flex-end'
                                    }}
                                    color={AppConstants.COLORS.HEDARBACKGROUND}
                                    onPress={() => {
                                        this.props.func1();
                                        this.setState({ open: false })
                                    }}
                                    size={AppConstants.FONTSIZE.FS22}
                                />
                            </View>
                        </View>


                        <CustomMultiPicker
                            options={this.state.skilll}
                            search={true} // should show search bar?
                            multiple={true} //
                            placeholder={"Search"}
                            placeholderTextColor={'#757575'}
                            returnValue={"label"} // label or value
                            callback={(res) => { this.setState({ dataSource: res }); }} // callback, array of selected items
                            rowBackgroundColor={"#eee"}
                            rowHeight={40}
                            rowRadius={5}
                            iconColor={AppConstants.COLORS.HEDARBACKGROUND}
                            iconSize={27}

                            selectedIconName={"ios-checkmark-circle-outline"}
                            unselectedIconName={"ios-radio-button-off"}
                            scrollViewHeight={130}
                            icon={0}
                            selected={this.state.selected}
                        />

                    </View>
                    <TouchableOpacity style={{ borderBottomRightRadius: AppConstants.getDeviceWidth(2), borderBottomLeftRadius: AppConstants.getDeviceWidth(2), height: AppConstants.getDeviceHeight(5), width: AppConstants.getDeviceWidth(90), backgroundColor: AppConstants.COLORS.PROFILEINACTIVE, justifyContent: 'center', }}
                        onPress={() => {
                            var array = [...this.state.dataSource];
                            // array.splice(0, 1)
                            this.setState({ dataSource: array })

                            this.setState({ open: false })
                        }}>
                        {/* <LinearGradient start={{ x: 0.0, y: 0.0 }} end={{ x: 1.0, y: 0.0 }} colors={['#d6b469', '#fff59a', '#d6b469',]}
                            style={{ height: AppConstants.getDeviceHeight(6), justifyContent: 'center', width: AppConstants.getDeviceWidth(90), borderBottomRightRadius: AppConstants.getDeviceWidth(2), borderBottomLeftRadius: AppConstants.getDeviceWidth(2), alignSelf: 'center', position: 'absolute', bottom: 0, }}> */}
                        <Text style={{
                            fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
                            fontWeight: '600',
                            alignSelf: 'center',
                            color: AppConstants.COLORS.PROFILEBACKGROUND,
                        }}>
                            DONE
                                    </Text>
                        {/* </LinearGradient> */}
                    </TouchableOpacity>



                </Modal>
                {this.state.isLoading1 ? <View style={AppConstants.CommonStyles.spinner}>
                    <ActivityIndicator size='large' ></ActivityIndicator>
                </View> : null}
            </View >
        );
    }

}
