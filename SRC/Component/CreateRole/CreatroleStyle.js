import * as AppConstants from '../Helper/AppConstants';
export default {
    mainContainer: {
        flex: 1,

    },
    projecttitle1: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS18),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        fontWeight: 'bold',
        marginLeft: AppConstants.getDeviceWidth(5),
        marginTop: AppConstants.getDeviceHeight(2)
    },
    projectdesc: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        marginLeft: AppConstants.getDeviceWidth(5),
        marginTop: AppConstants.getDeviceHeight(2),
        fontWeight: 'bold'
    },
    Audio: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        marginLeft: AppConstants.getDeviceWidth(5),


    },
    projectdesc1: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        alignSelf: 'center',
       
    },
    projectdesc2: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PLACEHOLDAR,
        marginLeft: AppConstants.getDeviceWidth(5),

    },
    textboxText1: {
        width: AppConstants.getDeviceWidth(90),
        height: AppConstants.getDeviceHeight(4),
        color: AppConstants.COLORS.BLACK,
        backgroundColor: AppConstants.COLORS.PROFILETEXINPUT,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        marginLeft: AppConstants.getDeviceWidth(5),
        marginRight: AppConstants.getDeviceWidth(7),
        paddingLeft: AppConstants.getDeviceWidth(2),
        justifyContent: 'center',
        padding: 0,
        margin: 0,
        marginTop: AppConstants.getDeviceHeight(1),
    },
    textboxText2: {
        width: AppConstants.getDeviceWidth(60),
        height: AppConstants.getDeviceHeight(4),
        color: AppConstants.COLORS.BLACK,
        backgroundColor: AppConstants.COLORS.PROFILETEXINPUT,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        marginLeft: AppConstants.getDeviceWidth(5),
        // marginRight: AppConstants.getDeviceWidth(7),
        paddingLeft: AppConstants.getDeviceWidth(2),
        justifyContent: 'center',
        padding: 0,
        margin: 0,
        marginTop: AppConstants.getDeviceHeight(1),
    },
    textmultiline: {
        width: AppConstants.getDeviceWidth(90),
        height: AppConstants.getDeviceHeight(11),
        color: AppConstants.COLORS.BLACK,
        backgroundColor: AppConstants.COLORS.PROFILETEXINPUT,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS13),
        marginLeft: AppConstants.getDeviceWidth(5),
        marginRight: AppConstants.getDeviceWidth(2),
        paddingLeft: AppConstants.getDeviceWidth(2),
        marginTop: AppConstants.getDeviceHeight(1)
    },
    submit: {
        width: AppConstants.getDeviceWidth(23),
        height: AppConstants.getDeviceHeight(4),
        backgroundColor: AppConstants.COLORS.PROFILEINACTIVE,
        marginLeft: AppConstants.getDeviceWidth(5),
        justifyContent: 'center',
        marginTop: AppConstants.getDeviceHeight(1.5),
        marginBottom: AppConstants.getDeviceHeight(2)
    },
    button1: {
        width: AppConstants.getDeviceWidth(100),
        marginTop: AppConstants.getDeviceHeight(8),
        height: AppConstants.getDeviceHeight(3),
        justifyContent: "center",
    },
    savetouchable: {
        backgroundColor: AppConstants.COLORS.PROFILEINACTIVE,
        height: AppConstants.getDeviceHeight(5),
        marginBottom: AppConstants.getDeviceHeight(2),
        alignItems: 'center',
        justifyContent: 'center',
        width: AppConstants.getDeviceWidth(35),
        alignSelf: 'center'
    },
    modalRenderView: {
        flex: 1,
        position: "absolute",
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        justifyContent: "center"
    },
    datepickerdate: {
        alignSelf: 'center',
        alignItems: 'center',
        textAlign: 'center',
        // paddingRight: AppConstants.getDeviceWidth(5),
     
        paddingBottom: AppConstants.getDeviceWidth(2),
        color: '#000',
        marginTop:AppConstants.getDeviceHeight(1)
    },
    datepicker: {
        width: AppConstants.getDeviceWidth(42),
        height: AppConstants.getDeviceHeight(5),
        backgroundColor: AppConstants.COLORS.BACKHOME,
        marginLeft: AppConstants.getDeviceWidth(5),
        justifyContent: 'center',
        marginTop:AppConstants.getDeviceHeight(1)
    },
    projectdate: {
        fontSize: AppConstants.moderateScale(12),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.datecolor,
        fontWeight: 'bold',
        marginLeft: AppConstants.getDeviceWidth(5),
        marginRight: AppConstants.getDeviceWidth(0.5),
        alignSelf: 'center',
        alignItems: 'center',
        textAlign: 'center',
        
    },
    line: {
        width: AppConstants.getDeviceWidth(0.5),
        height: AppConstants.getDeviceHeight(5),
        backgroundColor: AppConstants.COLORS.datecolor,
     

    },
    lin1: { width: AppConstants.getDeviceWidth(100), height: AppConstants.moderateScale(0.5), backgroundColor: AppConstants.COLORS.PLACEHOLDAR, marginTop: AppConstants.getDeviceHeight(3), },
    video: { alignSelf: 'center', marginTop: AppConstants.getDeviceHeight(2), marginLeft: AppConstants.getDeviceWidth(1) },
    lin2: { width: AppConstants.getDeviceWidth(100), height: AppConstants.moderateScale(0.5), backgroundColor: AppConstants.COLORS.PLACEHOLDAR },
    script: { alignSelf: 'center', marginTop: AppConstants.getDeviceHeight(1.5), marginLeft: AppConstants.getDeviceWidth(1) },
    modal: {
        width: AppConstants.getDeviceWidth(90),
        height: AppConstants.getDeviceHeight(70),
        backgroundColor: AppConstants.COLORS.VIEWBACKGROUND,
        marginTop: AppConstants.getDeviceHeight(5),
        borderRadius: 5,
       
    },
    text: {
        justifyContent: "center",
        alignItems: "center",
        justifyContent: 'space-between',
        alignSelf: 'center',
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        fontWeight: AppConstants.WEIGHT.FONTWEIGHT
    },
}