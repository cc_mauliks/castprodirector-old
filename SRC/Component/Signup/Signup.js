import React, { Component } from 'react';
import {
    TextInput,
    Image,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    Platform,
    Keyboard,
    Alert,
    ActivityIndicator,
    NativeModules,
    ImageBackground
} from 'react-native';
import styles from './SignupStyle'
import * as AppConstants from '../Helper/AppConstants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import OfflineNotice from '../Helper/OfflineNotice';
import Toast, { DURATION } from 'react-native-easy-toast'
import { PostData, internet } from '../WebServices/webAPIRequest';
import Iconss from "react-native-vector-icons/MaterialCommunityIcons";
import Check from "react-native-vector-icons/FontAwesome";
import API from "../WebServices/API";
import DatePicker from 'react-native-datepicker';
import { Dropdown } from 'react-native-material-dropdown';
import Icons from "react-native-vector-icons/AntDesign";
import DeviceInfo, { getUniqueId, getTimezone } from "react-native-device-info";
const { PlatformConstants } = NativeModules;
import firebase from 'react-native-firebase';

const Images = {
    backgroundImage: 'background',
    Loginlogo: 'loginlogo',
    Loginbackground: 'loginbackground'

}
export let Gender = [{
    value: 'Male',
}, {
    value: 'Female',
},
];
export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Firstname: '',
            Firstname1: false,
            Lastname: '',
            Lastname1: false,
            EmialId: '',
            EmialId1: false,
            EmialId11: false,
            Password: '',
            Password1: false,
            Password2: false,
            Confirmpassword: '',
            Confirmpassword1: false,
            Confirmpassword11: false,
            Phonenumber: '',
            Phonenumber1: false,
            isLoading: false,
            terms: '',
            terms1: false,
            checkboxname: 'square-o',
            date: '',
            date1: false,
            gendar: 'Male',
            currantdate: '',
            devicetype: '',
            countrycode: [],
            selectcountrycode: '',
            selectcountrycode1: false,
            countryname: '',
            countryname1: false,
            uniqueId: '',
            device_token: ''
        }
    }



    componentWillMount() {
        firebase.messaging().getToken()
            .then((token) => {
                this.setState({ device_token: token });
                PushNoty.RegisterForPushNotification(this.props.navigation);
            });

        this.setState({ countrycode: this.props.navigation.state.params.cuntrycode, });
        if (AppConstants.countryname != '') {
            this.setState({ selectcountrycode: this.props.navigation.state.params.cuntrycode.filter(item => item.coutryname == AppConstants.countryname)[0].value })
        }

        var date = new Date().getDate(); //Current Date
        var month = new Date().getMonth() + 1; //Current Month
        var year = new Date().getFullYear(); //Current Year
        this.setState({ currantdate: date + '-' + month + '-' + year, deviceType: Platform.OS == 'ios' ? PlatformConstants.interfaceIdiom : DeviceInfo.getType() })

    }
    validateEmail = Email => {
        var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return re.test(Email);
    };
    validator = () => {

        if (this.state.Firstname == '') {
            this.setState({ Firstname1: true })
        }
        else if (this.state.Lastname == '') {
            this.setState({ Lastname1: true })
        }
        else if (this.state.EmialId == '') {
            this.setState({ EmialId1: true })
        }
        else if (!this.validateEmail(this.state.EmialId)) {
            this.setState({ EmialId11: true })
        }
        else if (this.state.selectcountrycode == '') {
            this.setState({ selectcountrycode1: true })
        }
        else if (this.state.Phonenumber == '') {
            this.setState({ Phonenumber1: true })
        }
        else if (this.state.date == '') {
            this.setState({ date1: true })
        }
        else if (this.state.Password == '') {
            this.setState({ Password1: true })
        }
        else if (this.state.Password.length < 4) {
            this.setState({ Password2: true })
        }
        else if (this.state.Confirmpassword == '') {
            this.setState({ Confirmpassword1: true })
        }
        else if (this.state.Password != this.state.Confirmpassword) {
            this.setState({ Confirmpassword11: true })
        }
        else if (this.state.terms == false) {
            this.setState({ terms1: true })
        }
        else {
            this.WSlogin()
        }
    }

    WSlogin() {
        Keyboard.dismiss();
        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({
                first_name: this.state.Firstname,
                last_name: this.state.Lastname,
                plaform_type: (Platform.OS === 'ios' ? 'Android' : 'iOS'),
                mobile_no: this.state.Phonenumber,
                email_address: this.state.EmialId,
                dob: this.state.date,
                gender: this.state.gendar,
                password: this.state.Password,
                device_type: 'phone',
                request_from: 'Director',
                device_id: AppConstants.DeviceID,
                country_code: this.state.selectcountrycode,
                device_token: this.state.device_token
            });
            PostData(API.Register, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false });
                    if (responseJson.statusCode == 1) {
                        global.userid = responseJson.userId;
                        global.otp = responseJson.otp;
                        this.setState({ Firstname: '', Lastname: '', Phonenumber: '', EmialId: '', date: '', gendar: '', Password: '', selectcountrycode: '', });
                        this.props.navigation.replace(AppConstants.SCREENS.OTP);
                    }
                    else if (responseJson.statusCode == 2) {
                        Alert.alert(
                            'Register User',
                            responseJson.message,
                            [
                                { text: 'OK', onPress: () => this.props.navigation.navigate(AppConstants.SCREENS.FORGOTPASSWORD) },
                            ],
                            { cancelable: false },
                        );
                    }
                    else {
                        Alert.alert(responseJson.message);
                    }
                })
                .catch(() => {
                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }
    }
    checkbox() {
        if (this.state.checkboxname == 'square-o') {
            this.setState({ terms: true, checkboxname: 'check-square', terms1: false });
        }
        else {
            this.setState({ terms: false, checkboxname: 'square-o' });
        }
    }
    render() {
        return (
            <KeyboardAwareView doNotForceDismissKeyboardWhenLayoutChanges={true}>
                <StatusBar barStyle="light-content" backgroundCopr={AppConstants.SHADOWCOLORS.INTRO} animated translucent></StatusBar>
                <OfflineNotice />
                <View style={styles.mainContainer}>
                    <ImageBackground source={{ uri: Images.Loginbackground }} style={styles.mainContainer}>
                        <Toast
                            ref="toast"
                            style={[AppConstants.CommonStyles.ToastStyle1, { justifyContent: 'flex-end', height: AppConstants.getDeviceHeight(11), }]}
                            position="top"
                            positionValue={AppConstants.getDeviceHeight(0)}
                            fadeInDuration={750}
                            fadeOutDuration={1000}
                            textStyle={AppConstants.CommonStyles.ToastTextStyle}
                        />
                        <ScrollView contentContainerStyle={{ flexGrow: 1, marginBottom: AppConstants.getDeviceHeight(10) }}>
                            <View >
                                <Iconss
                                    onPress={() => this.props.navigation.replace(AppConstants.SCREENS.LOGIN)}
                                    name="arrow-left"
                                    style={{ marginLeft: AppConstants.getDeviceWidth(4), marginTop: AppConstants.getDeviceHeight(this.props.navigation.getParam('header') != null && !this.props.navigation.getParam('header') ? 6.8 : Platform.OS === 'ios' ? 0 : 3) }}
                                    size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS25)}
                                    color={AppConstants.COLORS.WHITE}
                                />
                            </View>
                            <View style={styles.logoContainer}>
                                <Image style={styles.lginlogo} source={{ uri: Images.Loginlogo }} />
                            </View>
                            <View style={styles.textboxContainer}>
                                <TextInput style={styles.textboxText}
                                    placeholder="First Name"
                                    placeholderTextColor={AppConstants.COLORS.PLACEHOLDARLOGIN}
                                    underlineColorAndroid="transparent"
                                    returnKeyType={"next"}
                                    blurOnSubmit={false}
                                    autoCapitalize='words'
                                    onSubmitEditing={(event) => {
                                        this.refs.Lastname.focus();
                                    }}
                                    onChangeText={(Firstname) => this.setState({ Firstname, Firstname1: false })} />
                                <View style={styles.line}>
                                </View>
                                {this.state.Firstname1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(2) }]}>{AppConstants.Messages.FIRSTNAME}</Text> : null}
                            </View>
                            <View style={styles.textboxContainer1}>
                                <TextInput style={styles.textboxText}
                                    placeholder="Last Name"
                                    placeholderTextColor={AppConstants.COLORS.PLACEHOLDARLOGIN}
                                    underlineColorAndroid="transparent"
                                    returnKeyType={"next"}
                                    blurOnSubmit={false}
                                    autoCapitalize="words"
                                    onSubmitEditing={(event) => {
                                        this.refs.EmialId.focus();
                                    }}
                                    ref='Lastname'
                                    onChangeText={(Lastname) => this.setState({ Lastname, Lastname1: false })} />
                                <View style={styles.line}>
                                </View>
                                {this.state.Lastname1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(2) }]}>{AppConstants.Messages.LASTNAME}</Text> : null}
                            </View>
                            <View style={styles.textboxContainer1}>
                                <TextInput style={styles.textboxText}
                                    placeholder="Email Id"
                                    placeholderTextColor={AppConstants.COLORS.PLACEHOLDARLOGIN}
                                    underlineColorAndroid="transparent"
                                    returnKeyType={"next"}
                                    blurOnSubmit={false}
                                    autoCapitalize="none"
                                    onSubmitEditing={(event) => {
                                        this.refs.Phonenumber.focus();
                                    }}
                                    ref='EmialId'
                                    onChangeText={(EmialId) => this.setState({ EmialId, EmialId1: false, EmialId11: false })} />
                                <View style={styles.line}>
                                </View>
                                {this.state.EmialId1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(2) }]}>{AppConstants.Messages.NOEMAIL}</Text> : null}
                                {this.state.EmialId11 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(2) }]}>{AppConstants.Messages.NOEMAILINVALIDEMAILADDRESS}</Text> : null}
                            </View>
                            <View style={styles.Phonenumber}>
                                <View style={{ width: AppConstants.getDeviceWidth(23) }}>
                                    <Dropdown
                                        containerStyle={styles.countrycode}
                                        customStyles={{ placeholderText: { fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.PLANVIEWTEXT), }, }}
                                        value={this.state.selectcountrycode}
                                        labelHeight={0}
                                        style={{ fontSize: AppConstants.FONTSIZE.FS14, paddingLeft: AppConstants.getDeviceWidth(1), color: AppConstants.COLORS.WHITE }}
                                        inputContainerStyle={{ borderBottomColor: "transparent" }}
                                        labelPadding={0}
                                        renderAccessory={() => <Icons name="down" color={AppConstants.COLORS.WHITE} style={{ position: 'absolute', right: 0, marginRight: AppConstants.getDeviceWidth(2), paddingTop: AppConstants.getDeviceHeight(4) }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS16)} />}
                                        data={this.state.countrycode}
                                        onChangeText={selectcountrycode => {
                                            this.setState({ selectcountrycode: selectcountrycode, selectcountrycode1: false });
                                        }} />
                                    <View style={styles.line1}>
                                    </View>
                                    {this.state.selectcountrycode1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(2) }]}>{AppConstants.Messages.COUNTRYCODE}</Text> : null}
                                </View>
                                <View style={{ width: AppConstants.getDeviceWidth(80) }}>
                                    <TextInput style={styles.textboxTextphonenumber}
                                        placeholder="Phone Number"
                                        placeholderTextColor={AppConstants.COLORS.PLACEHOLDARLOGIN}
                                        underlineColorAndroid="transparent"
                                        keyboardType={"phone-pad"}
                                        returnKeyType={"next"}
                                        blurOnSubmit={false}
                                        autoCapitalize="none"
                                        maxLength={15}
                                        onSubmitEditing={(event) => {
                                            this.refs.Password.focus();
                                        }}
                                        ref='Phonenumber'
                                        onChangeText={(Phonenumber) => this.setState({ Phonenumber, Phonenumber1: false })} />
                                    <View style={styles.line2}>
                                    </View>
                                </View>
                            </View>
                            {this.state.Phonenumber1 == true ? <Text style={styles.textnumber}>
                                {AppConstants.Messages.MOBILENONOTANUMBER}
                            </Text> : null}
                            <View style={styles.textboxContainer}>
                                <DatePicker
                                    style={styles.datestyle}
                                    date={this.state.date}
                                    mode="date"
                                    placeholder="Date of Birth"
                                    format="DD-MM-YYYY"
                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    showIcon={false}
                                    customStyles={{
                                        dateInput: {
                                            borderWidth: 0, alignItems: 'flex-start',
                                            marginLeft: AppConstants.getDeviceWidth(3.53),
                                        },
                                        dateIcon: null,
                                        dateText: {
                                            textAlign: 'left',
                                            paddingBottom: AppConstants.getDeviceWidth(2),
                                            color: AppConstants.COLORS.WHITE,
                                            marginLeft: AppConstants.getDeviceWidth(2)
                                        },
                                        placeholderText: {
                                            marginBottom: AppConstants.getDeviceHeight(1),
                                            marginLeft: AppConstants.getDeviceWidth(2),
                                            color: AppConstants.COLORS.PLACEHOLDARLOGIN
                                        }
                                    }}
                                    onDateChange={(date) => {
                                        this.setState({ date: date, date1: false })
                                    }} />
                                <View style={styles.line}>
                                </View>
                                {this.state.date1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(2) }]}>{AppConstants.Messages.NODATEOFBIRTH}</Text> : null}
                            </View>
                            <View style={styles.textboxContainer}>
                                <Dropdown
                                    containerStyle={styles.containergender}
                                    customStyles={{ placeholderText: { fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.PLANVIEWTEXT), }, }}
                                    value={this.state.gendar}
                                    labelHeight={0}
                                    style={{ fontSize: AppConstants.FONTSIZE.FS14, paddingLeft: AppConstants.getDeviceWidth(1), color: AppConstants.COLORS.WHITE }}
                                    inputContainerStyle={{ borderBottomColor: "transparent" }}
                                    labelPadding={0}
                                    renderAccessory={() => <Icons name="down" color={AppConstants.COLORS.WHITE} style={{ position: 'absolute', right: 0, marginRight: AppConstants.getDeviceWidth(2), paddingTop: AppConstants.getDeviceHeight(4) }} size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS16)} />}
                                    data={Gender}
                                    onChangeText={gendar => {
                                        this.setState({ gendar: gendar });
                                    }} />
                                <View style={styles.line}>
                                </View>
                            </View>
                            <View style={styles.textboxContainer1}>
                                <TextInput style={styles.textboxText}
                                    placeholder="Password"
                                    placeholderTextColor={AppConstants.COLORS.PLACEHOLDARLOGIN}
                                    underlineColorAndroid="transparent"
                                    returnKeyType={"next"}
                                    blurOnSubmit={false}
                                    secureTextEntry={true}
                                    autoCapitalize="none"
                                    maxLength={20}
                                    onSubmitEditing={(event) => {
                                        this.refs.Confirmpassword.focus();
                                    }}
                                    ref='Password'
                                    onChangeText={(Password) => this.setState({ Password, Password1: false ,Password2:false})} />

                                <View style={styles.line}>
                                </View>
                                {this.state.Password1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(2) }]}>{AppConstants.Messages.NOPASSWORD}</Text> : null}
                                {this.state.Password2 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(2) }]}>{AppConstants.Messages.NOPASSWORDfor}</Text> : null}
                            </View>
                            <View style={styles.textboxContainer1}>
                                <TextInput style={styles.textboxText}
                                    placeholder="Confirm Password"
                                    placeholderTextColor={AppConstants.COLORS.PLACEHOLDARLOGIN}
                                    underlineColorAndroid="transparent"
                                    returnKeyType={"done"}
                                    secureTextEntry={true}
                                    blurOnSubmit={false}
                                    autoCapitalize="none"
                                    maxLength={20}
                                    onSubmitEditing={(event) => {
                                        Keyboard.dismiss();
                                    }}
                                    ref='Confirmpassword'
                                    onChangeText={(Confirmpassword) => this.setState({ Confirmpassword, Confirmpassword1: false, Confirmpassword11: false })} />
                                <View style={styles.line}>
                                </View>
                                {this.state.Confirmpassword1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(2) }]}>{AppConstants.Messages.NOCONFIRMPASSWORD}</Text> : null}
                                {this.state.Confirmpassword11 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(2) }]}>{AppConstants.Messages.PASSWORDSHOULDMATCH}</Text> : null}

                            </View>
                            <View style={styles.terms}>
                                <View style={{ flexDirection: 'row', justifyContent: 'center', position: 'absolute', marginTop: AppConstants.getDeviceHeight(2) }}>
                                    <Check
                                        onPress={() => this.checkbox()}
                                        name={this.state.checkboxname}
                                        size={AppConstants.moderateScale(AppConstants.FONTSIZE.FS25)}
                                        color={AppConstants.COLORS.LANGAUGE}
                                    />
                                    <Text style={styles.termcondiontext}>
                                        I agree with the
                                    </Text>
                                    <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => { this.props.navigation.navigate(AppConstants.SCREENS.TERMSANDCONDITION) }}>
                                        <Text style={[styles.termcondiontext, { textDecorationLine: 'underline', }]}>
                                            Terms & Conditions
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {this.state.terms1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(5), marginTop: AppConstants.getDeviceHeight(3), alignSelf: 'center' }]}>{AppConstants.Messages.TREMS}</Text> : null}

                            <View style={styles.regierterview}>
                                <TouchableOpacity style={styles.signupbutton} onPress={() => { this.validator() }}>
                                    <Text style={styles.Signup}>
                                        Register
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </ImageBackground>
                </View>
                {this.state.isLoading ? <View style={AppConstants.CommonStyles.spinner}>
                    <ActivityIndicator size='large' color={AppConstants.COLORS.SIGNUP}></ActivityIndicator>
                </View> : null}
            </KeyboardAwareView>
        )
    }
}
