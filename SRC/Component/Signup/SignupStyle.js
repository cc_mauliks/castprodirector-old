import * as AppConstants from '../Helper/AppConstants';
export default {
    mainContainer: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: AppConstants.COLORS.Loginback,
    },

    logoContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: AppConstants.getDeviceHeight(3),
    },
    lginlogo: {
        aspectRatio: 1,
        height: AppConstants.getDeviceHeight(20),
        width: AppConstants.getDeviceHeight(20),
    },
    textboxContainer: {
        marginTop: AppConstants.getDeviceHeight(4),
        width: AppConstants.getDeviceWidth(90),
        height: AppConstants.getDeviceHeight(5),
        marginLeft: AppConstants.getDeviceWidth(5),
        marginRight: AppConstants.getDeviceWidth(5),
    },
    textboxContainer1: {
        marginTop: AppConstants.getDeviceHeight(3),
        width: AppConstants.getDeviceWidth(90),
        height: AppConstants.getDeviceHeight(5),
        marginLeft: AppConstants.getDeviceWidth(5),
        marginRight: AppConstants.getDeviceWidth(6),
    },
    Phonenumber: {
        marginTop: AppConstants.getDeviceHeight(3),
        width: AppConstants.getDeviceWidth(90),
        height: AppConstants.getDeviceHeight(5),
        marginLeft: AppConstants.getDeviceWidth(5),
        marginRight: AppConstants.getDeviceWidth(6),
        flexDirection: 'row',
        alignItems: 'center',
    },

    terms: {
        marginTop: AppConstants.getDeviceHeight(3),
        width: AppConstants.getDeviceWidth(90),
        height: AppConstants.getDeviceHeight(3),
        marginLeft: AppConstants.getDeviceWidth(5),
        alignItems: 'center',
    },
    textboxText: {
        width: AppConstants.getDeviceWidth(90),
        height: AppConstants.getDeviceHeight(6.6),
        color: AppConstants.COLORS.WHITE,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS13),
        paddingLeft: AppConstants.getDeviceWidth(2),
    },
    textboxTextphonenumber: {
        width: AppConstants.getDeviceWidth(70),
        height: AppConstants.getDeviceHeight(6.6),
        color: AppConstants.COLORS.WHITE,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS13),
        paddingLeft: AppConstants.getDeviceWidth(2),
    },
    Signup: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS18),
        fontWeight: '600',
        alignSelf: 'center',
        color: AppConstants.COLORS.PROFILEINACTIVE,
        marginBottom: AppConstants.getDeviceHeight(1)
    },
    line: {
        width: AppConstants.getDeviceWidth(87),
        height: AppConstants.getDeviceHeight(0.1),
        backgroundColor: AppConstants.COLORS.LANGAUGE,
    },
    line1: {
        width: AppConstants.getDeviceWidth(20),
        height: AppConstants.getDeviceHeight(0.1),
        marginLeft: AppConstants.getDeviceWidth(2),
        backgroundColor: AppConstants.COLORS.LANGAUGE,
    },
    line2: {
        width: AppConstants.getDeviceWidth(65),
        height: AppConstants.getDeviceHeight(0.1),
        backgroundColor: AppConstants.COLORS.LANGAUGE,
    },
    signupbutton: {
        height: AppConstants.getDeviceHeight(5),
        width: AppConstants.getDeviceWidth(35),
        bottom: 0, justifyContent: 'flex-end',
        marginBottom: AppConstants.getDeviceHeight(2),
        alignItems: 'center', alignSelf: 'center'
    },
    countrycode: {
        width: AppConstants.getDeviceWidth(23),
        height: AppConstants.getDeviceHeight(5),
        marginTop: AppConstants.getDeviceHeight(1.5),
        paddingLeft: AppConstants.getDeviceWidth(2)
    },
    textnumber: {
        color: AppConstants.COLORS.RED,
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        marginLeft: AppConstants.getDeviceWidth(5),
        marginLeft: AppConstants.getDeviceWidth(2),
        marginTop: AppConstants.getDeviceWidth(1),
    },
    datestyle: {
        width: AppConstants.getDeviceWidth(90),
        height: AppConstants.getDeviceHeight(5),
        alignItems: 'flex-start',
        marginRight: AppConstants.getDeviceWidth(7),
    },
    containergender: {
        width: AppConstants.getDeviceWidth(90),
        height: AppConstants.getDeviceHeight(5),
        paddingLeft: AppConstants.getDeviceWidth(2)
    },
    termcondiontext: {
        marginTop: AppConstants.getDeviceHeight(0.5),
        color: AppConstants.COLORS.WHITE,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS13),
        marginLeft: AppConstants.getDeviceWidth(3.53),
        justifyContent: 'center',
    },
    regierterview: { height: AppConstants.getDeviceHeight(10), bottom: 0, flex: 1, justifyContent: 'flex-end', }
}