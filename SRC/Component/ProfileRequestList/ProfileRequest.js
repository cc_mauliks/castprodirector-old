import React, { Component } from 'react';

import {
    Keyboard,
    TouchableOpacity,
    View,
    Text,
    StatusBar,
    FlatList,
    ActivityIndicator,
    RefreshControl,
    Alert
} from 'react-native';
import styles from './ProfileRequestStyle'
import * as AppConstants from '../Helper/AppConstants';
import OfflineNotice from '../Helper/OfflineNotice';
import Toast, { DURATION } from 'react-native-easy-toast'
import Icons from "react-native-vector-icons/AntDesign";
import { Avatar } from 'react-native-elements';
import API from "../WebServices/API";
import { PostData, internet } from '../WebServices/webAPIRequest';
const Images = {
    Bio: 'bio',
}
export default class ProfileRequest extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Search: '',
            dataSource: [],
            isRefreshing: false,
            page: 1,
            nexturl: '',
            errormessage: ''
        }
        this._renderRow = this._renderRow.bind(this);
        this.handleLoadMore = this.handleLoadMore.bind(this)
        this.lists = [
            this.props.navigation.addListener('didFocus', () => { this.componentDidFocus() })
        ];

    }
    componentDidFocus() {
        this.setState({ page: 1, dataSource: [] })
        this.ProfileRequestList();
    }
    renderFooter = () => {
        if (this.state.isRefreshing == true) {
            return (
                <ActivityIndicator
                    style={{ color: '#000' }}
                />
            );
        }
        return null;

    };
    handleLoadMore() {
        if (this.state.nexturl == '') {

            this.setState({ isRefreshing: false });
        }
        else {
            this.setState({ page: this.state.page + 1, isRefreshing: true, isLoading: false });
            this.WsAllProject();
        }

    };
    onRefresh() {
    }
    ProfileRequestList() {
        Keyboard.dismiss();
        if (global.isConnected) {
            if (this.state.isRefreshing == true) {
            }
            else {
                this.setState({ isLoading: true, });
            }
            APIData = JSON.stringify({
            });
            PostData(API.ProfileReqeustlist + '?page=' + this.state.page, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false });
                    if (responseJson.statusCode == 1) {

                        this.setState({
                            dataSource: [
                                ...this.state.dataSource,
                                ...responseJson.data
                            ],
                            nexturl: responseJson.next_page_url,
                        })

                    }
                    else {
                        this.setState({ errormessage: responseJson.message })
                        // Alert.alert(responseJson.message);
                    }
                }).catch(() => {
                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }
    }

    _renderRow(item, index, ) {
        var image = item.item.profile_img == '' ? Images.Bio : item.item.profile_img;
        return (
            <View style={styles.FlatlistContainer1}>
                <TouchableOpacity
                    onPress={() => { this.props.navigation.navigate(AppConstants.SCREENS.VIEWARTISTPROFILE, { artistid: item.item.actor_id }) }}>
                    <View style={styles.FlatlistContainer}>

                        <View style={{ width: AppConstants.getDeviceWidth(100), flexDirection: 'row', marginBottom: AppConstants.getDeviceHeight(1), marginTop: AppConstants.getDeviceHeight(1) }}>
                            <Avatar
                                size={AppConstants.getDeviceHeight(9)}
                                overlayContainerStyle={{ backgroundColor: 'black', }}
                                containerStyle={{ alignSelf: 'center', backgroundColor: null, marginLeft: AppConstants.getDeviceWidth(1), }}
                                rounded
                                source={{ uri: image }} />
                            <View style={{ width: AppConstants.getDeviceWidth(55), justifyContent: 'center', }}>

                                <Text style={styles.projecttitle}>{item.item.name}</Text>


                                <Text style={[styles.projectrole,]}>Mobile: {item.item.mobile_no}</Text>
                                <Text style={styles.projectrole}>Email: {item.item.email_address}</Text>
                            </View>
                            <View style={styles.dateview}>
                                <View style={styles.dateview2}>
                                    <Text style={styles.time1}>Status:</Text>
                                    <Text style={styles.time}> {item.item.status}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
    render() {
        return (
            <View style={styles.mainContainer}>
                <StatusBar barStyle="light-content" backgroundCopr={AppConstants.SHADOWCOLORS.INTRO} animated translucent></StatusBar>
                <OfflineNotice />
                <View style={styles.mainContainer}>
                    <Toast
                        ref="toast"
                        style={[AppConstants.CommonStyles.ToastStyle1, { justifyContent: 'flex-end', height: AppConstants.getDeviceHeight(11), }]}
                        position="top"
                        positionValue={AppConstants.getDeviceHeight(0)}
                        fadeInDuration={750}
                        fadeOutDuration={1000}
                        textStyle={AppConstants.CommonStyles.ToastTextStyle}
                    />
                    <Toast
                        ref="toast1"
                        style={[AppConstants.CommonStyles.ToastStyle2, { height: AppConstants.getDeviceHeight(6.5), }]}
                        position='bottom'
                        positionValue={200}
                        fadeInDuration={1000}
                        fadeOutDuration={1000}
                        textStyle={AppConstants.CommonStyles.ToastTextStyle1} />
                    <FlatList
                        scrollEnabled={true}
                        refreshControl={
                            <RefreshControl refreshing={this.state.isRefreshing}
                            />
                        }
                        data={this.state.dataSource}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={this._renderRow.bind(this)}

                        ListFooterComponent={this.renderFooter.bind(this)}
                        onEndReachedThreshold={0.4}
                        onEndReached={this.handleLoadMore.bind(this)}
                    />
                </View>
                {this.state.dataSource == '' ?
                    <View style={styles.viewerorr}>
                        <Text style={styles.error}>{this.state.errormessage}</Text>
                    </View> : null}
                {this.state.isLoading ? <View style={AppConstants.CommonStyles.spinner}>
                    <ActivityIndicator size='large' ></ActivityIndicator>
                </View> : null}
            </View>
        );
    }
}