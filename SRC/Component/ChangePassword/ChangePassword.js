
import React, { Component } from 'react';
import {
    View,
    Text,
    StatusBar,
    TextInput,
    TouchableOpacity,
    Keyboard,
    ActivityIndicator,
    Alert
} from 'react-native';
import styles from './ChangePasswordStyle'
import * as AppConstants from '../Helper/AppConstants';
import OfflineNotice from '../Helper/OfflineNotice';
import Toast, { DURATION } from 'react-native-easy-toast'
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import API from "../WebServices/API";
import { PostData, internet } from '../WebServices/webAPIRequest';

export default class ChangePassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            OldPassword: '',
            NewPasssowrd: '',
            ConfirmPasssword: '',
            isLoading: false,
            OldPassword1: false,
            NewPasssowrd1: false,
            ConfirmPasssword1: false,
            matchpassword1: false
        };

    }
    validator() {
        if (this.state.OldPassword == '') {
            this.setState({ OldPassword1: true })
        }
        else if (this.state.NewPasssowrd == '') {
            this.setState({ NewPasssowrd1: true })
        }
        else if (this.state.ConfirmPasssword == '') {
            this.setState({ ConfirmPasssword1: true })
        }
        else if (this.state.NewPasssowrd != this.state.ConfirmPasssword) {
            this.setState({ matchpassword1: true })
        }
        else {
            this.ChangePassword();
        }
    }
    ChangePassword() {
        Keyboard.dismiss();
        if (global.isConnected) {
            this.setState({ isLoading: true });
            APIData = JSON.stringify({
                old_password: this.state.OldPassword,
                new_password: this.state.NewPasssowrd,
                confirm_password: this.state.ConfirmPasssword
            });
            PostData(API.Chnagepassword, APIData)
                .then((responseJson) => {
                    this.setState({ isLoading: false });
                    if (responseJson.statusCode == 1) {
                        this.refs.toast1.show(responseJson.message, DURATION.LENGTH_SHORT, true);
                        this.state.navigation.goBack();
                    }
                    else {
                        Alert.alert(responseJson.message);
                    }
                }).catch(() => {
                    this.setState({ isLoading: false });
                    Alert.alert(AppConstants.Messages.APIERROR);
                });
        }
    }
    render() {
        return (
            <View style={styles.mainContainer}>
                <KeyboardAwareView doNotForceDismissKeyboardWhenLayoutChanges={true}>
                    <StatusBar barStyle="light-content" backgroundCopr={AppConstants.SHADOWCOLORS.INTRO} animated translucent></StatusBar>
                    <OfflineNotice />
                    <View style={styles.mainContainer}>
                        <Toast
                            ref="toast"
                            style={[AppConstants.CommonStyles.ToastStyle1, { justifyContent: 'flex-end', height: AppConstants.getDeviceHeight(11), }]}
                            position="top"
                            positionValue={AppConstants.getDeviceHeight(0)}
                            fadeInDuration={750}
                            fadeOutDuration={1000}
                            textStyle={AppConstants.CommonStyles.ToastTextStyle}
                        />
                        <Toast
                            ref="toast1"
                            style={[AppConstants.CommonStyles.ToastStyle2, { height: AppConstants.getDeviceHeight(6.5), }]}
                            position='bottom'
                            positionValue={200}
                            fadeInDuration={1000}
                            fadeOutDuration={1000}
                            textStyle={AppConstants.CommonStyles.ToastTextStyle1}
                        />
                        <Text style={styles.profiletext2}>Old Password</Text>
                        <TextInput style={styles.textboxText}
                            placeholderTextColor={AppConstants.COLORS.PLACEHOLDAR}
                            underlineColorAndroid="transparent"
                            blurOnSubmit={false}
                            autoCapitalize="none"
                            secureTextEntry={true}
                            returnKeyType={"next"}
                            maxLength={20}
                            value={this.state.OldPassword}
                            onSubmitEditing={() => {
                                this.refs.Lastname.focus();
                            }}
                            onChangeText={(OldPassword) => this.setState({ OldPassword ,OldPassword1:false})} />
                        {this.state.OldPassword1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(7) }]}>{AppConstants.Messages.OLDPASSWORD}</Text> : null}

                        <Text style={styles.profiletext2}>New Password</Text>
                        <TextInput style={styles.textboxText}
                            placeholderTextColor={AppConstants.COLORS.PLACEHOLDAR}
                            underlineColorAndroid="transparent"
                            blurOnSubmit={false}
                            autoCapitalize="none"
                            secureTextEntry={true}
                            returnKeyType={"next"}
                            maxLength={20}
                            value={this.state.NewPasssowrd}
                            onSubmitEditing={(event) => {
                                Keyboard.dismiss();
                            }}
                            ref='Lastname'
                            onChangeText={(NewPasssowrd) => this.setState({ NewPasssowrd ,NewPasssowrd1:false})} />
                        {this.state.NewPasssowrd1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(7) }]}>{AppConstants.Messages.NONEWPASSWORD}</Text> : null}

                        <Text style={styles.profiletext2}>Confirm Password</Text>
                        <TextInput style={styles.textboxText}
                            placeholderTextColor={AppConstants.COLORS.PLACEHOLDAR}
                            underlineColorAndroid="transparent"
                            blurOnSubmit={false}
                            autoCapitalize="none"
                            returnKeyType={"next"}
                            secureTextEntry={true}
                            maxLength={20}
                            value={this.state.ConfirmPasssword}
                            onSubmitEditing={(event) => {
                                Keyboard.dismiss();
                            }}
                            ref='Lastname'
                            onChangeText={(ConfirmPasssword) => this.setState({ ConfirmPasssword,ConfirmPasssword1:false,matchpassword1:false })} />

                        {this.state.ConfirmPasssword1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(7) }]}>{AppConstants.Messages.NOCONFIRMPASSWORD}</Text> : null}

                        {this.state.matchpassword1 == true ? <Text style={[AppConstants.CommonStyles.errorcolor, { marginLeft: AppConstants.getDeviceWidth(7) }]}>{AppConstants.Messages.PASSWORDSHOULDMATCH}</Text> : null}

                        <View style={styles.button}>
                            <TouchableOpacity style={{ backgroundColor: AppConstants.COLORS.PROFILEINACTIVE, height: AppConstants.getDeviceHeight(5), marginBottom: AppConstants.getDeviceHeight(2), alignItems: 'center', justifyContent: 'center', width: AppConstants.getDeviceWidth(30), alignSelf: 'center' }} onPress={() => { this.validator() }}>
                                <Text style={styles.Signup}>
                                    SAVE
                                    </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </KeyboardAwareView>
                {this.state.isLoading ? <View style={AppConstants.CommonStyles.spinner}>
                    <ActivityIndicator size='large' ></ActivityIndicator>
                </View> : null}
            </View>
        )
    }
}