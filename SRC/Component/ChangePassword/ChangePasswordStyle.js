import * as AppConstants from '../Helper/AppConstants';
export default {
    mainContainer: {
        flex: 1,

    },
    textboxText: {
        width: AppConstants.getDeviceWidth(86),
        height: AppConstants.getDeviceHeight(5.5),
        color: AppConstants.COLORS.BLACK,
        backgroundColor: AppConstants.COLORS.PROFILETEXINPUT,
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS13),
        marginLeft: AppConstants.getDeviceWidth(7),
        marginRight: AppConstants.getDeviceWidth(7),
        paddingLeft: AppConstants.getDeviceWidth(2),
        justifyContent: 'center',
        padding: 0,
        margin: 0,
    },
    profiletext2: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS12),
        marginLeft: AppConstants.getDeviceWidth(7),
        marginBottom: AppConstants.getDeviceHeight(0.5),
        marginTop: AppConstants.getDeviceHeight(2),
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,
        // fontWeight: 'bold',
    },
    button: {
        width: AppConstants.getDeviceWidth(100),
        marginTop: AppConstants.getDeviceHeight(8),
        height: AppConstants.getDeviceHeight(3),
        justifyContent: "center",
    },
    Signup: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontWeight: 'bold',
        alignSelf: 'center',
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        color: AppConstants.COLORS.PROFILEBACKGROUND,

    },
}